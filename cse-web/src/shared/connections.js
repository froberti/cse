export const API_URL = process.env.API_URL  || "http://localhost:8080" || "https://cse-web-253604.appspot.com"  ; //"https://cse-web-253604.appspot.com" ||
export const LOGIN_ENDPOINT = "/api/login";
export const USERS_ENDPOINT = "/api/user";
export const COURSES_ENDPOINT = "/api/course";
export const THEMES_ENDPOINT = "/api/course/theme";
export const PAYMENTS_ENDPOINT = "/api/payment";