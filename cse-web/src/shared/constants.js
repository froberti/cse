export const logInProviders = {
    'tienda': 1,
    'google': 2,
    'facebook': 3,
    'mercadoLibre': 4
};

export const userStatus = {
    'valid': 1,
    'pendingEmail': 2,
    'pendingInfo': 3,
    'disabled': 4
};

export const paymentStatus = {
    done: 1,
    pending: 2,
    cancelled: 3,
    error: 4
};

export const courseStatus = {
    active: 1,
    inactive: 2,
    deleted: 3
};

export const userRoles = {
    'default': 1,
    'manager': 2
};

export const functionalities = {
    'payForCourses': 1,
    'addEnabledUserEmails': 2,
    'checkPaymentStatus': 3,
    'manageCourses': 4
};