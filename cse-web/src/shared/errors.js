import {errors} from "../localization/errors"

export const shardErrors = {
    "EXPIRED_SESSION": "Expiró tu sesión",
    "DEFAULT_EXCEPTION": errors.defaultErrorMessage,
};

export const getErrorStringFromError = (error) => {
    try{
        if(error.msg)
            return error.msg;
        return shardErrors[error.response.data] ?shardErrors[error.response.data]:errors.defaultErrorMessage;
    }catch(e){
        return errors.defaultErrorMessage
    }
};