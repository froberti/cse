import {paymentStatus} from "./constants";

export const paymentStatusStrings = {
    [paymentStatus.done]: "Confirmado",
    [paymentStatus.pending]: "Pendiente",
    [paymentStatus.cancelled]: "Cancelado",
    [paymentStatus.error]: "Error"
};