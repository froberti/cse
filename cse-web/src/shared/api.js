import axios from "axios"
import {
    API_URL,
    COURSES_ENDPOINT,
    LOGIN_ENDPOINT,
    PAYMENTS_ENDPOINT,
    THEMES_ENDPOINT,
    USERS_ENDPOINT
} from "./connections";
import {getAuthInfo, isAuthInfoPresent} from "../features/login/storeSession";
import {endSession} from "../store/store";

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    if(isAuthInfoPresent())
        return {...config,headers: {...config.headers,"Authorization":  "Bearer " + getAuthInfo().token}};

    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});
//
axios.interceptors.response.use(
    function(response) {
        // Do something with response data

        return response;
    },
    function(error) {

        try{
            if(error.response.data.type === "NoTokenProvidedException" || error.response.data.type === "InvalidTokenProvidedException" )
                endSession();
        }catch(e){
            console.warn (e)
        }finally{
            // Do something with response error
            return Promise.reject(error);
        }
    });

export const getPayments = (page,filter) => axios.get(API_URL + PAYMENTS_ENDPOINT + `?page=${page}&filter=${filter}`);

export const completePendingInfo = (data) => axios.put(API_URL + USERS_ENDPOINT,data);

export const login = (data) => axios.post(API_URL + LOGIN_ENDPOINT,data);

export const register = (data) => axios.post(API_URL + USERS_ENDPOINT + "/register",data);

export const getCourses = () => axios.get(API_URL + COURSES_ENDPOINT);

export const getAvailableThemes = () => axios.get(API_URL+THEMES_ENDPOINT);

export const addTheme = (theme) => axios.post(API_URL+THEMES_ENDPOINT,theme);

export const addCourse = (course) => axios.post(API_URL+COURSES_ENDPOINT,course);

export const addPayment = (itemId) => axios.post(API_URL+PAYMENTS_ENDPOINT,{itemId});

export const deleteTheme = (id) => axios.delete(API_URL+THEMES_ENDPOINT+`/${id}`);

export const deleteCourse = (id) => axios.delete(API_URL+COURSES_ENDPOINT+`/${id}`);

export const updateTheme = (data) => axios.put(API_URL+THEMES_ENDPOINT,data);

export const updateCourse = (data) => axios.put(API_URL+COURSES_ENDPOINT,data);