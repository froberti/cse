import NAME from "./constants";

export const getLoginResponse = (store) => store[NAME].loginResponse;
export const getUserFunctionalities = (store) => (store[NAME].loginResponse && store[NAME].loginResponse.user && store[NAME].loginResponse.user.functionalities) || [];
export const loadingLogin = (store) => store[NAME].loading;