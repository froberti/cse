import {
    COMPLETE_PENDING_INFO, COMPLETE_PENDING_INFO_FULFILLED, COMPLETE_PENDING_INFO_REJECTED,
    LOG_IN, LOG_IN_FULFILLED, LOG_IN_REJECTED, LOG_OUT, LOG_OUT_FULFILLED, LOG_OUT_REJECTED
} from "./actionTypes";
import {removeAuthInfo, setAuthInfo} from "./storeSession";
import {openErrorToast} from "../snackbar/index";
import {completePendingInfo, login} from "../../shared/api";

export const completePendingInfoAction = (dni,phone) => async (dispatch) => {
    dispatch({type:COMPLETE_PENDING_INFO});
    try{
        const response = await completePendingInfo({dni,phone});
        dispatch({type:COMPLETE_PENDING_INFO_FULFILLED,payload: response.data});
        setAuthInfo(response.data);
        return response;
    }catch(e){
        dispatch({type:COMPLETE_PENDING_INFO_REJECTED,error:e});
        dispatch(openErrorToast(e));
        throw e
    }
};

export const loginAction = (email,password,providerId,token) => async (dispatch) => {
    dispatch({type:LOG_IN});

    try{
        const response = await login({email,password,providerId,token});
        dispatch({type:LOG_IN_FULFILLED,payload: response.data});
        setAuthInfo(response.data);
        return response;
    }catch(e){
        dispatch({type:LOG_IN_REJECTED,error:e});
        dispatch(openErrorToast(e));
        throw e
    }
};

export const setUserLogout = (dispatch) => dispatch({type:LOG_OUT_FULFILLED});

export const logOutAction = (dispatch) => {
    dispatch({type:LOG_OUT});

    try{
        //const users = await axios.post();
        dispatch(setUserLogout);
        removeAuthInfo();
    }catch(e){
        dispatch({type:LOG_OUT_REJECTED,error:e});
        dispatch(openErrorToast(e));
    }

};