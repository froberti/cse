import {
    COMPLETE_PENDING_INFO,
    COMPLETE_PENDING_INFO_FULFILLED, COMPLETE_PENDING_INFO_REJECTED,
    LOG_IN,
    LOG_IN_FULFILLED,
    LOG_IN_REJECTED,
    LOG_OUT,
    LOG_OUT_FULFILLED,
    LOG_OUT_REJECTED
} from "./actionTypes";
import {getAuthInfo} from "./storeSession";
import {userStatus} from "../../shared/constants";

const initialState = {
    loginResponse: getAuthInfo(),
    userPermissions: [],
    loading: false
};

export default function reducer(state=initialState, action){

    switch (action.type){
        case LOG_IN: return {...state,loading: true};
        case LOG_IN_FULFILLED: return {...state,loading:false,loginResponse: action.payload,userPermissions: (action.payload.userPermissions || [])};
        case LOG_IN_REJECTED: return {...state,loading:false};

        case LOG_OUT: return {...state,loading: true};
        case LOG_OUT_FULFILLED: return {...state,loading: false,loginResponse: null};
        case LOG_OUT_REJECTED: return {...state,loading:false};

        case COMPLETE_PENDING_INFO: return {...state,loading:true};
        case COMPLETE_PENDING_INFO_FULFILLED: return {...state,loading:false,loginResponse: {...state.loginResponse,user:{...state.loginResponse.user,userStatusId:userStatus.valid}  }};
        case COMPLETE_PENDING_INFO_REJECTED: return {...state,loading:false};
       default: return state
    }
}