import React from "react";
import {connect} from "react-redux";
import LoginForm from "./form";
import {loginAction} from "../actions";
import {passwordValidation, userValidation} from "../model";
import {openErrorToast} from "../../snackbar";
import {loadingLogin} from "../selectors";
import {createStructuredSelector} from "reselect";
import {routes} from "../../../router/routes";
import {logInProviders} from "../../../shared/constants";
import {GooglePlusProxy} from "../googleLogin";
import {withRouter} from "react-router-dom";

const LoginPage  = ({dispatch,loading,history}) => {
    function handleLogin(event){
        event.preventDefault();
        const email = event.target.email.value;
        const password = event.target.password.value;

        const emailIsOk = userValidation(email);
        if(emailIsOk !== true)
            return dispatch(openErrorToast(emailIsOk));

        const passwordIsOk = passwordValidation(password);
        if(passwordIsOk !== true)
            return dispatch(openErrorToast(passwordIsOk));

        dispatch(loginAction(email,password,logInProviders.tienda))
    }
    function handleGoogleLogInButtonClick() {
        GooglePlusProxy.login(
            (user) => {
                dispatch(loginAction(null,null,logInProviders.google,user.idToken));
            },
            (e) => {
                return dispatch(openErrorToast(e));
            }
        );
    }

    return <div id={"login-page"} className={"page first center-all-content"}>
                <LoginForm loading={loading}
                           handleGoogleLogInButtonClick={handleGoogleLogInButtonClick}
                           onRegisterButtonClick={()=>history.push(routes.register.path)} handleSubmit={handleLogin}/>
    </div>;
};

export default connect(createStructuredSelector({
    loading: loadingLogin
}))(withRouter(LoginPage))