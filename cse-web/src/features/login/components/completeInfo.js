import React from "react";
import {dniValidation,phoneValidation} from "../model";
import {connect} from "react-redux";
import {openErrorToast} from "../../snackbar";
import {completePendingInfoAction} from "../actions";
import Paper from "@material-ui/core/Paper/Paper";
import Avatar from "@material-ui/core/Avatar/Avatar";
import LockOutlinedIcon from "@material-ui/core/SvgIcon/SvgIcon";
import Typography from "@material-ui/core/Typography/Typography";
import {strings} from "../../../localization/strings";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Input from "@material-ui/core/Input/Input";
import Button from "@material-ui/core/Button/Button";

const CompleteInfoForm = ({handleSubmit,loading}) => (
    <Paper id='inputs-form' className={"center-all-content"}>
        <Typography component="h1" variant="h5">
            {strings.confirmYourDataToAccessToYourAccount}
        </Typography>
        <form onSubmit={handleSubmit}>
            <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="dni">{strings.dni}</InputLabel>
                <Input id="dni" name="dni" autoComplete="dni" autoFocus disabled={loading} />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="phone">{strings.phone}</InputLabel>
                <Input name="phone" type="phone" id="phone" autoComplete="phone" disabled={loading} />
            </FormControl>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={loading}
            >
                {strings.confirm}
            </Button>
        </form>
    </Paper>
);

const CompleteInfo = ({dispatch,loading}) =>{
    function handleLogin(event){
        event.preventDefault();
        const dni = event.target.dni.value;
        const phone = event.target.phone.value;

        const dniIsOk = dniValidation(dni);
        if(dniIsOk !== true)
            return dispatch(openErrorToast(dniIsOk));

        const phoneIsOk = phoneValidation(phone);
        if(phoneIsOk !== true)
            return dispatch(openErrorToast(phoneIsOk));

        dispatch(completePendingInfoAction(dni,phone))
    }
    return  <div id={"login-page"} className={"page first center-all-content"}>
        <CompleteInfoForm
            loading={loading}
            handleSubmit={handleLogin}/>
    </div>;
};

export default connect()(CompleteInfo)
