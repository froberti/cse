import React from 'react'
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from "@material-ui/core/Paper";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {strings} from "../../../localization/strings";
import styled from "styled-components";
import GoogleLogo from '../../../assets/images/g-normal.png';
const LoginForm = ({handleSubmit,loading,onRegisterButtonClick,handleGoogleLogInButtonClick}) => (
    <Paper id='inputs-form' className={"center-all-content"}>
        <Avatar >
            <LockOutlinedIcon color={"secondary"}/>
        </Avatar>
        <Typography component="h1" variant="h5">
            {strings.loginToYourAccount}
        </Typography>
        <form onSubmit={handleSubmit}>
            <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="email">{strings.emailPlaceholder}</InputLabel>
                <Input id="email" name="email" autoComplete="email" autoFocus disabled={loading} />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="password">{strings.passwordPlaceholder}</InputLabel>
                <Input name="password" type="password" id="password" autoComplete="current-password" disabled={loading} />
            </FormControl>
            <div className={"register-button-container"}>
                <Typography variant={"subtitle1"} inline >{strings.haveAccount}</Typography>
                <Button variant={"outlined"}
                        onClick={onRegisterButtonClick} color={"secondary"}>{strings.register}</Button>
            </div>

            <div>
                <Typography variant={"h6"} align={"left"}>{strings.youCanAlsoLoginWith}</Typography>
                <div >
                    <GoogleButton key={"button"} onClick={handleGoogleLogInButtonClick}>
                        <span className="icon"/>
                        <span className="buttonText">Google</span>
                    </GoogleButton>
                </div>
            </div>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={loading}
            >
                {strings.login}
            </Button>
        </form>
    </Paper>
);


const GoogleButton = styled.div`
    display: inline-block;
    background: white;
    color: #444;
    width: 190px;
    border-radius: 5px;
    border: thin solid #888;
    box-shadow: 1px 1px 1px grey;
    white-space: nowrap;
    margin: 15px 0;

    span.label {
        font-family: serif;
        font-weight: normal;
    }
    span.icon {
        background: url(${GoogleLogo}) transparent 5px 50% no-repeat;
        display: inline-block;
        vertical-align: middle;
        width: 42px;
        height: 42px;
    }
    span.buttonText {
        display: inline-block;
        vertical-align: middle;
        padding-left: 42px;
        padding-right: 42px;
        font-size: 14px;
        font-weight: bold;
        /* Use the Roboto font that is loaded in the <head> */
        font-family: 'Roboto', sans-serif;
    }
    &:hover{
        cursor:pointer;
    }
`;
export default LoginForm