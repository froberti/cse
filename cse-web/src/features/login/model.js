import {types, valueIsOfType} from "../../utils/types";
import {errors} from "../../localization/errors";

export const MIN_USER_LENGTH = 4;
export const MAX_USER_LENGTH = 25;
export const MIN_PASSWORD_LENGTH = 3;
export const MAX_PASSWORD_LENGTH = 10;

export const userValidation = (user) => {
    if(!user)
        return {msg: errors.enterUser};

    if(user.length < MIN_USER_LENGTH || user.length > MAX_USER_LENGTH)
        return {msg: errors.invalidUserLength};

    if(!valueIsOfType(user,types.email))
        return {msg: errors.invalidUser};

    return true
};

export const passwordValidation = (password) => {
    if(!password)
        return {msg: errors.enterPassword};
    
    if(password.length < MIN_PASSWORD_LENGTH || password.length > MAX_PASSWORD_LENGTH)
        return {msg: errors.invalidPasswordLength};

    if(!valueIsOfType(password,types.password))
        return {msg: errors.invalidPassword};

    return true
};


export const dniValidation = (dni) => {
    if(!dni)
        return {msg: errors.enterDni};

    if(dni.length < 7)
        return {msg: errors.invalidDniLength};

    if(!valueIsOfType(dni,types.number))
        return {msg: errors.invalidDni};

    return true
};

export const phoneValidation = (phone) => {
    if(!phone)
        return {msg: errors.enterPhone};

    if(phone.length < 7)
        return {msg: errors.invalidPhoneLength};

    if(!valueIsOfType(phone,types.number))
        return {msg: errors.invalidPhone};

    return true
};


