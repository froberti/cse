import React from "react"

const images = [
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0001.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0002.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0003.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0004.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0005.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0006.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0007.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0008.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/cleanItDossier/Dossier%20CLEAN%20IT_page-0009.jpg"
];

const CleanItDossier = () =>
    <div id={"clean-it-dossier"}>
        {
           images.map( (i,key) =>
               <img src={i} alt={key}/>
           )
        }
    </div>;

export default CleanItDossier