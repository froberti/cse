export const products = [

    //{title: "Proyector",images: proyector},
    {title: "Kit Robótica",images:
            [
                "https://storage.googleapis.com/www.cseducacion.com.ar/productos/robotica/kit%20robotica%20primaria%20(1)-min.jpg",
                "https://storage.googleapis.com/www.cseducacion.com.ar/productos/robotica/kit%20robotica%20primaria%20(1).jpg",
                "https://storage.googleapis.com/www.cseducacion.com.ar/productos/robotica/kit%20robotica%20primaria%20(2)-min.jpg",
                "https://storage.googleapis.com/www.cseducacion.com.ar/productos/robotica/kit%20robotica%20primaria%20(2).jpg",
                "https://storage.googleapis.com/www.cseducacion.com.ar/productos/robotica/kit%20robotica%20primaria%20(3)-min.jpg",
                "https://storage.googleapis.com/www.cseducacion.com.ar/productos/robotica/kit%20robotica%20primaria%20(3).jpg",
                "https://storage.googleapis.com/www.cseducacion.com.ar/productos/robotica/kit%20robotica%20primaria%20(4).jpg",
            ],
        subtitle:"Ideal para trabajar con estudiantes de 10 años en adelante.",
        description: [
            "Componentes de aluminio",
            "Componentes de metal",
            "Componentes de abs",
            "Compatible con Arduino",
            "Servo motor",
            "Motorreductores",
            "Módulo LED",
            "Buzzer",
            "Sensor de luz",
            "Sensor de ultra sonido",
            "Sensor touch",
            "Receptor RF para control remoto",
            "Set de ruedas grandes",
            "Manual digital de usuario"
        ]},
    {title: "CSE Humanoide",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/humanoide/Kit%20Robot%20Humanoide%20%20(10)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/humanoide/Kit%20Robot%20Humanoide%20%20(11)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/humanoide/Kit%20Robot%20Humanoide%20%20(6)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/humanoide/Kit%20Robot%20Humanoide%20%20(7)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/humanoide/Kit%20Robot%20Humanoide%20%20(8)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/humanoide/Kit%20Robot%20Humanoide%20%20(9)-min.jpg",
        ],
        description: [
            "Altura: 27cm.",
            "Peso: 830gr.",
            "Potencia nominal: 7.4V 3.6A.",
            "Servo Motores: 18 piezas (Piernas=10/Brazos=6/Pecho=1).",
            "Control y descarga: Bluetooth.",
            "Batería: 7.4V 1.3A.",
            "Torque máx: 5.3kg 5V 1A.",
            "Voltaje máx: 5V-9V.",
            "LED: 256 Colores variables.",
            "Engranajes: 4 de desaceleración.",
            "Interfaz de comunicación serial dúplex multinivel.",
            "Ángulo: 320° ajustable.",
            "Arduino: la interfaz de control es compatible."
        ]},
    {title: "CSE Drone",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/drone/drone%20(1)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/drone/drone%20(2)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/drone/drone%20(3)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/drone/drone%20(4)-min.jpg",
        ],
        subtitle: "Kit 4 en 1",
        description: [
            "Peso: 75-80gr.",
            "Dimensión máxima: 200x200x46mm.",
            "Batería: 800mha.",
            "Vuelo estable: Se mantiene suspendido en el aire",
            "9 modos de vuelo",
            "Conexión remota 2.4 G"
        ]},
    {title: "Impresora 3D",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/impresora3dd/impresora%203d%20(4)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/impresora3dd/impresora%203d%20(1)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/impresora3dd/impresora%203d%20(2)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/impresora3dd/impresora%203d%20(3)-min.jpg",
        ],
        subtitle: "TECCOM PRO",
        description: [
            "DIÁMETRO DE LA BOQUILLA: 0.4 mm",
            "DIÁMETRO DEL FILAMENTO: 1.75 mm",
            "GROSOR DE LA CAPA: 0.05 – 0.36 mm",
            "TAMAÑO MÁXIMO DE CONSTRUCCIÓN (An x Pr x Al): 200 x 200 x 185 mm",
            "BASE DE IMPRESIÓN: No precisa de adhesivos, evita warping incluso en ABS",
            "MATERIAL: Imprime en PLA, ABS, PETG*, POLIPROPILENO*, NYLON CON FIBRA DE CARBONO*",
            "CONECTIVIDAD: Se puede conectar a través de Unidad flash USB, Ethernet, WiFi, Cable USB",
            "TAMAÑO EXTERNO (An x Pr x Al): 421 x 433 x 439 mm",
            "CALIBRACIÓN: Autonivelación HRMS",
            "INTERFAZ DE USUARIO: Pantalla táctil a color de 5”",
            "CÁMARA DE MONITORIZACIÓN: a través del PC o celular mediante conexión WiFi",
            "CARGA AUTOMÁTICA DEL CARTUCHO: Carga automática del filamento hasta la boquilla",
            "LUZ INTERNA: Lámpara LED"
        ]},

    {title: "Escaner 3D",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/escaner3d/Imagen1-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/escaner3d/Imagen2.png"
        ],
        description: [
            "Sistemas operativos compatibles: Windows de 64 bits 8 o posteriores",
            "Volumen de escaneo: Min: 0.2m x 0.2m x 0.2m Max: 2m x 2m x 2m",
            "Dimensiones: 12,9 (c) x 17,8 (h) x 3,3 (d) cm",
            "Rango de operación: Min: 0.2m Max: 1,6 m",
            "Profundidad de la imagen: 640 (w) x 480 (h) px",
            "Color de la imagen: 1920 (w) x 1080 (h) px",
            "Campo de visión: Horizontal: 45 ° Vertical: 57,5 ° Diagonal: 69 °",
            "Resolución espacial x / y @ 0,5 m: 0,9 mm",
            "Resolución de profundidad @ 0,5 m: 1mm",
            "Temperatura de funcionamiento: 10-40 ° C",
            "Interfaz de datos: USB 3.0",
            "Longitud del cable USB: 182cm",
            "Rendimiento de imagen máximo: 30 fps"
        ]},
    // {title: "Lápiz 3D",images: drone,
    //     subtitle: "Kit 4 en 1",
    //     description: [
    //         "Peso: 75-80gr.",
    //         "Dimensión máxima: 200x200x46mm.",
    //         "Batería: 800mha.",
    //         "Vuelo estable: Se mantiene suspendido en el aire",
    //         "9 modos de vuelo",
    //         "Conexión remota 2.4 G"
    //     ]},
    {title: "Cargador Solar",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/cargadorSolar/cargador%20solar%20(1)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/cargadorSolar/cargador%20solar%20(2)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/cargadorSolar/cargador%20solar%20(3)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/cargadorSolar/cargador%20solar%20(4)-min.jpg",
        ],
        description: [
            "Batería de 12000 mAh",
            "4 paneles solares para optimizar carga.",
            "1 Salida de 5V x 1A.",
            "1 Salida de 5V x 2A.",
            "1 Entrada Mini USB 5V",
            "Linterna con 3 programas diferentes.",
        ]},
    {title: "Cuaderno inteligente",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/cuaderno/cuadernos%20inteligentes%20(1)-min.jpeg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/cuaderno/cuadernos%20inteligentes%20(1)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/cuaderno/cuadernos%20inteligentes%20(2)-min.jpg",
        ],
        subtitle: "CSE WRITTING PAD",
        description: [
            "Modo de escritura en tiempo real: todo contenido escrito se sincronizará automáticamente con el celular en tiempo real, cuando se conectan por bluetooth.",
            "Segmentación automática del contenido de escritura.",
            "Reproducir de nuevo la pista de escritura en video.",
            "Grabación de sonidos sincronizados al exportar video.",
            "Batería incluida con carga por USB.",
            "Guardar archivos en la nube para compartir en el futuro."
        ]},
    {title: "Holograma",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/holograma/holograma%20(1)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/holograma/holograma%20(2)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/holograma/holograma%20(3)-min.jpg",
        ],
        subtitle: "CSE Z1",
        description: [
            "Puedes cargar fotos y videos personalizados",
            "Consumo: 10w - 15w",
            "Alimentación Transformador: 240v 50/60 hz",
            "Alimentación equipo: 12V 2A",
            "Formatos soportados: mp4, avi, rmvb, mkv, gif, jpg, png.",
            "Memoria sd: 8gb",
            "Cantidad de LEDs: 224",
            "Peso: 0.7kg",
            "Medidas: 420*130*110mm",
            "Incluye software compatible con windows"
        ]},
    {title: "HUAWEI VR2 IMAX",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/imax/realidad%20virtual%20(3)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/imax/realidad%20virtual%20(2)-min.jpg",
        ],
        description: [
            "Teatro IMAX: permite a los usuarios beneficiarse de su propio cine virtual",
            "Resolución 3K: 2 pantallas LCD de 1600 x 1440 píxeles cada una.",
            "Seguimiento: 360°",
            "Campo de visión: 100°",
            "Micrófono: incorporado.",
            "Conectividad: USB y MicroUSB.",
            "Sonido DTS de 360​​° para una experiencia de usuario envolvente.",
            "Versatilidad: es posible acceder desde un smartphone Huawei o desde la PC a la plataforma SteamVR.",
            "Requisitos mínimos: Intel Core i5 + GeForce GTX 1060"
        ]},
    {title: "Oculus Rift",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/oculus/realidad%20virtual%20(1)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/oculus/realidad%20virtual%20(4)-min.jpg",
        ],
        description: [
            "Pantalla OLED 2160×1200 px.",
            "Tasa de refresco de 90 fps.",
            "Angulo de visión: 110º.",
            "Área de seguimiento: 1,5 x 3,3 m.",
            "Sensores de funcionamiento: Acelerómetro, giroscopio, magnetómetro y sistema\n" +
            "Posicional 360 grados.",
            "Peso: 470g"
        ]},
    {title: "Mochila Solar",images: [
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/mochilasolar/mochila%20solar%20(0)-min.jpg",
            "https://storage.googleapis.com/www.cseducacion.com.ar/productos/mochilasolar/mochila%20solar%20(1)-min.jpg",
        ],
        description: [
            "Mochila solar de policristalino",
            "Panel solar monocristalinos",
            "Tamaño: 53*39*17 cm",
            "Panel solar: 4W(5V*800MA)",
            "Material del bolso: 600D",
            "Con entrada mini USB y salida USB",
            "Cargador con batería de 10000MAH"
        ]}
];
