import React from "react";
import {GET_CONFIG, GET_CONFIG_FULFILLED, GET_COURSES, GET_COURSES_FULFILLED} from "./actionTypes";

function importAll(r) {
    let images = [];
    r.keys().map((item, index) => {
        images[index] = r(item);
        return item
    });
    return images;
}

//const lapiz = importAll(require.context('../../assets/images/products/lapiz', false, /\.(png|jpe?g)$/));

/*
* Visor HDMI ?
* Realidad Virtual Oculus Rift -
* Realidad Virtual Huawei IMAX -
* Mochila solar -
* Impresora TECCOM PRO -
* Holograma CSE -
* Escaner 3D -
* Cuaderno Inteligente
* CSE Humanoide -
* Cargador Solar -
* Kit Robótica -
* Drone
* Lapiz 3d
* */


const disenos3Deducativos = importAll(require.context('../../assets/images/courses/disenos3Deducativos', false, /\.(png|jpe?g)$/));
const droneEnElCampo = importAll(require.context('../../assets/images/courses/droneEnElCampo', false, /\.(png|jpe?g)$/));
const impresion3d = importAll(require.context('../../assets/images/courses/impresion3dFueraYdentroDelAula', false, /\.(png|jpe?g)$/));
const inteligenciaArtificial = importAll(require.context('../../assets/images/courses/inteligenciaArtificial', false, /\.(png|jpe?g)$/));
const introduccionProgramacion = importAll(require.context('../../assets/images/courses/introduccionProgramacion', false, /\.(png|jpe?g)$/));
const introduccionRoboticaEducativa = importAll(require.context('../../assets/images/courses/introduccionRoboticaEducativa', false, /\.(png|jpe?g)$/));
const pensamientoComputacional = importAll(require.context('../../assets/images/courses/pensamientoComputacional', false, /\.(png|jpe?g)$/));
const primerDrone = importAll(require.context('../../assets/images/courses/primerDrone', false, /\.(png|jpe?g)$/));
const primerRobotAvanzado = importAll(require.context('../../assets/images/courses/primerRobotAvanzado', false, /\.(png|jpe?g)$/));
const primerRobotBasico = importAll(require.context('../../assets/images/courses/primerRobotBasico', false, /\.(png|jpe?g)$/));
const primerRobotIntermedio = importAll(require.context('../../assets/images/courses/primerRobotIntermedio', false, /\.(png|jpe?g)$/));
const programacionAvanzada = importAll(require.context('../../assets/images/courses/programacionAvanzada', false, /\.(png|jpe?g)$/));
const programacionIntermedia = importAll(require.context('../../assets/images/courses/programacionIntermedia', false, /\.(png|jpe?g)$/));
const realidadVirtual = importAll(require.context('../../assets/images/courses/realidadVirtual', false, /\.(png|jpe?g)$/));
const robotInicial = importAll(require.context('../../assets/images/courses/roboticaInicial', false, /\.(png|jpe?g)$/));
const roboticaOrientadaAi = importAll(require.context('../../assets/images/courses/roboticaOrientadaAi', false, /\.(png|jpe?g)$/));
const roboticaParaPrimaria = importAll(require.context('../../assets/images/courses/roboticaParaPrimaria', false, /\.(png|jpe?g)$/));
const roboticaParaSecundaria = importAll(require.context('../../assets/images/courses/roboticaParaSecundaria', false, /\.(png|jpe?g)$/));

export const getInitialConfigAction = async (dispatch,getState) => {
    dispatch({type: GET_CONFIG});

    const courses = {
        programacion : {
            key: "programacion",
            name:"Programación" ,
            colors: {primary:"violet",secondary: "pink"},
            image
                :
                "https://i1.wp.com/virtualeduca.org/mediacenter/wp-content/uploads/2019/02/CEREBRO.png?fit=1000%2C660&ssl=1",
            courses
                :
                [
                    {
                        name: "Introducción a la Programación",
                        description: "La programación resulta un campo de conocimiento fundamental para el presente y el futuro de la humanidad ya que ayuda a desarrollar innovaciones que permiten solucionar diversas problemáticas sociales.Esto incluye avances en la medicina, la comunicación, la industria, la producción y la economía, e incluso, en tareas de rescate o exploración. Nosotros aprenderemos a programar con bloques y jugando. Muchas personas piensan que aprender a programar es aburrido, estructurado, que tienen mucha matemática difícil o simplemente no les interesa porque no le encuentran utilidad. En este curso conoceremos diferentes aplicaciones y sitios web donde a través del juego y la capacidad de desarrollar soluciones para pasar de nivel en nivel iremos aprendiendo la lógica de la programación, para conocer de qué se trata, cómo la aplico a mi materia y qué necesito para llevarlo al aula.",
                        images: introduccionProgramacion,
                        cost: "ARS 2500 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?", answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuando tengo tiempo de anotarme?", answer: "Cada dos meses lanzamos campaña de inscripción, tu puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Si yo ya se programar con bloques tengo que hacer el curso?", answer: "No, el curso está orientado a los docentes que no han programado nunca, recomendamos entonces inscribirse en el siguiente curso sobre “Hacer mi primer robot”."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?", answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                            name: "Programación Intermedia",
                            images: programacionIntermedia,
                            cost: "ARS $2500 mensual",
                            duration: "2 meses",
                            target: "Docentes de los 3 niveles educativos",
                            description: "Este curso es la continuación del nivel básico, en este tiempo conoceremos el código para programar diversas aplicaciones. Es una introducción desafiante al mundo web, utilizando herramientas Microsoft como C# .net y SQL Server. Alrededor del mundo entero se buscan personas expertas en este campo, y este curso es ideal para empezar.",
                            videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                            faqs: [
                                {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                                {question: "¿Cómo me puedo anotar?", answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante.\n"},
                                {question: "¿Tiene correlatividad?", answer: "Si, primero deberás hacer el curso de “Introducción a la Programación”."},
                                {question: "¿Si yo ya se programar con bloques tengo que hacer el curso?", answer: "Si, ya que en este curso comenzaremos a programar en código de Arduino."},
                                {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?", answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."},
                            ],
                            share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },{
                            name: "Programación Avanzada",
                            images: programacionAvanzada,
                            cost: "ARS $2500 mensual",
                            duration: "2 meses",
                            target: "Docentes de los 3 niveles educativos",
                            description: "Este curso es la continuación del nivel básico, en este tiempo conoceremos el código para programar diversas aplicaciones. Es una introducción desafiante al mundo web, utilizando herramientas Microsoft como C# .net y SQL Server. Alrededor del mundo entero se buscan personas expertas en este campo, y este curso es ideal para empezar.",
                            videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                            faqs: [
                                {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                                {question: "¿Cómo me puedo anotar?", answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante.\n"},
                                {question: "¿Tiene correlatividad?", answer: "Si, primero deberás hacer el curso de “Programación Intermedia”."},
                                {question: "¿En este curso también trabajaremos con Arduino?", answer: "Sí, pero no será la prioridad, ya que el foco principal será la programación orientada a objetos."},
                                {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?", answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."},
                            ],
                            share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Pensamiento Computacional",
                        description: "No pensar en los demás es muy común hoy en día. Pensado para promover la ayuda e integración del prójimo, el pensamiento computacional propone pensar en problemas de la escuela, el barrio y la ciudad y, al mismo tiempo, pensar en una solución que involucre algún medio de procesamiento de información para resolverlo. Usar la tecnología como medio de integración social y desarrollo sustentable.",
                        images: pensamientoComputacional,
                        cost: "ARS $3000 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?", answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?", answer: "Cada dos meses lanzamos campaña de inscripción, tu puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar algún material especial para asistir a clase?", answer: "No, en el curso vamos a utilizar las herramientas del Espacio Maker para trabajar."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?", answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta. "}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    }
                ]
        },
        robotic: {
            key: "robotic",
            name:"Robótica" ,
            colors: {primary: "rgba(240,89,38,0.8)",secondary: "orange"},
            image
                :
                "https://i1.wp.com/virtualeduca.org/mediacenter/wp-content/uploads/2019/02/CEREBRO.png?fit=1000%2C660&ssl=1",
            courses
                :
                [
                    {
                        name: "Introducción a la robótica educativa",
                        description: "Primero pasos en el mundo de la robótica.",
                        images: introduccionRoboticaEducativa,
                        cost: "ARS 3000 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?",answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?",answer: "Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar un kit de robótica?",answer: "No, nosotros proveemos los kits para que puedan trabajar en clase."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Mi primer robot (nivel básico)",
                        description: "La robótica está al alcance de su mano, y puedes trabajar múltiples contenidos educativos en un solo proyecto interdisciplinario. Qué importancia tiene la robótica en el mundo actual, cómo podemos conocer la robótica haciendo, qué beneficios educativos trae esta tecnología, el verdadero trabajo en equipo, asignación de funciones dentro de un grupo de trabajo. El objetivo será realizar el primer proyecto de robótica en la escuela con contenido curricular.",
                        images: primerRobotBasico,
                        cost: "ARS 3500 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Tengo que tomar apunte durante el curso?",answer: "Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?",answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?",answer: "Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar un kit de robótica?",answer: "No, en el curso vamos a crear un robot sin necesidad de un kit."},
                            {question: "¿Tengo que comprar los componentes de electrónica?",answer: "No, nosotros te proveemos de los materiales de electrónica necesarios para desarrollar el curso."},
                            {question: "¿Una vez finalizado el curso me puedo quedar con el robot?",answer: "Si, además de conocimiento te vas a llevar tu robot de souvenir."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Trabajando con robótica (nivel intermedio)",
                        description: "Propuesta para integrar la robótica con la tecnología BlueTooth.",
                        images: primerRobotIntermedio,
                        cost: "ARS 3500 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Tengo que tomar apunte durante el curso?",answer: "Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?",answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?",answer: "Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar un kit de robótica?",answer: "No, en el curso vamos a crear un robot sin necesidad de un kit."},
                            {question: "¿Tengo que comprar los componentes de electrónica?",answer: "No, nosotros te proveemos de los materiales de electrónica necesarios para desarrollar el curso."},
                            {question: "¿Una vez finalizado el curso me puedo quedar con el robot?",answer: "Si, además de conocimiento te vas a llevar tu robot de souvenir."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Trabajando con robótica (nivel avanzado)",
                        description: "Propuesta para integrar la robótica con la tecnología Wi Fi.",
                        images: primerRobotAvanzado,
                        cost: "ARS 3500 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Tengo que tomar apunte durante el curso?",answer: "Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?",answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?",answer: "Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar un kit de robótica?",answer: "No, en el curso vamos a crear un robot sin necesidad de un kit."},
                            {question: "¿Tengo que comprar los componentes de electrónica?",answer: "No, nosotros te proveemos de los materiales de electrónica necesarios para desarrollar el curso."},
                            {question: "¿Una vez finalizado el curso me puedo quedar con el robot?",answer: "Si, además de conocimiento te vas a llevar tu robot de souvenir."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Robótica Educativa para Inicial",
                        description: "Propuesta para integrar la robótica con la tecnología Wi Fi.",
                        images: robotInicial,
                        cost: "ARS 3000 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Tengo que tomar apunte durante el curso?",answer: "Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?",answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?",answer: "Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar un kit de robótica?",answer: "No, nosotros proveemos los kits para que puedan trabajar en clase."},
                            {question: "¿Tengo que comprar algún material especial para asistir a clase?",answer: "No, en el curso vamos a utilizar las herramientas del Espacio Maker para trabajar."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Robótica Educativa orientada a AI",
                        description: "Propuesta para integrar la robótica con la tecnología Wi Fi.",
                        images: roboticaOrientadaAi,
                        cost: "ARS 3000 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Tengo que tomar apunte durante el curso?",answer: "Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?",answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Tiene correlatividad con algún otro curso?",answer: "SI, el curso está orientado a los docentes que han realizado los cursos de “Introducción a la programación” y, “Hacé tu primer robot” o “Introducción a la robótica educativa”."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?",answer: "Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar un kit de robótica?",answer: "No, nosotros proveemos los kits para que puedan trabajar en clase."},
                            {question: "¿Tengo que comprar algún material especial para asistir a clase?",answer: "No, en el curso vamos a utilizar las herramientas del Espacio Maker para trabajar."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Robótica Educativa para Primaria",
                        description: "Propuesta para integrar la robótica con la tecnología Wi Fi.",
                        images: roboticaParaPrimaria,
                        cost: "ARS 3000 mensual",
                        duration: "2 meses",
                        target: "Docentes de nivel Primario.",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?",answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?",answer: "Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar un kit de robótica?",answer: "No, nosotros proveemos los kits para que puedan trabajar en clase."},
                            {question: "¿Tengo que comprar algún material especial para asistir a clase?",answer: "No, en el curso vamos a utilizar las herramientas del Espacio Maker para trabajar."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Robótica Educativa para Secundaria",
                        description: "Propuesta para integrar la robótica con la tecnología Wi Fi.",
                        images: roboticaParaPrimaria,
                        cost: "ARS 3000 mensual",
                        duration: "2 meses",
                        target: "Docentes de nivel Secudario.",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question: "¿Cómo me puedo anotar?",answer: "Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question: "¿Hasta cuándo tengo tiempo de anotarme?",answer: "Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question: "¿Tengo que comprar un kit de robótica?",answer: "No, nosotros proveemos los kits para que puedan trabajar en clase."},
                            {question: "¿Tengo que comprar algún material especial para asistir a clase?",answer: "No, en el curso vamos a utilizar las herramientas del Espacio Maker para trabajar."},
                            {question: "¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer: "Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    }
                ],
        },
        threeDPrint: {
            key: "threeDPrint",
            name:"Impresión y diseño 3D" ,
            colors: {primary: "pink",secondary: "#ff8a56"},
            image
                :
                "https://i1.wp.com/virtualeduca.org/mediacenter/wp-content/uploads/2019/02/CEREBRO.png?fit=1000%2C660&ssl=1",
            courses
                :
                [
                    {
                        name: "Diseños en 3D súper fáciles",
                        description: "Tinkercad, scanner 3D y tendrás tu idea e invento en la mano en cuestión de minutos.  En este curso aprenderemos sobre la historia de las impresoras 3D, cómo fueron integradas durante años en el mundo y cómo fue cambiando el uso en función del desarrollo y del costo. Cómo se compone una impresora 3D y su lógica de funcionamiento. Qué cuidados debemos tener como docentes en su uso dentro y fuera del aula. Archivos compatibles con impresoras 3D y software de diseño que podemos utilizar. Finalmente a través del software Tinkercad y un escáner 3D cada docente diseña y manda a imprimir su propio objeto.",
                        images: disenos3Deducativos,
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        //faqs: [{question: "¿How do you turn this on?", answer: "Press enter"}],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Conceptos básicos para impresión 3D dentro y fuera del aula",
                        description: "Distintos software para diseñar en 3D, herramienta esencial para resolver todo tipo de problemas. Cómo docentes es muy importante que seamos capaces de desarrollar diversos proyectos con los alumnos donde ellos sean capaces de pensar en un problema del barrio, la escuela o el país, y al mismo tiempo desarrollen su solución. Para ello cada lo hará en este curso y obtendrá un objeto o producto final con una funcionalidad importante para la sociedad actual.",
                        images: impresion3d,
                        cost: "ARS 2750 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question:"¿Tengo que tomar apunte durante el curso?",answer:"Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question:"¿Cómo me puedo anotar?",answer:"Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question:"¿Hasta cuándo tengo tiempo de anotarme?",answer:"Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question:"¿Tengo que comprar una impresora 3D?",answer:"No, en el curso vamos a tener a disposición varias impresoras 3D de última generación para usar."},
                            {question:"¿Tengo que comprar el PLA o ABS para asistir a clase?",answer:"No, nosotros te proveemos de toda la materia prima necesaria para imprimir en 3D."},
                            {question:"¿Una vez finalizado el curso me puedo quedar con mis impresiones?",answer:"Si, además de conocimiento te vas a llevar tu impresión de souvenir."},
                            {question:"¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer:"Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    }
                ],
        },
        drone: {
            key: "drone",
            name:"Drone" ,
            colors: {primary: "#b6b894",secondary: "#888888"},
            image
                :
                "https://i1.wp.com/virtualeduca.org/mediacenter/wp-content/uploads/2019/02/CEREBRO.png?fit=1000%2C660&ssl=1",
            courses
                :
                [
                    {
                        name: "Hacé tu primer drone",
                        description: "Diseñá, pintá y construí tu primer drone de madera. Llevate a casa tu primer drone hecho a medida por vos mismo!",
                        images: primerDrone,
                        cost: "ARS 4000 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question:"¿Tengo que tomar apunte durante el curso?",answer:"Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question:"¿Cómo me puedo anotar?",answer:"Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question:"¿Hasta cuándo tengo tiempo de anotarme?",answer:"Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question:"¿Tengo que comprar un drone para asistir?",answer:"No, con el pago del curso incluye un drone armable y programable para vos."},
                            {question:"¿Una vez finalizado el curso me puedo quedar con el drone que armé?",answer:"Si, además de conocimiento te vas a llevar tu drone a casa."},
                            {question:"¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer:"Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    },
                    {
                        name: "Drone en el campo",
                        description: "Guía avanzada para aprender utilizar tu drone en campo abierto y conocer sus límites.",
                        images: droneEnElCampo,
                        cost: "ARS 4000 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question:"¿Tengo que tomar apunte durante el curso?",answer:"Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question:"¿Cómo me puedo anotar?",answer:"Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question:"¿Hasta cuándo tengo tiempo de anotarme?",answer:"Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question:"¿Tengo que comprar un drone para asistir?",answer:"No, como este curso es correlativo a “Hacé tu primer Drone” ya deberías tener el tuyo."},
                            {question:"¿Cuál es la diferencia con el curso anterior?",answer:"El foco de este curso es poder programar el Drone para que cumpla con un objetivo específico, en este caso el uso que se le puede dar dentro del campo.\n"},
                            {question:"¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer:"Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    }
                ],
        },
        artificialIntelligence: {
            key: "artificialIntelligence",
            name:"Inteligencia Artificial" ,
            colors: {primary: "#e91e63c9",secondary: "#ff9c8b"},
            image
                :
                "https://i1.wp.com/virtualeduca.org/mediacenter/wp-content/uploads/2019/02/CEREBRO.png?fit=1000%2C660&ssl=1",
            courses
                :
                [
                    // {
                    //     name: "Cómo introducir la AI en el aula",
                    //     description: "En el mundo que nos desarrollamos la AI cada día es más utilizada, por lo tantos es muy importante que los alumnos sepan muy bien de qué se trata, cómo funciona y qué limitaciones presenta al día de hoy.",
                    //     images: inteligenciaArtificial,
                    //     videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                    //     //faqs: [{question: "¿How do you turn this on?", answer: "Press enter"}],
                    //     share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    // },
                    {
                        name: "Aprendiendo con Realidad Virtual",
                        description: "La principal ventaja de la tecnología es que ya no tenemos que imaginar para aprender, poder recrear espacios virtuales para recorrer y conocer desde dentro.",
                        images: realidadVirtual,
                        cost: "ARS 3000 mensual",
                        duration: "2 meses",
                        target: "Docentes de los 3 niveles educativos",
                        videos: ["https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"],
                        faqs: [
                            {question: "¿Qué modalidad tiene el curso?", answer: "La modalidad es presencial, ya que nos reuniremos todas las semanas a trabajar y además habrá material subido a una plataforma educativa."},
                            {question:"¿Tengo que tomar apunte durante el curso?",answer:"Prestar atención en la nueva información, manipular las nuevas herramientas y tomar apunte al mismo tiempo no hará falta, ya que subiremos todo el material clase a clase en la plataforma educativa."},
                            {question:"¿Cómo me puedo anotar?",answer:"Te puedes inscribir completando el formulario de pre-inscripción y luego te enviaremos el cupón de pago para confirmar la reserva de vacante."},
                            {question:"¿Hasta cuándo tengo tiempo de anotarme?",answer:"Cada dos meses lanzamos campaña de inscripción, tú puedes elegir el bimestre que quieres anotarte ya que los cupos son limitados."},
                            {question:"¿Tengo que comprar algún material especial para asistir a clase?",answer:"No, en el curso vamos a utilizar los lentes de Realidad Virtual del Espacio Maker para trabajar."},
                            {question:"¿Cuándo termine el curso podré seguir entrando a la plataforma educativa?",answer:"Sí, pero una vez que termines el curso, pasaremos a todos los ex alumnos interesados al sector de retroalimentación, donde podrán compartir allí lo que van haciendo en su labor docente con lo aprendido o compartiendo alguna duda para que dentro de la comunidad puedan ayudar a resolver con esa consulta."}
                        ],
                        share: [{icon: "facebook", link: "https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"}]
                    }
                ],
        }
    };

    dispatch({type: GET_CONFIG_FULFILLED,payload: {courses}});
};