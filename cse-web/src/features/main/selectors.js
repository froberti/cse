import NAME from "./constants";

export const isLoadingConfig = (store) => store[NAME].loadingConfig;

export const getCourses = (store) => store[NAME].courses;

export const getThemes = (store) => store[NAME].courses;

export const getProducts = (store) => store[NAME].products;