import React from "react";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import Typography from "@material-ui/core/Typography";
import {strings} from "../../../localization/strings";
import Mask from "../../../common/masks/mask";
import Course from "../../../common/pages/course";
import Parallax from "../../../common/parallax";
import HightlightText from "../../../common/pages/highlightText";
import scrollToComponent from 'react-scroll-to-component';
import ShareLinks from "../../../common/shareLinks";
import {getInitialConfigAction} from "../actions";
import {getCourses, getProducts, isLoadingConfig} from "../selectors";
import {routes} from "../../../router/routes";
import OgiLogo from "../../../assets/images/ogi-logo.png";
import NewTechLogo from "../../../assets/images/new-tech-logo.png";
import ParallaxImage from "../../../assets/images/parallax.jpeg";
import { TimelineLite, CSSPlugin, AttrPlugin }  from "gsap/all";
import Grid from "@material-ui/core/Grid/Grid";
import TonyGif from "../../../assets/gifs/tony.gif";
import ProductModal from "../../../common/pages/productModal";
import MainIcons from "../../../common/pages/main-icons";
import Footer from "../../../common/footer";

const plugins = [ CSSPlugin, AttrPlugin ];

const refNames = {
    products: "Products"
};

class MainPage extends React.Component{
    state={productModalOpen: false,productSelected:null};

    handleCloseProductModal = () => { this.setState({productModalOpen:false})};
    handleOpenProductModal = (product) => { this.setState({productModalOpen:true,productSelected:product}) };

    render(){
        const {productModalOpen,productSelected} = this.state;
        const {courses,history,products,loadingConfig} = this.props;
        return <React.Fragment>
            <section id={"main-page"} className={"page first center-all-content"}>

                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/eBdpBPP7m88?autoplay=1&loop=1&controls=0&mute=1&playlist=GRonxog5mbw" frameBorder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen id="main-video" ></iframe>
                {/*<img src={MainVideo} alt=""/>*/}
                {/*<video preload autoPlay id="main-video" loop={"loop"} muted >*/}
                    {/*<source src="https://drive.google.com/uc?export=download&id=1mR-SUpBTBqC-P8hBshgiNxBi3doyrfkQ" type="video/mp4"/>*/}
                {/*</video>*/}
                <div className="centered-box main-box"/>
                <div className="titles-container">
                    <Typography variant={"h3"} align={"center"} className={"transparent-text"}>
                        {strings.mainTitle}
                    </Typography>
                    <Typography variant={"h5"} align={"center"} className={"subtitle"}>
                        {strings.mainSubtitle}
                    </Typography>
                </div>

                <Mask/>
            </section>

            <ShareLinks links={[
                {icon: "facebook",link:"https://www.facebook.com/CSE.SMART.SERVICE/",title:"Facebook"},
                {icon: "instagram",link:"https://www.instagram.com/c.s.educacion/",title:"Instagram"},
                {icon: "linkedin",link:"https://www.linkedin.com/in/alan-climent-56511111b",title: "Linkedin"},
                {icon: "twitter",link:"https://twitter.com/@cse89634907",title:"Twitter"},
                {icon: "whatsapp",link:"https://wa.me/5491159231641",title:"Whatsapp"},
                // {icon: "youtube",link:"/youtube",title:"Youtube"},
            ]}/>

            <MainIcons
                icons={[]}
                history={history}
                scrollToProducts={this.scrollToRef(refNames.products)}
            />
            {/*<Items/>*/}

            {/*<div className={"button-container see-programs"}>*/}
                {/*<div>*/}

                {/*<Typography variant={"h6"} align={"center"} className={"white-colored"}>*/}
                    {/*{strings.toKnowMoreAboutPrograms}*/}
                {/*</Typography>*/}
                {/*<div className="custom-divider"/>*/}
                {/*<Button variant={"contained"} className={"color-fade-background"} size="large" onClick={()=>history.push(routes.placa.path)} >*/}
                    {/*{strings.seePrograms}*/}
                {/*</Button>*/}
                {/*</div>*/}
            {/*</div>*/}

            <Course title={"Espacio Maker"}
                    subtitle={"Desarrollamos cursos para docentes de los distintos niveles educativos y materias. Fomentamos la inclusión de las nuevas herramientas digitales para acceso al contenido para mejorar la calidad educativa en la sociedad del siglo 21."}
                    themes={courses}
                    handleSeeMoreClick={()=>{window.scrollTo(0,0); history.push(routes.themes.path)}}
                    handleCourseClick={(theme,course)=> {window.scrollTo(0,0);history.push(routes.themes.path + `/${theme}/${course}`)}}
            />

            <Parallax
                text={"Tecnología para la educación"}
                image={ParallaxImage}/>

            <Typography variant={"h3"} align={"center"} style={{marginTop: 15}} className={"title grey-colored"}>
                Empresas que nos apoyan
            </Typography>
            <HightlightText text={"OGI Technologies"}
                            image={OgiLogo}
                            link={"http://www.ogitech.es/"}
                            subtitle={"Tiene todos los componentes para la solución completa de la Educación."}/>

            <HightlightText text={"Nuevas Tecnologías para la educación"}
                            image={NewTechLogo}
                            link={"http://www.ntpe.es/"}
                            subtitle={"Proporciona e integra las tecnologías digitales para colaborar en la preparación de profesores y estudiantes para un futuro, basado en el profundo conocimiento de las instituciones educativas."}/>


            <div className={"main-gif-container"}>
                <img src={TonyGif} className={"main-gif"} alt="VrGif"/>
            </div>


            <div id={"products"} ref={this.setRef(refNames.products)} style={{padding:"35px 0 20px 0"}} >
                <div className={"common-container"}>

                    <div className="text-container">
                        <Typography variant={"h2"} align={"center"} className={"title grey-colored"}>Productos</Typography>
                        <Typography variant={"subtitle1"} align={"center"} className={"title grey-colored"}>Calidad para tu institución educativa</Typography>
                    </div>
                    <Grid container className={"products-container"}>
                        {
                            products.map( (product,key) =>
                                <Grid item xs={12} sm={6} md={4} className="product shadow-on-hover"
                                      key={key}
                                      onClick={()=>{ this.handleOpenProductModal(product) }}
                                      style={{background: product.images[0]}}>
                                    <Typography variant={"h4"} align="center" className={"product-title grey-colored"}>
                                        {product.title}
                                    </Typography>
                                    <img src={product.images[0]} alt={product.title}/>
                                </Grid>)
                        }
                    </Grid>
                </div>
            </div>
            <ProductModal
                open={productModalOpen}
                product={productSelected}
                handleClose={this.handleCloseProductModal}
            />
            <Footer/>
        </React.Fragment>
    };

    componentWillUnmount(){ clearInterval(this.interval) }

    scrollToRef = name => () => {
        setTimeout( ()=> scrollToComponent(this[name], { offset: 0, align: 'top' }),100 )
    };
    setRef = name => section => {
        this[name] = section
    };
    componentDidMount(){
        //this.interval= setInterval(function(){
           // document.getElementById('main-video').load();
            //document.getElementById('main-video').play();
        //},14000);

        const {dispatch} = this.props;
        dispatch(getInitialConfigAction);
        let tl = new TimelineLite();
        tl.to(".transparent-text", 1.5, {fontSize:60,opacity:1});
        tl.to(".main-box",2,{opacity:0.5});
        tl.to(".subtitle",2,{opacity:1});
    }
}

export default connect(createStructuredSelector({
    courses: getCourses,
    products: getProducts,
    loadingConfig: isLoadingConfig
}))(MainPage)