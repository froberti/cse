
import {
    GET_CONFIG,
    GET_CONFIG_FULFILLED, GET_CONFIG_REJECTED,
    GET_COURSES,
    GET_COURSES_FULFILLED,
    GET_COURSES_REJECTED
} from "./actionTypes";
import {products} from "./model";

const initialState = {
    loadingConfig: false,
    products: products,
    courses: null,
};

export default function reducer(state=initialState, action){

    switch (action.type){
        case GET_COURSES: return {...state,gettingCourses:true};
        case GET_COURSES_FULFILLED: return {...state, gettingCourses: false, courses: action.payload};
        case GET_COURSES_REJECTED: return {...state,gettingCourses:false};

        case GET_CONFIG: return {...state,loadingConfig:true};
        case GET_CONFIG_FULFILLED: return {...state,loadingConfig: false, courses: action.payload.courses};
        case GET_CONFIG_REJECTED: return {...state,loadingConfig:false};

        default: return state
    }
}