import reducer from "./reducer";
export {default as MainPage} from "./components/index";
export {default as mainReducerName} from "./constants";
export default reducer;