import React from "react"
import Placa from "../../assets/images/placa.jpeg";

const PlacaPage = () =>
    <div id={"placa-page"}>
        <div className={"placa-image-container"}>
            <img className="placa-image" src={Placa} alt="Placa"/>
        </div>
    </div>;

export default PlacaPage