import React from "react";
import { Route,  Switch} from "react-router-dom";
import ProductPage from "./product";
import SelectProductPage from "./selectProduct";

const themeRoutes = [
    {path: "/productos/:product",component: ProductPage},
    {path: "/productos",component: SelectProductPage},
];

const ThemesRouter = () =>
    <Switch>
        {
            themeRoutes.map( route =>
                <Route key={route.path}
                       path={route.path}
                       component={route.component}/>
            )
        }
    </Switch>;

export default ThemesRouter