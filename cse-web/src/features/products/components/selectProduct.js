import React from "react";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {getProducts, isLoadingConfig} from "../../main/selectors";
import {getInitialConfigAction} from "../../main/actions";
import Loader from "../../../common/loader/logo";
import Typography from "@material-ui/core/Typography";
import {strings} from "../../../localization/strings";
import {routes} from "../../../router/routes";
import Grid from "@material-ui/core/Grid";

class SelectProductPage extends React.Component{
    render(){
        const {products,loading,history} = this.props;
        return <section id={"select-product"} className={"page"}>
            {loading && <Loader/>}
            {!loading && products &&
            <React.Fragment>
                <Typography variant={"h3"} align={"center"}>{strings.selectProduct}</Typography>
                <Grid container className={"products-container"}>
                    {
                        products.map( (product,key) =>
                            <Grid item xs={12} sm={6} md={4} className="product shadow-on-hover"
                                 key={key}
                                 onClick={()=>{window.scrollTo(0,0); history.push(routes.products.path+`/${key}`)}}
                                 style={{background: product.images[0]}}>
                                <Typography variant={"h4"} align="center" className={"product-title"}>
                                    {product.title}
                                </Typography>
                                <img src={product.images[0]} alt={product.title}/>
                            </Grid>
                        )
                    }
                </Grid>
            </React.Fragment>
            }
        </section>
    };

    componentDidMount(){
        const {dispatch,products} = this.props;
        if(!products)
            dispatch(getInitialConfigAction);
    }
}

export default connect(createStructuredSelector({
    products: getProducts,
    loading: isLoadingConfig
}))(SelectProductPage)