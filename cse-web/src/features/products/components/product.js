import React from "react";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {getProducts, isLoadingConfig} from "../../main/selectors";
import {getInitialConfigAction} from "../../main/actions";
import Loader from "../../../common/loader/logo";
import Typography from "@material-ui/core/Typography";
//import ImagesSlider from "../../../common/imagesSwiper";
import Grid from "@material-ui/core/Grid";
import CustomSwiper from "../../../common/swiper";

class ProductPage extends React.Component{
    render(){
        const {products,loading} = this.props;
        const {product} = this.props.match.params;

        const selectedProduct = products && products[product];

        return <section id={"course-page"} className={`page center-all-content`}>
            {loading && <Loader/>}

            {
                selectedProduct &&
                <React.Fragment>
                    <Typography variant={"h3"}>{selectedProduct.title}</Typography>
                    <Grid container>
                        <Grid item xs={12}>
                            <CustomSwiper
                                items={
                                    selectedProduct.images.map(image=><img src={image} alt="Product"/>)
                                }
                                containerClass={"image-info"}/>
                        </Grid>
                    </Grid>
                </React.Fragment>
            }
        </section>
    };

    componentDidMount(){
        const {dispatch,products} = this.props;
        if(!products)
            dispatch(getInitialConfigAction);
    }
}

export default connect(createStructuredSelector({
    products: getProducts,
    loading: isLoadingConfig
}))(ProductPage)

