import reducer from "./reducer";
export {default as App} from "./components/index";
export {default as appReducerName} from "./constants";
export default reducer;