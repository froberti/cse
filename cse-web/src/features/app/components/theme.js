import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import CssBaseline from '@material-ui/core/CssBaseline';

// A theme with custom primary and secondary color.
// It's optional.
const theme = createMuiTheme({
    palette: {
        secondary: {
            light: "#ff63ff",
            main: "#fe05f0",
            dark: "#c600bd",
        },
        primary: {
            light: "#ff8b53",
            main: "#F05926",
            dark: "#b62400",
        },
    },
    typography: {
        useNextVariants: true,
    },
});

const Theme = (props) =>
    <MuiThemeProvider theme={theme}>
        <CssBaseline />
        {props.children}
    </MuiThemeProvider>;

export default Theme;