import React from "react";
import Router from "../../../router";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {Toast} from "../../snackbar/index";
import Loader from "../../../common/loader";
import {isLoading} from "../selectors";
import {getLoginResponse} from "../../login/selectors";
import {LoginPage} from "../../login";
import {logOutAction} from "../../login/actions";
import Theme from "./theme";

class App extends React.Component{
    render(){
        const {loading,loggedIn} = this.props;

        return  <Theme>
                    <Router handleLogout={()=>this.props.dispatch(logOutAction)} loggedIn={!!loggedIn}/>
                    <Loader loading={loading}/>
                    <Toast/>
                </Theme>
    };
}

export default connect(createStructuredSelector({
    loggedIn: getLoginResponse,
    loading: isLoading
}))(App)