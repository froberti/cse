// import axios from "axios";
// import {WINDOWS_AUTH_ENDPOINT} from "../../shared/connections";
// import {openErrorToast} from "../snackbar";
// import {WINDOWS_AUTH, WINDOWS_AUTH_FULFILLED, WINDOWS_AUTH_REJECTED} from "./actionTypes";
//
// const getOptions = () => ({
//     method: 'GET',
//     headers: {
//         Accept: 'application/json',
//         'Content-Type': 'application/json'
//     },
//     withCredentials: true
// });
//
//
// export const authorizeAction = async (dispatch,getState) => {
//     dispatch({type:WINDOWS_AUTH});
//     try{
//         const response = await axios.get(WINDOWS_AUTH_ENDPOINT, getOptions());
//         console.log(response.json())
//         dispatch({type: WINDOWS_AUTH_FULFILLED,payload: response.data});
//     }catch(e){
//         dispatch({type: WINDOWS_AUTH_REJECTED});
//         openErrorToast(e);
//         throw(e)
//     }
// };

import {HIDE_LOADER, SHOW_LOADER} from "./actionTypes";

export const showLoader = dispatch => dispatch({type: SHOW_LOADER});
export const hideLoader = dispatch => dispatch({type: HIDE_LOADER});
