import NAME from "./constants";

export const isOnline = (store) => store[NAME].online;
// export const isAuthorized = (store) => !!store[NAME].authorizeResponse;
export const isLoading = (store) => store[NAME].loading;