import NAME from "./constants";

export const WEB_APP_ONLINE = NAME + "/WEB_APP_ONLINE";
// export const WINDOWS_AUTH = NAME + "/WINDOWS_AUTH";
// export const WINDOWS_AUTH_FULFILLED = NAME + "/WINDOWS_AUTH_FULFILLED";
// export const WINDOWS_AUTH_REJECTED = NAME + "/WINDOWS_AUTH_REJECTED";

export const SHOW_LOADER = NAME + "/SHOW_LOADER";
export const HIDE_LOADER = NAME + "/HIDE_LOADER";