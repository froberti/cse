import React from "react";
import {routes} from "../../../router/routes";
import Button from "@material-ui/core/Button";

const ThemesBar = ({themes,history,selectedTheme}) =>
    <div className="themes-bar">
        {
            themes.map( theme =>
                <div className="button-container">
                    <Button variant={"contained"}
                            className={selectedTheme.key === theme.key ?"color-fade-background":""}
                            size="large"
                            onClick={()=>history.push(routes.themes.path+"/"+theme.key)} >
                        {theme.name}
                    </Button>
                </div>
            )
        }
    </div>;

export default ThemesBar