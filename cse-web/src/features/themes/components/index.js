import React from "react";
import { Route,  Switch} from "react-router-dom";
import CoursePage from "./course/course";
import ThemePage from "./theme";
import SelectThemePage from "./selectTheme";

const themeRoutes = [
    {path: "/temas/:theme/:course",component: CoursePage},
    {path: "/temas/:theme",component: ThemePage},
    {path: "/temas",component: SelectThemePage},
];

const ThemesRouter = () =>
    <Switch>
        {
            themeRoutes.map( route =>
                <Route key={route.path}
                       path={route.path}
                       component={route.component}/>
            )
        }
    </Switch>;

export default ThemesRouter