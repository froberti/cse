import React,{useState} from "react";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid/Grid";
import Paper from "@material-ui/core/Paper";
import DefaultCourseImage from "../../../assets/images/main-logo.png";
import {strings} from "../../../localization/strings";
import {Waypoint} from "react-waypoint";
import Tab from "@material-ui/core/Tab/Tab";
import Tabs from "@material-ui/core/Tabs/";
import AppBar from "@material-ui/core/AppBar/";
import SwipeableViews from 'react-swipeable-views';


//Mock
//const themes = [{ name:"Programación" ,courses: [{name: "Curso 1", image: "https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"}] }];

const Course = ({title,subtitle,themes,handleSeeMoreClick,handleCourseClick }) =>{
    const [themeIndex, setIndex] = useState(0);
    const [active, setActive] = useState(false);
    const themeValues = themes && Object.values(themes);

    return <section >
            <Waypoint
                onEnter={() => setActive(true)}
                onLeave={() => setActive(false)}
            >
                <Typography variant={"h2"} className={`animated highlighter ${active? "active":"inactive"}`}>{title}</Typography>
            </Waypoint>


        <AppBar position="static" color="default">
            <Tabs
                value={themeIndex}
                onChange={(e,index)=>{console.log(index); setIndex(index)}}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
            >
                {
                    themeValues && themeValues.map( (theme,key) =>
                        <Tab label={theme.name}/>
                    )
                }
            </Tabs>
        </AppBar>
        <SwipeableViews
            axis={'x'}
            index={themeIndex}
            onChangeIndex={index=>{console.log(index); setIndex(index)}}
        >

            {
                themeValues && themeValues.map( (theme,key) =>
                    <Typography component="div" dir={"ltr"} style={{ padding: 8 * 3 }}>
                        {theme.courses.map( (course,index)=>
                            <Grid item xs={12} sm={6} md={4} className="course" key={themeValues[themeIndex].name+index}>
                                <Paper  className={"course-paper"} onClick={()=>handleCourseClick(themeValues[themeIndex].key,index)}>
                                    <Typography variant={"subtitle1"} className={"course-title"}>{course.name}</Typography>
                                    <img style={{width:150}} src={(course.images && course.images[0]) || DefaultCourseImage} alt={course.name}/>
                                </Paper>
                            </Grid>)
                        }
                    </Typography>
                )
            }
        </SwipeableViews>

    </section>
};

export default Course