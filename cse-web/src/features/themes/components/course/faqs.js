import React from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const Faqs = ({faqs}) =>
    <div className={"faqs-container"}>
        {
            faqs.map(faq =>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography variant={"h6"}>{faq.question}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography variant={"subtitle1"}>
                            {faq.answer}
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            )
        }
    </div>;

export default Faqs