import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import CashIcon from "@material-ui/icons/AttachMoney";
import TimeIcon from "@material-ui/icons/Timelapse";
import TargetIcon from "@material-ui/icons/Check";

const Blob = ({subtitle,text,icon}) =>
    <div className={"blob"}>
        <div className={"icon-container"}>
            {icon}
        </div>
        <Typography variant={"h5"}>
            {subtitle}
        </Typography>
        <Typography variant={"subtitle1"} align={"center"}>
            {text}
        </Typography>
    </div>;

const CourseDescription = ({cost,duration,target}) =>
    <Grid container className={"course-blob-container"}>
        { cost &&
            <Grid item xs={12} md={6} lg={4}>
                <a href="https://wa.me/5491159231641" target={"_blank"} style={{textDecoration:"none"}}>
                    <Blob text={""} subtitle={"Pedí tu cotización!"} icon={<CashIcon/>}/>
                </a>
                {/*<Blob text={cost} subtitle={"Costo mensual"} icon={<CashIcon/>}/>*/}
            </Grid>
        }
        { duration &&
            <Grid item xs={12} md={6} lg={4}>
                <Blob text={duration} subtitle={"Duración"} icon={<TimeIcon/>}/>
            </Grid>
        }
        { target &&
            <Grid item xs={12} md={6} lg={4}>
                <Blob text={target} subtitle={"Dirigido a"} icon={<TargetIcon/>}/>
            </Grid>
        }
    </Grid>;

export default CourseDescription