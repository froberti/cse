import React from "react";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {getThemes, isLoadingConfig} from "../../../main/selectors";
import {getInitialConfigAction} from "../../../main/actions";
import Loader from "../../../../common/loader/logo";
import Typography from "@material-ui/core/Typography";
import ShareLinks from "../../../../common/shareLinks/index";
import Faqs from "./faqs";
import ImagesSlider from "../../../../common/imagesSwiper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/es/Button/Button";
import {routes} from "../../../../router/routes";
import ThemesBar from "../themesBar";
import CourseDescription from "./description";

class ThemePage extends React.Component{
    render(){
        const {themes,loading,history} = this.props;
        const {theme,course} = this.props.match.params;

        const selectedTheme = themes && themes[theme];
        const selectedCourse = selectedTheme && selectedTheme.courses[course];

        return <section id={"course-page"} className={`page`}
                        style={{background: `linear-gradient(180deg, ${(selectedTheme && selectedTheme.colors.secondary) || "rgba(255, 255, 255, 0.7)"} 60%, ${selectedTheme && selectedTheme.colors.primary} 100%)`}}>
            {loading && <Loader/>}

            <ThemesBar themes={(themes && Object.values(themes)) || []} history={history} selectedTheme={selectedTheme}/>
            {
                selectedCourse &&
                    <React.Fragment>
                        <Typography variant={"h4"} align={"center"} className={"course-title"}>{selectedCourse.name}</Typography>
                        <Grid container className={"course-info-container"}>
                            <Grid item xs={12} md={6}>
                                <ImagesSlider images={selectedCourse.images}/>
                            </Grid>
                            <Grid item xs={12} md={6} className={"course-description-container"}>
                                <div>
                                    <div className={"divider"}/>
                                    <Typography variant={"h6"} className={"description-text"}>
                                        {selectedCourse.description}
                                    </Typography>

                                    <CourseDescription
                                        cost={selectedCourse.cost}
                                        duration={selectedCourse.duration}
                                        target={selectedCourse.target}
                                    />
                                    <div className={"divider"}/>
                                </div>
                                <ShareLinks links={selectedCourse.share}/>
                            </Grid>
                        </Grid>
                        <Faqs faqs={selectedCourse.faqs || []}/>
                    </React.Fragment>
            }
        </section>
    };

    componentDidMount(){
        const {dispatch,themes} = this.props;
        if(!themes)
            dispatch(getInitialConfigAction);
    }
}

export default connect(createStructuredSelector({
    themes: getThemes,
    loading: isLoadingConfig
}))(ThemePage)

