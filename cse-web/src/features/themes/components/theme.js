import React from "react";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {getThemes, isLoadingConfig} from "../../main/selectors";
import {getInitialConfigAction} from "../../main/actions";
import Loader from "../../../common/loader/logo";
import Typography from "@material-ui/core/Typography";
//import {strings} from "../../../localization/strings";
import {routes} from "../../../router/routes";

class ThemePage extends React.Component{
    render(){
        const {themes,loading,history} = this.props;
        const {theme} = this.props.match.params;

        const selectedTheme = themes && themes[theme];

        return <section id={"theme"}
                        className={"page"}
                        style={{background: `linear-gradient(180deg, ${(selectedTheme && selectedTheme.colors.secondary) || "rgba(255, 255, 255, 0.7)"} 0, ${selectedTheme && selectedTheme.colors.primary} 100%)`}}>
            {loading && <Loader/>}

            <Typography variant={"h3"}>{selectedTheme && selectedTheme.name}</Typography>

            <div className={"course-container"} >

                {!loading && themes && selectedTheme &&
                    selectedTheme.courses.map( (course,key) =>
                            <div className="course shadow-on-hover"
                                 key={course.name}
                                 onClick={()=>{window.scrollTo(0,0);history.push(routes.themes.path+`/${theme}/${key}`)}}
                                 style={{background: `url('${(course.images && course.images[0])}')`}}>
                                <Typography variant={"h4"} className={"course-title"}>
                                    {course.name}
                                </Typography>
                                <div className="overlay"/>
                            </div>
                    )
                }
            </div>

        </section>
    };

    componentDidMount(){
        const {dispatch,themes} = this.props;
        if(!themes)
            dispatch(getInitialConfigAction);
    }
}

export default connect(createStructuredSelector({
    themes: getThemes,
    loading: isLoadingConfig
}))(ThemePage)