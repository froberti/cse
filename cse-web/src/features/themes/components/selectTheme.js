import React from "react";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {getThemes, isLoadingConfig} from "../../main/selectors";
import {getInitialConfigAction} from "../../main/actions";
import Loader from "../../../common/loader/logo";
import Typography from "@material-ui/core/Typography";
import {strings} from "../../../localization/strings";
import {routes} from "../../../router/routes";
//import Course from "./selectCourse";

class SelectThemePage extends React.Component{
    render(){
        const {themes,loading,history} = this.props;
        return <section id={"select-theme"} className={"page"}>
            {loading && <Loader/>}


            {/*{*/}
                {/*!loading && themes &&*/}

                    {/*<Course title={"Espacio Maker"}*/}
                            {/*subtitle={"Desarrollamos cursos para docentes de los distintos niveles educativos y materias. Fomentamos la inclusión de las nuevas herramientas digitales para acceso al contenido para mejorar la calidad educativa en la sociedad del siglo 21."}*/}
                            {/*themes={themes}*/}
                            {/*handleSeeMoreClick={()=>{window.scrollTo(0,0); history.push(routes.themes.path)}}*/}
                            {/*handleCourseClick={(theme,course)=> {window.scrollTo(0,0);history.push(routes.themes.path + `/${theme}/${course}`)}}*/}
                    {/*/>*/}
            {/*}*/}


            {!loading && themes &&
                <React.Fragment>
                    <Typography variant={"h3"}>{strings.selectTheme}</Typography>
                    <div className={"themes-container"}>
                        {
                            Object.values(themes).map( theme =>
                                <div className="theme shadow-on-hover"
                                     key={theme.name}
                                     onClick={()=>{window.scrollTo(0,0); history.push(routes.themes.path+`/${theme.key}`)}}
                                     style={{background: theme.colors.primary}}

                                //style={{background: `linear-gradient(180deg, ${theme.colors.secondary || "rgba(255, 255, 255, 0.7)"} 0, ${theme.colors.primary} 100%)`}}
                                >
                                    <Typography variant={"h4"} className={"theme-title"}>
                                        {theme.name}
                                    </Typography>
                                </div>
                            )
                        }
                    </div>
                </React.Fragment>
            }
        </section>
    };

    componentDidMount(){
        const {dispatch,themes} = this.props;
        if(!themes)
            dispatch(getInitialConfigAction);
    }
}

export default connect(createStructuredSelector({
    themes: getThemes,
    loading: isLoadingConfig
}))(SelectThemePage)