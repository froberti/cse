import reducer from "./reducer";
export {default as ThemesPage} from "./components/index";
export {default as themesReducerName} from "./constants";
export default reducer;