import React,{useState} from "react";
import {addPayment} from "../../../shared/api";

const MpButton = ({itemId}) => {
    const [loading,setLoading] = useState(false);

    const handleMpButtonClick = () =>{
        setLoading(true);

        addPayment(itemId)
            .then( r => {
                    window.open(r.data, '_blank');
                }
            )
            .finally( () => {
                setLoading(false)
            })

    };

    return loading? <div>LOADING ...</div>:
        <div onClick={handleMpButtonClick}>
            MP BUTTON
        </div>
};

export default MpButton
