import React,{useState} from "react"
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import ManagerPage from "./index";
import PaymentsPage from "./payments";
import CashIcon from "@material-ui/icons/CreditCard";
import ClassIcon from "@material-ui/icons/Class";

const tabs = {
    managerPage: 0,
    paymentsPage: 1
};

const ManagerNav = () => {
    const [tab,setTab] = useState(0);
    const handleChange = (event, newValue) => {
        setTab(newValue);
    };
    return  <div style={{minHeight:"calc(100vh - 45px)",paddingTop:80}}>
            <Paper>
                <Tabs value={tab} onChange={handleChange}
                      indicatorColor="primary"
                      textColor="primary"
                      centered>
                    <Tab icon={<ClassIcon />} label="Cursos" />
                    <Tab icon={<CashIcon />} label="Pagos" />
                </Tabs>
            </Paper>
            {
                tab === tabs.managerPage &&
                <ManagerPage/>
            }
            {
                tab === tabs.paymentsPage &&
                <PaymentsPage/>
            }
        </div>
};

export default ManagerNav