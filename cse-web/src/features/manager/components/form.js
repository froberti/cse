import React,{useState} from 'react'
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Input from "../../../common/input";
import styled from "styled-components";

const Form = ({handleSubmit,loading,inputs}) => {
    const [formValues,setFormValue] = useState({});
    return (
        <FormContainer>
            {
                inputs.map( i =>
                    <Input
                        {...i}
                        value={formValues[i.name] || ""}
                        onChange={(v)=> setFormValue({...formValues,[i.name]:v})}
                    />
                )
            }
            <ConfirmButtonContainer>
                <Button color="primary" variant={"contained"} onClick={()=>handleSubmit(formValues)}>
                    OK
                </Button>
            </ConfirmButtonContainer>
        </FormContainer>
    );
};

const ConfirmButtonContainer = styled.div`
    text-align:center
`;

const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    .custom-input{
        margin: 10px 5px; 
    }
`;

export default Form