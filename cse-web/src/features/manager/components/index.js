import React from "react";
import {connect} from "react-redux";
import {getThemes, loadingLogin} from "../selectors";
import {createStructuredSelector} from "reselect";
import {
    addCourseAction,
    addThemeAction,
    deleteCourseAction,
    deleteThemeAction,
    getAvailableThemesAction, updateCourseAction, updateThemeAction
} from "../actions";
import styled from "styled-components";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import Form from "./form";
import {courseInputs, themeInputs} from "../model";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon/ListItemIcon";
import SendIcon from "@material-ui/icons/Send";

const Course = ({course,onDeleteCourse}) =>
    <CourseContainer>
        <ListItemText primary={course.name} secondary={`$ ${course.price}`}/>
        <ListItemIcon onClick={onDeleteCourse}>
            <DeleteIcon/>
        </ListItemIcon>
    </CourseContainer>;

const CourseContainer = styled(ListItem)`
    padding-left: 35px !important;
`;
const Theme = ({theme,onAddCourseClick,onDeleteThemeClick,onDeleteCourse}) =>
    <React.Fragment>
        <ListItem container>
            <ListItemIcon>
                <SendIcon />
            </ListItemIcon>
            <ListItemText
                primary={theme.name}
            />
            <ListItemIcon onClick={onDeleteThemeClick}>
                <DeleteIcon/>
            </ListItemIcon>
        </ListItem>
        <List>
            {theme.courses.map( c => <Course course={c} onDeleteCourse={()=>onDeleteCourse(theme.id,c.id)}/> )}
        </List>
        <AddCourseContainer>
            <Tooltip title="Agregar curso" placement={"top"}>
                <IconButton onClick={()=>onAddCourseClick(theme)}>
                    <AddIcon/>
                </IconButton>
            </Tooltip>
        </AddCourseContainer>
    </React.Fragment>;

const AddCourseContainer = styled.div`
    text-align: center;
`;

class ProfilePage extends React.Component{
    state={themeDialogOpen:null,courseDialogOpen:null};
    render(){
        const {themeDialogOpen,courseDialogOpen} = this.state;
        const {themes} = this.props;
        return <div>
            <List>
                {
                    themes.map( t =>
                        <Theme
                            theme={t}
                            onAddCourseClick={() => this.setState({courseDialogOpen:t})}
                            onDeleteThemeClick={() => this.onDeleteTheme(t.id)}
                            onDeleteCourse={(themeId,id) => this.onDeleteCourse(themeId,id)}
                        />
                    )
                }
            </List>
            <Tooltip title="Agregar temática" placement={"top"}>
                <IconButton onClick={()=> this.setState({themeDialogOpen:true})}>
                    <AddIcon/>
                </IconButton>
            </Tooltip>
            <Dialog open={!!themeDialogOpen} onClose={()=>this.setState({themeDialogOpen:null})} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Temática</DialogTitle>
                <DialogContent>
                    <Form
                        handleSubmit={v => this.onAddTheme(v)}
                        loading={false}
                        inputs={themeInputs}
                    />
                </DialogContent>
            </Dialog>
            <Dialog open={!!courseDialogOpen} onClose={()=>this.setState({courseDialogOpen:null})} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Curso</DialogTitle>
                <DialogContent>
                    <Form
                        handleSubmit={v => this.onAddCourse(v)}
                        loading={false}
                        inputs={courseInputs}
                    />
                </DialogContent>
            </Dialog>
        </div>
    }

    componentDidMount(){
        this.props.dispatch(getAvailableThemesAction)
    }

    onAddTheme = (inputs) =>
        this.props.dispatch(addThemeAction(inputs))
            .then( r => this.setState({themeDialogOpen:null}));

    onAddCourse = (inputs) =>
        this.props.dispatch(addCourseAction({...inputs,courseThemeId: this.state.courseDialogOpen.id}))
            .then( r => this.setState({courseDialogOpen:null}));

    onUpdateCourse = (data) =>
        this.props.dispatch(updateCourseAction(data))
            .then( r => this.setState({courseDialogOpen:null}));

    onUpdateTheme = (data) =>
        this.props.dispatch(updateThemeAction(data))
            .then( r => this.setState({courseDialogOpen:null}));

    onDeleteCourse = (themeId,id) =>
        this.props.dispatch(deleteCourseAction(themeId,id));

    onDeleteTheme = (id) =>
        this.props.dispatch(deleteThemeAction(id));

}

export default connect(createStructuredSelector({
    loading: loadingLogin,
    themes: getThemes
}))(ProfilePage)