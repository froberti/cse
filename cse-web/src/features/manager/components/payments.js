import React from "react";
import {connect} from "react-redux";
import {getPayments, getThemes, loadingLogin} from "../selectors";
import {createStructuredSelector} from "reselect";
import {getPaymentsAction} from "../actions";
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import {inputTypes} from "../../../utils/types";
import Input from "../../../common/input";
import {formatDate} from "../../../utils/date";
import {paymentStatusStrings} from "../../../shared/strings";
import styled from "styled-components";
import SearchIcon from "@material-ui/icons/Search";
import Fab from "@material-ui/core/Fab/Fab";

class PaymentsPage extends React.Component{
    state={themeDialogOpen:null,courseDialogOpen:null,filterValue:"",page: 0};
    render(){
        const {payments} = this.props;
        const {filterValue,page} = this.state;
        return <div>
            <FilterInputContainer>
                <Input
                    inputType={inputTypes.text}
                    label={"Filtro"}
                    value={filterValue}
                    onChange={v => this.setState({filterValue:v})}
                />
                <div>
                    <Fab variant="extended" aria-label="buscar" onClick={()=>{this.getPayments(page,filterValue)}}>
                        Buscar
                        <SearchIcon/>
                    </Fab>
                </div>
            </FilterInputContainer>
            <List>
                {
                    payments.map( p =>
                        <ListItem button>
                            <ListItemText primary={`${p.user.name}, ${p.user.surname} - ${p.user.email}`}
                                          secondary={`${p.amount} - ${paymentStatusStrings[p.paymentStatusId]} - ${formatDate(p.createDatetime)}`}/>
                        </ListItem>
                    )
                }
            </List>
        </div>
    }

    componentDidMount(){
        this.getPayments(0,"")
    }

    getPayments = (page,filter) =>
        this.props.dispatch(getPaymentsAction(page,filter))

}

const FilterInputContainer = styled.div`
    text-align: center;
`;

export default connect(createStructuredSelector({
    loading: loadingLogin,
    payments: getPayments
}))(PaymentsPage)