import {
    ADD_THEME,
    ADD_THEME_FULFILLED,
    ADD_THEME_REJECTED,
    GET_AVAILABLE_THEMES,
    GET_AVAILABLE_THEMES_FULFILLED,
    GET_AVAILABLE_THEMES_REJECTED,
    ADD_COURSE,
    ADD_COURSE_FULFILLED,
    ADD_COURSE_REJECTED,
    ADD_PAYMENT,
    ADD_PAYMENT_FULFILLED,
    ADD_PAYMENT_REJECTED,
    DELETE_THEME,
    DELETE_THEME_FULFILLED,
    DELETE_THEME_REJECTED,
    DELETE_COURSE,
    DELETE_COURSE_FULFILLED,
    DELETE_COURSE_REJECTED,
    UPDATE_THEME,
    UPDATE_THEME_FULFILLED,
    UPDATE_THEME_REJECTED,
    UPDATE_COURSE_REJECTED,
    UPDATE_COURSE_FULFILLED,
    UPDATE_COURSE,
    GET_PAYMENTS, GET_PAYMENTS_FULFILLED, GET_PAYMENTS_REJECTED
} from "./actionTypes";
import {openErrorToast} from "../snackbar/index";
import {
    getAvailableThemes,
    addTheme,
    addCourse,
    addPayment,
    deleteTheme,
    deleteCourse,
    updateTheme, updateCourse, getPayments
} from "../../shared/api";

export const addCourseAction = ({name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order}) => async (dispatch) => {
    dispatch({type: ADD_COURSE});
    try{
        const response = await addCourse({name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order});
        dispatch({type: ADD_COURSE_FULFILLED,payload: {id:response.data,name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order}});
        return response.data
    }catch(e){
        dispatch({type: ADD_COURSE_REJECTED,error:e});
        throw e
    }
};

export const addThemeAction = ({name,description,order}) => async (dispatch) => {
    dispatch({type: ADD_THEME});
    try{
        const response = await addTheme({name,description,order});
        dispatch({type: ADD_THEME_FULFILLED,payload: {id: response.data,name,description,order,courses:[]} });
        return response.data
    }catch(e){
        dispatch({type: ADD_THEME_REJECTED,error:e});
        throw e
    }
};

export const getAvailableThemesAction = async (dispatch) => {
    dispatch({type:GET_AVAILABLE_THEMES});

    try{
        const response = await getAvailableThemes();
        dispatch({type:GET_AVAILABLE_THEMES_FULFILLED,payload: response.data});
        return response;
    }catch(e){
        dispatch({type:GET_AVAILABLE_THEMES_REJECTED,error:e});
        dispatch(openErrorToast(e));
    }
};

export const deleteThemeAction = id => async (dispatch) => {
    dispatch({type:DELETE_THEME});

    try{
        const response = await deleteTheme(id);
        dispatch({type:DELETE_THEME_FULFILLED,payload: id});
        return response;
    }catch(e){
        dispatch({type:DELETE_THEME_REJECTED,error:e});
        dispatch(openErrorToast(e));
    }
};

export const deleteCourseAction = (courseThemeId,id) => async (dispatch) => {
    dispatch({type: DELETE_COURSE});

    try {
        const response = await deleteCourse(id);
        dispatch({type: DELETE_COURSE_FULFILLED, payload: {courseThemeId:courseThemeId,id}});
        return response;
    } catch (e) {
        dispatch({type: DELETE_COURSE_REJECTED, error: e});
        dispatch(openErrorToast(e));
    }
};


export const updateCourseAction = ({name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order}) => async (dispatch) => {
    dispatch({type: UPDATE_COURSE});
    try{
        const response = await updateCourse({name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order});
        dispatch({type: UPDATE_COURSE_FULFILLED,payload: {id:response.data,name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order}});
        return response.data
    }catch(e){
        dispatch({type: UPDATE_COURSE_REJECTED,error:e});
        throw e
    }
};

export const updateThemeAction = ({id,name,description,order}) => async (dispatch) => {
    dispatch({type: UPDATE_THEME});
    try{
        const response = await updateTheme({id,name,description,order});
        dispatch({type: UPDATE_THEME_FULFILLED,payload: response.data });
        return response.data
    }catch(e){
        dispatch({type: UPDATE_THEME_REJECTED,error:e});
        throw e
    }
};

export const getPaymentsAction = (page,filter) => async (dispatch) => {
    dispatch({type: GET_PAYMENTS});
    try{
        const response = await getPayments(page,filter);
        dispatch({type: GET_PAYMENTS_FULFILLED,payload: response.data });
        return response.data
    }catch(e){
        dispatch({type: GET_PAYMENTS_REJECTED,error:e});
        throw e
    }
};