import {
    ADD_COURSE,
    ADD_COURSE_FULFILLED,
    ADD_COURSE_REJECTED,
    ADD_THEME,
    ADD_THEME_FULFILLED,
    ADD_THEME_REJECTED,
    DELETE_COURSE,
    DELETE_COURSE_FULFILLED,
    DELETE_COURSE_REJECTED, DELETE_THEME,
    DELETE_THEME_FULFILLED,
    DELETE_THEME_REJECTED,
    GET_AVAILABLE_THEMES,
    GET_AVAILABLE_THEMES_FULFILLED,
    GET_AVAILABLE_THEMES_REJECTED, GET_PAYMENTS, GET_PAYMENTS_FULFILLED, GET_PAYMENTS_REJECTED,
    UPDATE_COURSE,
    UPDATE_COURSE_FULFILLED,
    UPDATE_COURSE_REJECTED,
    UPDATE_THEME,
    UPDATE_THEME_FULFILLED,
    UPDATE_THEME_REJECTED
} from "./actionTypes";

const initialState = {
    availableThemes: [],
    loadingThemes: false,

    payments: [],

    addingTheme:false,
    addingPayment: false,
    addingCourse: false
};

export default function reducer(state=initialState, action){

    switch (action.type){
        case GET_AVAILABLE_THEMES: return {...state,loadingThemes: true};
        case GET_AVAILABLE_THEMES_FULFILLED: return {...state,loadingThemes:false,availableThemes: action.payload};
        case GET_AVAILABLE_THEMES_REJECTED: return {...state,loadingThemes:false};

        case ADD_COURSE: return {...state,addingCourse:true};
        case ADD_COURSE_FULFILLED:
            return {
                ...state,
                addingCourse:false,
                availableThemes:
                    state.availableThemes
                    .map( t => t.id === action.payload.courseThemeId? {...t,courses: [...t.courses,action.payload]}:t)
            };
        case ADD_COURSE_REJECTED: return {...state,addingCourse:false};

        case ADD_THEME: return {...state,addingTheme:true};
        case ADD_THEME_FULFILLED: return {...state,addingTheme:false,availableThemes: [...state.availableThemes,action.payload]};
        case ADD_THEME_REJECTED: return {...state,addingTheme:false};

        case UPDATE_THEME: return {...state,updatingTheme:true};
        case UPDATE_THEME_FULFILLED: return {...state,updatingTheme:false,availableThemes: state.availableThemes.map( r => r.id === action.payload.id? action.payload:r)};
        case UPDATE_THEME_REJECTED: return {...state,updatingTheme:false};

        case UPDATE_COURSE: return {...state,updatingCourse:true};
        case UPDATE_COURSE_FULFILLED: return {...state,updatingCourse:false,availableThemes: state.availableThemes.map( r => r.id === action.payload.courseThemeId? {...r,courses: r.courses.map(c => c.id === action.payload.id ? action.payload:c)}:r)};
        case UPDATE_COURSE_REJECTED: return {...state,updatingCourse:false};

        case DELETE_COURSE: return {...state,deletingCourse:true};
        case DELETE_COURSE_FULFILLED: return {...state,deletingCourse:false,availableThemes: state.availableThemes.map( r => r.id === action.payload.courseThemeId? {...r,courses: r.courses.filter(c => c.id !== action.payload.id)}:r)};
        case DELETE_COURSE_REJECTED: return {...state,deletingCourse:false};

        case DELETE_THEME: return {...state,deletingTheme:true};
        case DELETE_THEME_FULFILLED: return {...state,deletingTheme:false,availableThemes: state.availableThemes.filter( t => t.id !== action.payload)};
        case DELETE_THEME_REJECTED: return {...state,deletingTheme:false};

        case GET_PAYMENTS: return {...state,loadingPayments: true};
        case GET_PAYMENTS_FULFILLED: return {...state,loadingPayments: false,payments: action.payload};
        case GET_PAYMENTS_REJECTED: return {...state,loadingPayments: false};

        default: return state
    }
}