import reducer from "./reducer";
export {default as ManagerPage} from "./components/tabs";
export {default as managerReducerName} from "./constants";
export default reducer;