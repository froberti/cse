import {inputTypes, validateVarchar} from "../../utils/types";
import {courseStatus} from "../../shared/constants";

export const themeInputs = [
    {
        name: "name",
        label: "Título",
        validator: (v) => v && validateVarchar(v,100),
        inputType: inputTypes.text
    },
    {
        name: "description",
        label: "Descripcion",
        validator: (v) => validateVarchar(v,100),
        inputType: inputTypes.text
    }
];


export const courseInputs = [
    {
        name: "name",
        label: "Título",
        validator: (v) => v && validateVarchar(v,100),
        inputType: inputTypes.text
    },
    {
        name: "description",
        label: "Descripcion",
        validator: (v) => validateVarchar(v,100),
        inputType: inputTypes.text
    },
    {
        name: "price",
        label: "Precio",
        validator: (v) => v && v > 0,
        inputType: inputTypes.text,
        type: "number"
    },
    {
        name: "courseStatusId",
        label: "Estado del curso",
        validator: (v) => !!v,
        inputType: inputTypes.select,
        options: [
            {value: courseStatus.active, label: "Activo"},
            {value: courseStatus.inactive, label: "Inactivo"}
        ]
    },
    {
        name: "showPrice",
        label: "Mostrar precio",
        validator: (v) => v === false || v === true || v === 0 || v === 1,
        inputType: inputTypes.select,
        options: [
            {value: 1, label: "Sí"},
            {value: 0, label: "No"}
        ]
    },
    {
        name: "duration",
        label: "Duración del curso",
        validator: (v) => !v || v > 0,
        inputType: inputTypes.text,
        type: "number"
    },
    {
        name: "target",
        label: "El curso es apto para",
        validator: (v) => validateVarchar(v,100),
        inputType: inputTypes.text
    }
];