import NAME from "./constants";

export const getLoginResponse = (store) => !!store[NAME].loginResponse;
export const getUserPermissions = (store) => !!store[NAME].userPermissions;
export const loadingLogin = (store) => store[NAME].loading;
export const getThemes = store => store[NAME].availableThemes;