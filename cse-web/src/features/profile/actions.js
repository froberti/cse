import {
    ADD_THEME, ADD_THEME_FULFILLED, ADD_THEME_REJECTED,
    GET_AVAILABLE_THEMES, GET_AVAILABLE_THEMES_FULFILLED, GET_AVAILABLE_THEMES_REJECTED,
    ADD_COURSE, ADD_COURSE_FULFILLED, ADD_COURSE_REJECTED,
    ADD_PAYMENT, ADD_PAYMENT_FULFILLED, ADD_PAYMENT_REJECTED
} from "./actionTypes";
import {openErrorToast} from "../snackbar/index";
import {getAvailableThemes,addTheme,addCourse,addPayment} from "../../shared/api";

export const addPaymentAction = ({itemId}) => async (dispatch) =>{
    dispatch({type: ADD_PAYMENT});
    try{
        const response = await addPayment({itemId});
        dispatch({type: ADD_PAYMENT_FULFILLED,payload: response.data });
        return response.data
    }catch(e){
        dispatch({type: ADD_PAYMENT_REJECTED,error:e});
        throw e
    }
};

export const addCourseAction = ({name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order}) => async (dispatch) => {
    dispatch({type: ADD_COURSE});
    try{
        const response = await addCourse({name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order});
        dispatch({type: ADD_COURSE_FULFILLED,payload: response.data });
        return response.data
    }catch(e){
        dispatch({type: ADD_COURSE_REJECTED,error:e});
        throw e
    }
};

export const addThemeAction = ({name,description,order}) => async (dispatch) => {
    dispatch({type: ADD_THEME});
    try{
        const response = await addTheme({name,description,order});
        dispatch({type: ADD_THEME_FULFILLED,payload: response.data });
        return response.data
    }catch(e){
        dispatch({type: ADD_THEME_REJECTED,error:e});
        throw e
    }
};

export const getAvailableThemesAction = async (dispatch) => {
    dispatch({type:GET_AVAILABLE_THEMES});

    try{
        const response = await getAvailableThemes();
        dispatch({type:GET_AVAILABLE_THEMES_FULFILLED,payload: response.data});
        return response;
    }catch(e){
        dispatch({type:GET_AVAILABLE_THEMES_REJECTED,error:e});
        dispatch(openErrorToast(e));
    }
};
