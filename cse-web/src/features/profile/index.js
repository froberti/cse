import reducer from "./reducer";
export {default as ProfilePage} from "./components/index";
export {default as adminReducerName} from "./constants";
export default reducer;