import React from 'react'
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from "@material-ui/core/Paper";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {strings} from "../../../localization/strings";

const LoginForm = ({handleSubmit,loading,onRegisterButtonClick}) => (
    <Paper id='inputs-form' className={"center-all-content"}>
        <Avatar >
            <LockOutlinedIcon color={"secondary"}/>
        </Avatar>
        <Typography component="h1" variant="h5">
            {strings.loginToYourAccount}
        </Typography>
        <form onSubmit={handleSubmit}>
            <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="email">{strings.emailPlaceholder}</InputLabel>
                <Input id="email" name="email" autoComplete="email" autoFocus disabled={loading} />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="password">{strings.passwordPlaceholder}</InputLabel>
                <Input name="password" type="password" id="password" autoComplete="current-password" disabled={loading} />
            </FormControl>
            <div className={"register-button-container"}>
                <Typography variant={"subtitle1"} inline >{strings.haveAccount}</Typography>
                <Button variant={"outlined"}
                        onClick={onRegisterButtonClick} color={"secondary"}>{strings.register}</Button>
            </div>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={loading}
            >
                {strings.login}
            </Button>
        </form>
    </Paper>
);

export default LoginForm