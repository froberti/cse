import React from "react";
import {connect} from "react-redux";
import {getThemes, loadingLogin} from "../selectors";
import {createStructuredSelector} from "reselect";
import {getAvailableThemesAction} from "../actions";
import styled from "styled-components";
import MpButton from "./mpButton";
import Typography from "@material-ui/core/Typography/Typography";
import moment from "moment";
import {functionalities, paymentStatus} from "../../../shared/constants";
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import Collapse from "@material-ui/core/Collapse/Collapse";
import SendIcon from '@material-ui/icons/Send';
import {getUserFunctionalities} from "../../login/selectors";
import {formatDate} from "../../../utils/date";
import {paymentStatusStrings} from "../../../shared/strings";
import Button from "@material-ui/core/Button/Button";

const colors = {
    [paymentStatus.done]: "#13ab10",
    [paymentStatus.pending]: "#e8c308",
    [paymentStatus.error]: "#e80808",
    [paymentStatus.cancelled]: "#e80808",
};

const Payment = ({payment}) =>
    <PaymentContainer>
        <ListItemText primary={`Pago del : ${formatDate(payment.createDatetime)} - ${paymentStatusStrings[payment.paymentStatusId]}`}
                      secondary={`$ ${payment.amount}`}/>
    </PaymentContainer>;

const PaymentContainer = styled(ListItem)`
    padding-left: 45px !important;
`;

const Course = ({course}) =>
    <React.Fragment>
        <CourseContainer button>
            <ListItemText primary={course.name} />
            <ListItemIcon>
                <MpButton itemId={course.itemId}/>
            </ListItemIcon>
        </CourseContainer>
        <List component="div" disablePadding>
            {course.payments.map(p => <Payment payment={p}/>)}
        </List>
    </React.Fragment>;

const CourseContainer = styled(ListItem)`
    padding-left: 35px !important;
`;

const Theme = ({theme}) =>
    <React.Fragment>
        <ThemeContainer>
            <ListItemIcon>
                <SendIcon />
            </ListItemIcon>
            <ListItemText
                primary={theme.name}
            />
        </ThemeContainer>
        <Collapse in={true} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                {theme.courses.map( c => <Course course={c}/> )}
            </List>
        </Collapse>
    </React.Fragment>;


const ThemeContainer = styled(ListItem)`
`;

class ProfilePage extends React.Component{

    render(){
        const {themes,dispatch,loading,history,userFunctionalities} = this.props;
        return <div>
            <div style={{marginTop: 100}}/>
            {
                userFunctionalities && userFunctionalities.includes(functionalities.manageCourses) &&
                <Button variant={"contained"} onClick={()=>history.push("/user/manager")}>
                    Administrar cursos
                </Button>
            }
            <Typography variant={"h4"} align={"center"}>Cursos disponibles</Typography>
            <List>
                {
                    themes.filter(t => t.courses && t.courses.length > 0).map( t =>
                        <Theme
                            theme={t}
                        />
                    )
                }
            </List>
        </div>
    }

    componentDidMount(){
        this.props.dispatch(getAvailableThemesAction)
    }
}

export default connect(createStructuredSelector({
    loading: loadingLogin,
    themes: getThemes,
    userFunctionalities: getUserFunctionalities
}))(ProfilePage)