import React,{useState} from "react";
import {addPayment} from "../../../shared/api";
import styled from "styled-components";
import MpLogo from "../../../assets/images/mercado-pago-logo.png";
import CircularLoader from "@material-ui/core/CircularProgress";

const MpButton = ({itemId}) => {
    const [loading,setLoading] = useState(false);

    const handleMpButtonClick = () =>{
        setLoading(true);

        addPayment(itemId)
            .then( r => {
                    window.open(r.data, '_blank');
                }
            )
            .finally( () => {
                setLoading(false)
            })

    };

    return loading? <div><CircularLoaderStyled/></div>:
        <div onClick={handleMpButtonClick}>
            <MpIcon src={MpLogo}/>
        </div>
};

const MpIcon = styled.img`
    max-height: 25px;
    cursor: pointer;
`;
const CircularLoaderStyled = styled(CircularLoader)`
    height:30px!important;
    width:30px!important;
`;

export default MpButton
