import {GET_AVAILABLE_THEMES, GET_AVAILABLE_THEMES_FULFILLED, GET_AVAILABLE_THEMES_REJECTED} from "./actionTypes";


// ADD_THEME, ADD_THEME_FULFILLED, ADD_THEME_REJECTED,
//     GET_AVAILABLE_THEMES, GET_AVAILABLE_THEMES_FULFILLED, GET_AVAILABLE_THEMES_REJECTED,
//     ADD_COURSE, ADD_COURSE_FULFILLED, ADD_COURSE_REJECTED,
//     ADD_PAYMENT, ADD_PAYMENT_FULFILLED, ADD_PAYMENT_REJECTED


const initialState = {
    availableThemes: [],
    loadingThemes: false,

    addingTheme:false,
    addingPayment: false,
    addingCourse: false
};

export default function reducer(state=initialState, action){

    switch (action.type){
        case GET_AVAILABLE_THEMES: return {...state,loading: true};
        case GET_AVAILABLE_THEMES_FULFILLED: return {...state,loading:false,availableThemes: action.payload};
        case GET_AVAILABLE_THEMES_REJECTED: return {...state,loading:false};

       default: return state
    }
}