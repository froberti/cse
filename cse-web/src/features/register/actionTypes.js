import NAME from "./constants";

export const REGISTER = NAME + "/REGISTER";
export const REGISTER_FULFILLED = NAME + "/REGISTER_FULFILLED";
export const REGISTER_REJECTED = NAME + "/REGISTER_REJECTED";
