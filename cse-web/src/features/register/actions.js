import {
    REGISTER, REGISTER_FULFILLED, REGISTER_REJECTED
} from "./actionTypes";
import {openErrorToast} from "../snackbar/index";
import {register} from "../../shared/api";

export const registerAction = (inputs) => async (dispatch) => {
    dispatch({type:REGISTER});

    try{
        const response = await register(inputs);
        dispatch({type:REGISTER_FULFILLED,payload: response.data});
        return response;
    }catch(e){
        dispatch({type:REGISTER_REJECTED,error:e});
        dispatch(openErrorToast(e));
        throw e
    }
};
