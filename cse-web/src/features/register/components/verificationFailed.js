import React from "react";
import Button from "@material-ui/core/Button/Button";
import {routes} from "../../../router/routes";
import Typography from "@material-ui/core/Typography/Typography";

const FailedVerificationPage  = ({history}) =>
    <div id={"register-page"} className={"first center-all-content"}>
        <Typography variant={"h4"}>El email no pudo ser verificado, por favor registrate nuevamente</Typography>
        <Button style={{marginTop:15}} variant={"contained"} color={"primary"} onClick={()=>history.push(routes.login.path)}>
            Registrarme
        </Button>
    </div>;

export default FailedVerificationPage