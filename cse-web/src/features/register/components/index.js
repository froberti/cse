import React,{useState} from "react";
import {connect} from "react-redux";
import RegisterForm from "./form";
import {registerAction} from "../actions";
import {createStructuredSelector} from "reselect";
import {fields} from "../model";
import {loadingRegister} from "../selectors";
import Typography from "@material-ui/core/Typography/Typography";

const RegisterPage  = ({dispatch,loading}) => {
    const [emailSent,setEmailSent] = useState(false);
    function handleRegister(event){
        event.preventDefault();

        const inputs = Object.values(fields);

        let requestObject = {};

        for(let i =0 ; i < inputs.length ;i++){
            requestObject[inputs[i].name] = event.target[inputs[i].name].value;
            if(! inputs[i].validator( event.target[inputs[i].name].value ))
                return;
        }
        dispatch(registerAction(requestObject))
            .then( r => setEmailSent(true));
    }

    return  <div id={"register-page"} className={"first center-all-content"}>
                {
                    emailSent?
                        <Typography variant={"h5"}>Se ha enviado un email para verificar tu cuenta</Typography>
                        :<RegisterForm loading={loading}
                                      handleSubmit={handleRegister}
                                      inputs={Object.values(fields)}/>
                }
            </div>;
};

export default connect(createStructuredSelector({
    loading: loadingRegister
}))(RegisterPage)