import React from 'react'
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {strings} from "../../../localization/strings";
import Input from "../../../common/input/index";
import styled from "styled-components";

const RegisterForm = ({handleSubmit,loading,inputs}) => (
    <CustomInputContainer id='inputs-form' className={"center-all-content"}>
        <Avatar >
            <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
            {strings.registerToYourAccount}
        </Typography>
        <form onSubmit={handleSubmit}>

            {
                inputs.map( input =>
                    <Input
                        {...input}
                    />
                )
            }
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={loading}
            >
                {strings.register}
            </Button>
        </form>
    </CustomInputContainer>
);

const CustomInputContainer = styled(Paper)`
    .custom-input{
        margin: 15px;
    }
`;

export default RegisterForm