import React from "react";
import Button from "@material-ui/core/Button/Button";
import {routes} from "../../../router/routes";
import Typography from "@material-ui/core/Typography/Typography";

const VerificationPage  = ({history}) =>
    <div id={"register-page"} className={"first center-all-content"}>
        <Typography variant={"h4"}>Email verificado correctamente</Typography>
        <Button style={{marginTop:15}} variant={"contained"} color={"primary"} onClick={()=>history.push(routes.login.path)}>
            Ingresar
        </Button>
    </div>;

export default VerificationPage