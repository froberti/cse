import {REGISTER, REGISTER_FULFILLED, REGISTER_REJECTED } from "./actionTypes";

const initialState = {
    registerResponse: null,
    loading: false
};

export default function reducer(state=initialState, action){

    switch (action.type){
        case REGISTER: return {...state,loading: true};
        case REGISTER_FULFILLED: return {...state,loading:false,registerResponse: action.payload};
        case REGISTER_REJECTED: return {...state,loading:false};
       default: return state
    }
}