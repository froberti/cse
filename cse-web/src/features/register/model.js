import {inputTypes, types, valueIsOfType} from "../../utils/types";
import {errors} from "../../localization/errors";
import {strings} from "../../localization/strings";

export const MIN_EMAIL_LENGTH = 4;
export const MAX_EMAIL_LENGTH = 25;
export const MIN_PASSWORD_LENGTH = 3;
export const MAX_PASSWORD_LENGTH = 10;

export const mailValidation = (mail) => {
    if(!mail)
        return {msg: errors.enterUser};

    if(mail.length < MIN_EMAIL_LENGTH || mail.length > MAX_EMAIL_LENGTH)
        return {msg: errors.invalidUserLength};

    if(!valueIsOfType(mail,types.email))
        return {msg: errors.invalidUser};

    return true
};

export const passwordValidation = (password) => {
    if(!password)
        return {msg: errors.enterPassword};
    
    if(password.length < MIN_PASSWORD_LENGTH || password.length > MAX_PASSWORD_LENGTH)
        return {msg: errors.invalidPasswordLength};

    if(!valueIsOfType(password,types.password))
        return {msg: errors.invalidPassword};

    return true
};

export const fields = {
    email: {name: "email", label: strings.email,validator: mailValidation,type: "email",inputType:inputTypes.text},
    password: {name: "password", label: strings.passwordPlaceholder,validator: passwordValidation, type: "password",inputType:inputTypes.text},
    name: {name: "name", label: strings.name,validator: (value)=>!!value,inputType:inputTypes.text},
    surname: {name: "surname", label: strings.surname,validator: (value)=>!!value,inputType:inputTypes.text},
    dni: {name: "dni", label: strings.dni,validator: (value)=>!!value,inputType:inputTypes.text,type:"number"},
    phone: {name: "phone", label: strings.phone,validator: (value)=>valueIsOfType(value,types.number),type:"number",inputType:inputTypes.text }
};