import NAME from "./constants";

export const getRegisterResponse = (store) => !!store[NAME].registerResponse;
export const loadingRegister = (store) => store[NAME].loading;