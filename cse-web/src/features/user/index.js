import React from "react";
import {BrowserRouter, Route,  Switch} from "react-router-dom";
import {getRoutes} from "./routes";
import connect from "react-redux/es/connect/connect";
import {createStructuredSelector} from "reselect";
import {getLoginResponse, loadingLogin} from "../login/selectors";
import {LoginPage} from "../login"
import {userStatus} from "../../shared/constants";
import CompleteInfo from "../login/components/completeInfo";
import Typography from "@material-ui/core/Typography/Typography";

const UserRouter = ({loginResponse}) =>
    loginResponse ?
        (
            loginResponse.user.userStatusId === userStatus.valid?
            <Switch>
                {getRoutes().map((route)=>
                    <Route key={route.path}
                           exact={!!route.isExact}
                           path={route.path + (route.params || "")}
                           component={route.component}/>
                )}
            </Switch>:
                loginResponse.user.userStatusId === userStatus.pendingInfo?
                    <CompleteInfo/>:
                    loginResponse.user.userStatusId === userStatus.pendingEmail?
                        <Typography variant={"h5"} style={{marginTop: 150,textAlign:"center"}}>Mirá tu casilla de email y confirmalo!</Typography>
                        :null
        ):
        <LoginPage />;

export default connect(createStructuredSelector({
    loginResponse: getLoginResponse
}))(UserRouter)