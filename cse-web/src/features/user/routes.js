import React from "react";
import UserIcon from "@material-ui/icons/AccountCircle";
import {ProfilePage} from "../profile";
import {ManagerPage} from "../manager";
import {LoginPage} from "../login";
import {strings} from "../../localization/strings";

export const routes = {
    manager: {component: ManagerPage,path: "/user/manager",navTitle: strings.managerNavTitle},
    profile: {component: ProfilePage,path: "/user/",navTitle: strings.profileNavTitle}
};

export const getRoutes = () => Object.values(routes);
