import NAME from "./constants";

export const OPEN_TOAST = NAME + "/OPEN_TOAST";
export const CLOSE_TOAST = NAME + "/CLOSE_TOAST";
