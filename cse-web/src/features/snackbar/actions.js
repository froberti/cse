import {OPEN_TOAST, CLOSE_TOAST} from "./actionTypes";
import {variantStyles} from "./constants";
import {shardErrors, getErrorStringFromError} from "../../shared/errors";
import {logOutAction} from "../login/actions";

export const openSuccessToast = (msg) =>
    (dispatch) => dispatch({type: OPEN_TOAST,payload:{msg: msg,variant: variantStyles.success}});

export const openErrorToast = (e,actionClickHandler,cancelCallback) =>
    (dispatch) => {
        const errorMessage =  getErrorStringFromError(e);
        if( errorMessage === shardErrors.EXPIRED_SESSION)
            dispatch(logOutAction);
        dispatch({type: OPEN_TOAST,payload:{msg:errorMessage ,actionClickHandler,cancelCallback,variant: variantStyles.failure} })
    };

export const closeToast = (dispatch) => dispatch({type: CLOSE_TOAST});