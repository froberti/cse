import React from "react"

const images = [
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0001.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0002.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0003.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0004.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0005.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0006.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0007.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0008.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0009.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0010.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0011.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0012.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0013.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0014.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0015.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0016.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0017.jpg",
    "https://storage.googleapis.com/www.cseducacion.com.ar/sisyl/Sisyl%20CSE%20(1)_page-0018.jpg",
];

const SisylDossier = () =>
    <div id={"clean-it-dossier"}>
        {
           images.map( (i,key) =>
               <img src={i} alt={key}/>
           )
        }
    </div>;

export default SisylDossier