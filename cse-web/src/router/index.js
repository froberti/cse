import React from "react";
import {BrowserRouter, Route,  Switch} from "react-router-dom";
import {getNavRoutes, getRoutes, routes} from "./routes";
import NavBar from "../common/menu";
import Footer from "../common/footer/index";

const Router = ({handleLogout,loggedIn}) =>
    <BrowserRouter>
        <div id="app-container" >
            <NavBar routes={getNavRoutes()}
                    loggedIn={loggedIn}
                    mainPath={routes.main.path}
                    handleLogoutClick={handleLogout}
            />
            <Switch>
                {getRoutes().map((route)=>
                    <Route key={route.path}
                           exact={!!route.isExact}
                           path={route.path + (route.params || "")}
                           component={route.component}/>
                    )}
            </Switch>
        </div>
    </BrowserRouter>;

export default Router