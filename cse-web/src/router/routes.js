import React from "react";
import {MainPage} from "../features/main";
import {strings} from "../localization/strings";
import {RegisterPage} from "../features/register/index";
import {ThemesPage} from "../features/themes/index";
import CleanItPage from "../features/cleanIt/index";
import PlacaPage from "../features/placa";
import SisylDossier from "../features/sisyl";
import UserIcon from "@material-ui/icons/AccountCircle";
import UserRouter from "../features/user";
import VerificationPage from "../features/register/components/verified";
import FailedVerificationPage from "../features/register/components/verificationFailed";
export const routes = {
    verificationFailed: {component:FailedVerificationPage,path:"/verificationFailed"},
    verification: {component: VerificationPage,path: "/verification"},
    sisyl: {component: SisylDossier, path: "/sisyl",navTitle: strings.placaNavTitle},
    cleanIt: {component: CleanItPage, path: "/cleanit",navTitle: strings.placaNavTitle},
    placa: {component: PlacaPage, path: "/placa",navTitle: strings.placaNavTitle},
    //products: {component: ProductsPage, path: "/productos", navTitle: strings.productsNavTitle},
    themes: {component: ThemesPage, path: "/temas", navTitle: strings.themesNavTitle},
    //support: {component: CommonPage, path: "/sporte", navTitle: strings.supportNavTitle},
    register: {component: RegisterPage, path:"/register",navTitle: strings.registerNavTitle},
    login: {component: UserRouter, path:"/user",navTitle: strings.loginNavTitle,icon: <UserIcon/>},
    main: {component: MainPage, path:"/",navTitle: strings.mainNavTitle},
};

export const getNavRoutes = () => [
    routes.themes,
];

export const getRoutes = () => Object.values(routes);
