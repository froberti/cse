import { combineReducers } from 'redux';
import appReducer,{appReducerName} from "../features/app/index";
import toastReducer,{toastReducerName} from "../features/snackbar/index";
import mainReducer,{mainReducerName} from "../features/main/index";
import loginReducer,{loginReducerName} from "../features/login/index";
import registerReducer,{registerReducerName} from "../features/register/index";
import adminReducer,{adminReducerName} from "../features/profile/index";
import managerReducer,{managerReducerName} from "../features/manager/index";

const rootReducer = combineReducers({
    [mainReducerName]: mainReducer,
    [loginReducerName]: loginReducer,
    [appReducerName]: appReducer,
    [toastReducerName]: toastReducer,
    [registerReducerName]: registerReducer,
    [adminReducerName]: adminReducer,
    [managerReducerName]: managerReducer,
});

export default rootReducer;