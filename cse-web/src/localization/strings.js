const localizedStrings = {
    es:{
        //App
        appTitle: "CSE",

        //Products
        selectProduct: "Seleccioná un producto",

        //Courses
        moreSeenCourses: "Cursos más vistos",

        //Main
        mainTitle: "Club Service Educación",
        mainSubtitle: "Todos los servicios educativos, llave en mano para tu escuela.",
        mainButton: "Mirá como funciona",
        seePrograms: "Ver programas",
        toKnowMoreAboutPrograms: "Para conocer todos los programas del club services hace clic acá",

        //Select Theme
        selectTheme: "Seleccioná una temática",

        //Support
        seeMore: "Ver más",

        //Login
        loginToYourAccount: "Ingresá a tu cuenta",
        registerToYourAccount: "Registrate",
        emailPlaceholder: "Email",
        youCanAlsoLoginWith: "O ingresá con",
        passwordPlaceholder: "Contraseña",
        login: "Login",
        newToUs: "No tenés cuenta ?",
        register: "Registrarme",
        haveAccount: "¿No tenés cuenta?",

        //Register
        password: "Password",
        email: "Email",
        name: "Nombre",
        surname: "Apellido",
        schoolName: "Nombre de tu escuela",
        phone: "Teléfono",
        dni: "DNI",
        confirm: "Confirmar",
        confirmYourDataToAccessToYourAccount: "Completá tu información personal",

        //Nav
        loginNavTitle: "Login",
        supportNavTitle: "Soporte",
        mainNavTitle: "Página principal",
        registerNavTitle: "Registrarse",
        themesNavTitle: "Espacio Maker",
        productsNavTitle: "Productos",
        placaNavTitle: "Servicios",
        profileNavTitle: "Perfil",
        managerNavTitle: "Manager",

        //Footer
        footer: "© CSE - Tecnología",

    },
    zh:{
    }
};

export const strings = navigator.language && navigator.language.includes("es") ? localizedStrings.es:localizedStrings.es;
