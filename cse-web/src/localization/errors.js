import {MAX_PASSWORD_LENGTH, MAX_USER_LENGTH, MIN_PASSWORD_LENGTH, MIN_USER_LENGTH} from "../features/login/model";

export const errors = {
    invalidUserLength: `El usuario debe tener entre ${MIN_USER_LENGTH} y ${MAX_USER_LENGTH} caracteres`,
    invalidUser: `El usuario ingresado tiene caracteres que no están permitidos`,
    invalidPasswordLength: `La contraseña debe tener entre ${MIN_PASSWORD_LENGTH} y ${MAX_PASSWORD_LENGTH} caracteres`,
    invalidPassword: `La contraseña tiene caracteres que no están permitidos`,
    enterUser: "Ingesá tu usuario",
    enterPassword: "Ingresá tu contraseña",
    enterDni: "Ingresá tu dni",
    invalidDni: "Dni inválido",
    invalidDniLength: "Dni inválido",
    enterPhone: "Ingresá tu teléfono",
    invalidPhone: "Teléfono inválido",
    invalidPhoneLength: "Teléfono inválido",
    defaultErrorMessage: "Intentá nuevamente más tarde"
};