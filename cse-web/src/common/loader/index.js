import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

const CustomLoader = ({loading}) =>
    loading ?
    <div id={"full-screen-loader"}>
        <div id={"loader-center-container"}>
            <CircularProgress indeterminate>Preparing Files</CircularProgress>
        </div>
    </div>:"";

export default CustomLoader;