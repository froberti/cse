import React from "react";
import MainLogo from "../../assets/images/main-logo.png";
import {strings} from "../../localization/strings";

const LogoLoader = () =>
    <div className="logo-loader">
        <img className="logo-loader-image" src={MainLogo} alt={strings.appTitle}/>
    </div>;

export default LogoLoader