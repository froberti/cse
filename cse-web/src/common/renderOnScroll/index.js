import React,{useState} from "react";
import { Waypoint } from 'react-waypoint';

const RenderOnScroll = ({children}) =>{
    const [active, setActive] = useState(false);

    return <Waypoint
        onEnter={()=>setActive(true)}
        onLeave={()=>setActive(false)}
    >
            {active && children}
    </Waypoint>
};

export default RenderOnScroll