import React from "react"
import CloudImage from "../../assets/images/cloud.svg";

const CloudLoader = () =>
    <img src={CloudImage} alt={"Loader"}/>;

export default CloudLoader