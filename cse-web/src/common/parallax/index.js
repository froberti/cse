import React from "react";
import Parallax from "./parallax";

const ParallaxContainer = (props) => {

    return <div style={{minHeight: (props.height||500)}}>
            <Parallax {...props}/>
        </div>
};
export default ParallaxContainer