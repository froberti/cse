import React from "react"
import Typography from "@material-ui/core/Typography";

const Parallax = ({height,image,text,active}) =>{
    return <div className={`parallax-container`}>
        {/*<Mask className={"top-mask"}/>*/}
        <div className={"text-container"}>
            <Typography variant={"h4"} id="parallax-text" className={`text ${active ? "active":"inactive"}`}>
                {text}
            </Typography>
        </div>
    </div>
};


export default Parallax