import React,{useState} from "react";
import Button from "@material-ui/core/Button";
import styled from "styled-components";
import Menu from "@material-ui/core/Menu/Menu";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import {routes as routesObject} from "../../router/routes";

const DesktopMenu = ({routes,push,loggedIn,handleLogoutClick}) => {
    const [userMenuOpen,setUserMenuOpen] = useState(null);

    return <NavContainer>
        {
            routes.map(route =>
                <Button key={route.path}
                    style={route.color?{background:route.color,color: "white"}:{}}
                    variant={route.variant || "contained"}
                    // color={route.color || "primary"}
                    className={"nav-button color-fade-background"}
                    onClick={()=> push(route.path)}>{route.navTitle}</Button>
            )
        }
        <IconContainer onClick={(e)=>loggedIn? setUserMenuOpen(e.target):push(routesObject.login.path)}>
            {routesObject.login.icon}
        </IconContainer>
        <Menu
            anchorEl={userMenuOpen}
            open={Boolean(userMenuOpen)}
            onClose={()=>setUserMenuOpen(null)}
        >
            <MenuItem key={routesObject.login.path} color="inherit" onClick={()=>
                {
                    setUserMenuOpen(null);
                    window.scrollTo(0, 0);
                    push(routesObject.login.path);
                }}>
                Perfil
            </MenuItem>

            <MenuItem color="inherit" onClick={()=>
                {
                    setUserMenuOpen(null);
                    window.scrollTo(0, 0);
                    handleLogoutClick();
                    push(routesObject.main.path);
                }}>
                Salir
            </MenuItem>
        </Menu>
    </NavContainer>
};

const NavContainer = styled.nav`
    display: flex;
`;

const IconContainer = styled.div`
    display: inline-block;
    margin: 0 5px;
    cursor: pointer;
    svg{
        height:35px!important;
        width:35px!important;
        color: white;
    }
`;

export default DesktopMenu