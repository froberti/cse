import React,{useState} from "react"
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MenuIcon from "@material-ui/icons/Menu";

const MobileMenu = ({routes,push}) => {
    const [anchorEl, setAnchorEl] = useState(null);

    return <nav >
        <IconButton onClick={e=>setAnchorEl(e.target)}>
            <MenuIcon/>
        </IconButton>

        <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={()=>setAnchorEl(null)}
        >
            {
                routes.map(route =>
                    <MenuItem key={route.path} color="inherit" onClick={()=>
                    {
                        setAnchorEl(null);
                        window.scrollTo(0, 0);
                        push(route.path);
                    }}>
                        {route.navTitle}
                    </MenuItem>
                )
            }
        </Menu>
    </nav>
};

export default MobileMenu