import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MainLogo from "../../assets/images/main-logo.png";
import {withRouter} from "react-router-dom";
import Hidden from "@material-ui/core/Hidden";
import MobileMenu from "./mobile";
import DesktopMenu from "./desktop";
import {strings} from "../../localization/strings";
import Typography from "@material-ui/core/Typography";

const NavBar = ({routes,history,mainPath,handleLogoutClick,loggedIn}) =>{
    const push = (path) => { window.scrollTo(0,0); history.push(path) };
    return <div id={"nav-bar"}>
        <AppBar position="fixed" color="default">
            <Toolbar className={"toolbar"}>
                <img src={MainLogo} alt={"Logo"} className={"logo shadow-on-hover"} onClick={()=> history.push(mainPath)}/>
                <Hidden smDown>
                    <div className={"spacer"}/>
                    <DesktopMenu routes={routes} loggedIn={loggedIn} push={push} handleLogoutClick={handleLogoutClick}/>
                </Hidden>
                <Hidden mdUp>
                    <Typography variant={"h6"} className={"title"}>{strings.appTitle}</Typography>
                    <MobileMenu routes={routes} loggedIn push={push} handleLogoutClick={handleLogoutClick}/>
                </Hidden>
            </Toolbar>
        </AppBar>
    </div>;
};


export default withRouter(NavBar)