import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import styled from "styled-components";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";

const Loader = () => <CircularProgress size={20}/>;

const CustomAutosuggest = ({label,value,options,onChange,loading,readOnly,multiple,chips,disabled,getOptions}) =>
    <AutosuggestContainer>
        <Input
            inputType={inputTypes.text}
        />
    </AutosuggestContainer>;

const AutosuggestContainer = styled.div`
`;


export default CustomAutosuggest