import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Chip from "@material-ui/core/Chip";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import {strings} from "../../localization/strings";

const Loader = () => <CircularProgress size={20}/>;

const CustomSelect = ({showEmpty,label,value,options,onChange,loading,readOnly,multiple,chips,disabled,getOptions}) =>
    <FormControl className={"custom-input"}>
        <InputLabel htmlFor={(label||"")+"-select"} >
            {label}
        </InputLabel>

        <Select
            multiple={!!multiple}
            value={value === undefined || value ==="" ? (multiple?[]:""):value}
            onChange={(e)=>{
                onChange && onChange(e.target.value)
            }}
            inputProps={{
                name: `${label}-select`,
                id: `${label}-select`,
                readOnly: !!readOnly,
            }}
            renderValue={chips && (selected => (
                <div className={"chip-container"}>
                    {Array.isArray(selected) ?
                        selected.map(value => (
                            <Chip key={value} label={value}/>
                        )):
                        <Chip label={selected}/>
                    }
                </div>
            ))}
            IconComponent={loading? Loader:undefined}
            disabled={disabled || loading}
        >
            {
                showEmpty &&
                <MenuItem value={""}>{strings.empty}</MenuItem>
            }
            {
                (options? (getOptions?getOptions(options):options) : []).map(option =>
                    <MenuItem value={option.value} key={option.value}>{option.label}</MenuItem>
                )
            }
        </Select>
    </FormControl>;



export default CustomSelect