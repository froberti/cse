import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import CircleProgress from "@material-ui/core/CircularProgress";

const CustomSelect = ({label,value,options,onChange,loading}) =>
    <FormControl className={"input"}>
        <InputLabel htmlFor={(label||"")+"-select"}>
            {label}
            {loading && <div className={"input-loader-container"}>
                <CircleProgress/>
            </div>}
        </InputLabel>

        {!loading &&
            <Select
                value={value}
                onChange={(e)=>onChange(e.target.value)}
                inputProps={{
                    name: label+"-select",
                    id: label+"-select",
                }}
            >
                {
                    (options || []).map(option =>
                        <MenuItem value={option.value} key={option.value}>{option.label}</MenuItem>
                    )
                }
            </Select>
        }
    </FormControl>;

export default CustomSelect