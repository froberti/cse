import React from "react"
import Input from "./index";
import {inputTypes} from "../../utils/types";
import {strings} from "../../localization/strings";
import Typography from "@material-ui/core/Typography";

const NumberUnit = ({value,type,label,onChange,options}) =>
    <div>
        <Typography variant={"subtitle1"}>{label}</Typography>
        <Input
            inputType={inputTypes.select}
            label={strings.unit}
            value={value && value.unit}
            options={options}
            onChange={unit=>onChange({...value,unit})}
        />
        <Input
            inputType={inputTypes.text}
            label={strings.value}
            onChange={v=>onChange({...(value||{}),value: v})}
            value={(value && value.value) || ""}
            type={type}
        />
    </div>;

export default NumberUnit