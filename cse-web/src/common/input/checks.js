import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";

const Checks = ({value,options,onChange,label}) =>
    <div className={"checks-container"}>
        <Typography variant={"subtitle1"}>{label}</Typography>
        {
         options.map(option =>
             <FormControlLabel
                 key={option.value}
                 control={
                     <Checkbox
                         checked={value.includes(option.value)}
                         onChange={e => {
                             if(value.includes(option.value))
                                 onChange(value.filter(item => item !== option.value));
                             else
                                 onChange([...value,option.value])
                         }}
                     />
                 }
                 label={option.label}
             />
         )
        }
    </div>;

export default Checks;