import React from "react";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

const CustomFiles = ({value,type,name,label,onChange}) =>
    <React.Fragment>
        <div className={"input-file-container"}>
            <label htmlFor={`file-upload-${name}`} className="input-file-upload-label shadow-on-hover">
                <AddIcon/>
            </label>
            <input className="input-file"
                   accept="image/*"
                   onChange={
                       (e)=>{
                           if(e.target.files && e.target.files[0])
                               onChange([...(value||[]),e.target.files[0]]);
                           e.target.value = null;
                       }
                   }
                   id={`file-upload-${name}`}
                   type="file"/>

        </div>
        {
            value && value.map( (asset,index) =>
                <div className={"file-preview deletable"} key={index}>
                    <img src={asset && URL.createObjectURL(asset)} alt="Alt"/>
                    <div className={"delete-overlay"}>
                        <IconButton onClick={()=>onChange([...value].filter( (img,i) => index!==i))}>
                            <DeleteIcon/>
                        </IconButton>
                    </div>
                </div>
            )
        }
    </React.Fragment>;

export default CustomFiles
