import React from "react";
import TextField from "@material-ui/core/TextField";
import CheckIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';

const CustomTextField = ({value,type,label,onChange,valid, showState}) =>
    <TextField
        className={"custom-input"+(!valid && value && showState?" error-input":"")}
        error={!valid && !!value && showState}
        label={label}
        type={type}
        value={value || ""}
        onChange={(e)=>onChange(e.target.value)}
        InputProps={
            value && showState ? {
                endAdornment:
                    valid?
                        <CheckIcon className={"check-icon"} />:
                        <WarningIcon className={"warning-icon"} />
            }:{}
        }
    />;

export default CustomTextField
