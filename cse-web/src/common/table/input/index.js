import React from "react"
import CustomTextField from "./text";
import CustomSelect from "./select";
import Autosuggest from "./autosuggest";
import {inputTypes} from "../../../utils/types";

const Input = ({type,value,label,options,onChange,loading,valid,inputType,showCheck}) => {
    switch(inputType){
        case inputTypes.text: return <CustomTextField showState={showCheck} valid={valid} label={label} type={type} value={value} onChange={onChange}/>;
        case inputTypes.select: return <CustomSelect valid={valid} label={label} loading={loading} type={type} value={value} options={options} onChange={onChange}/>;
        case inputTypes.autoSuggest: return <Autosuggest valid={valid} label={label} loading={loading} type={type} value={value} options={options} onChange={onChange}/>;
        default: return <CustomTextField showState={showCheck} valid={valid} type={type} value={value} label={label} options={options} onChange={onChange}/>;
    }
};

export default Input