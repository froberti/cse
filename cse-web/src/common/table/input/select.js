import React from "react";
import {FormControl,InputLabel,Select,MenuItem} from "@material-ui/core";
import CircleProgress from "@material-ui/core/CircularProgress";

const CustomSelect = ({label,value,options,onChange,loading}) =>
    <FormControl className={"input"}>
        <InputLabel htmlFor={(label||"")+"-select"}>
            {label}
            {loading && <div className={"input-loader-container"}>
                <CircleProgress/>
            </div>}
        </InputLabel>

        {!loading &&
            <Select
                value={value}
                onChange={(e)=>onChange(e.target.value)}
                inputProps={{
                    name: label+"-select",
                    id: label+"-select",
                }}
            >
                {
                    (options || []).map(option =>
                        <MenuItem value={option.value} key={option.value}>{option.label}</MenuItem>
                    )
                }
            </Select>
        }
    </FormControl>;

export default CustomSelect