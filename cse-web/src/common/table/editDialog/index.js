import React from "react"
import Dialog from "@material-ui/core/Dialog"
import EditDialogContent from "./content";
import DialogTitle from "@material-ui/core/DialogTitle";
import {strings} from "../../../localization/strings";

const EditDialog = ({open,onClose,row,editableFields,onConfirm,asyncProps,title}) =>
    <Dialog
        open={open}
        onClose={onClose}
    >
        <DialogTitle id="alert-dialog-title">{title || strings.editModalTitle}</DialogTitle>
        {open && <EditDialogContent row={row}
                                    asyncProps={asyncProps}
                                    onConfirm={onConfirm}
                                    onClose={onClose}
                                    editableFields={editableFields}/>}
    </Dialog>;

export default EditDialog