import React from "react"
import DialogContent from "@material-ui/core/DialogContent"
import DialogActions from "@material-ui/core/DialogActions"
import {inputTypes} from "../../../utils/types";
import Input from "../input";
import {Button} from "@material-ui/core";
import {strings} from "../../../localization/strings";

class EditDialogContent extends React.Component{
    state={ values : this.props.row };
    render(){
        const {values} = this.state;
        const {editableFields,onClose,asyncProps} = this.props;
        return <React.Fragment>
            <DialogContent>
                {
                    editableFields.map( (field,index) =>
                        <Input
                            key={index}
                            onChange={(value)=>this.handleChange(field.col,value)}
                            value={values[field.col] || ""}
                            type={field.type}
                            inputType={field.inputType}
                            label={field.label}
                            loading={field.async && field.options && field.options.length<=0}
                            options={(field.inputType === inputTypes.select || field.inputType === inputTypes.autoSuggest) &&
                            (field.options || asyncProps[field.asyncPropName])}
                            valid={field.validator && field.validator(values[field.col])}
                            showCheck={values[field.col] && field.validator}
                        />
                    )
                }
            </DialogContent>
            <DialogActions key={"actions"}>
                <Button onClick={onClose} color="primary">
                    {strings.cancel}
                </Button>
                <Button onClick={()=>this.props.onConfirm(values)} color="primary" autoFocus>
                    {strings.edit}
                </Button>
            </DialogActions>
        </React.Fragment>
    }
    handleChange = (prop,value) => this.setState({values: {...this.state.values,[prop]:value}})

}

export default EditDialogContent