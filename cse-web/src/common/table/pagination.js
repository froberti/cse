import TablePagination from "@material-ui/core/TablePagination/TablePagination";
import React from "react";
import {strings} from "../../localization/strings";

const CustomPagination = ({page,rowsPerPageOptions,count,rowsPerPage,handleChangePage,handleChangeRowsPerPage}) =>
    <TablePagination
        rowsPerPageOptions={rowsPerPageOptions}
        count={count}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
            'aria-label': 'Previous Page',
        }}
        nextIconButtonProps={{
            'aria-label': 'Next Page',
        }}
        labelDisplayedRows={ ({ from, to, count })  => `${from}-${to} ${strings.of} ${count}`}
        labelRowsPerPage={strings.rowsPerPage}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
    />;

export default CustomPagination