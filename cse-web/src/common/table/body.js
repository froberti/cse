import TableBody from "@material-ui/core/TableBody/TableBody";
import React from "react";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import EditIcon from "@material-ui/icons/Edit";
import IconButton from "@material-ui/core/IconButton";
import CustomCell from "./cell";
import Checkbox from "@material-ui/core/Checkbox";

const CustomTableBody = ({rows,cols,editable,onEditClick,deletable,onSelectRow,selectedRows,uniqueIdRowField}) =>
    <TableBody>
        {rows.map( (cell,cellIndex) =>
            <TableRow key={"cell"+cellIndex} hover className={"custom-row"}>
                {deletable &&
                    <TableCell padding="checkbox">
                        <Checkbox checked={selectedRows.map(row=> row[uniqueIdRowField]).includes(cell[uniqueIdRowField])} onClick={()=>onSelectRow(cell)}/>
                    </TableCell>
                }
                {
                    cols.map( (col,index) =>
                        !index?
                            <TableCell key={index} component="th" scope="row">{cell[col.name]}</TableCell>:
                            <CustomCell key={index} value={cell[col.name]} type={col.type}/>                    )
                }
                {
                    editable &&
                        <TableCell align="right">
                            <IconButton onClick={()=>onEditClick(cell)}>
                                <EditIcon/>
                            </IconButton>
                        </TableCell>
                }
            </TableRow>
        )}
    </TableBody>;

export default CustomTableBody