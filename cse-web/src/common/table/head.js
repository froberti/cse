import React from "react";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import TableSortLabel from "@material-ui/core/TableSortLabel/TableSortLabel";
import {strings} from "../../localization/strings";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";

const CustomTableHead = ({ order, orderBy, rows, handleSort, deletable, toggleAllRows, allSelected}) =>
    <TableHead>
        <TableRow>
            {deletable &&
                <TableCell padding="checkbox" className={"custom-table-cell"}>
                    <Checkbox checked={allSelected} onClick={toggleAllRows}/>
                </TableCell>
            }

            {rows.map(
                (row,key) => (
                    <TableCell
                        className={"custom-table-cell"}
                        key={key}
                        align={'left'}
                        sortDirection={orderBy === row.name ? order : false}
                    >
                        <Tooltip
                            title={strings.sort}
                            placement={'bottom-end'}
                            enterDelay={300}
                        >
                            <TableSortLabel
                                active={orderBy === row.name}
                                direction={order}
                                onClick={()=>handleSort(row.name)}
                            >
                                {row.label}
                            </TableSortLabel>
                        </Tooltip>
                    </TableCell>
                )
            )}
            <TableCell
                className={"custom-table-cell"}
                align={'right'}
            />
        </TableRow>
    </TableHead>;

export default CustomTableHead