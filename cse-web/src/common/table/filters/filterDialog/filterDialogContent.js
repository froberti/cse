import React from "react";
import {Button, DialogActions, DialogContent} from "@material-ui/core";
import {strings} from "../../../../localization/strings";
import {inputTypes} from "../../../../utils/types";
import Input from "../../input";

class FilterDialogContent extends React.Component{
    state={filterValues: {}};

    render(){
        const {handleClose,filters,asyncProps} = this.props;
        const {filterValues} = this.state;

        return [
            <DialogContent key={"content"}>
                <form className={"custom-form"}>
                    {(filters || []).map((filter,key)=>
                        <div key={key} className={"form-field-container"}>
                            <Input
                                onChange={(value)=>this.handleFilterChange(filter.col,value)}
                                value={filterValues[filter.col] || ""}
                                type={filter.type}
                                inputType={filter.inputType}
                                label={filter.label}
                                loading={filter.async && filter.options && filter.options.length<=0}
                                options={
                                    (filter.inputType === inputTypes.select || filter.inputType === inputTypes.autoSuggest) &&
                                    (filter.options || asyncProps[filter.asyncPropName])}
                                valid={filter.validator && filter.validator(filterValues[filter.col])}
                                showCheck={filterValues[filter.col] && filter.validator}
                            />
                        </div>
                    )}
                </form>
            </DialogContent>,
            <DialogActions key={"actions"}>
                <Button onClick={handleClose} color="primary">
                    {strings.cancel}
                </Button>
                <Button onClick={()=>this.props.handleFilterChange(filterValues)} color="primary" autoFocus>
                    {strings.filter}
                </Button>
            </DialogActions>
        ]
    }

    handleFilterChange = (name,value) => {
        let {filterValues} = this.state;
        this.setState({filterValues: {...filterValues,[name]:value}});
    };
}

export default FilterDialogContent