import React from "react";
import {Dialog,DialogTitle} from "@material-ui/core";
import FilterDialogContent from "./filterDialogContent";
import {strings} from "../../../../localization/strings";

const FilterModal = ({open,handleClose,filters,handleFilterChange,asyncProps}) =>
    <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
    >
        <DialogTitle id="alert-dialog-title">{strings.filterModalTitle}</DialogTitle>
        <FilterDialogContent
            handleClose={handleClose}
            filters={filters}
            asyncProps={asyncProps}
            handleFilterChange={handleFilterChange}
        />
    </Dialog>;

export default FilterModal