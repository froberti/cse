import React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, Typography} from "@material-ui/core";
import {strings} from "../../../localization/strings";

const ConfirmDeleteDialog = ({open,onClose,rows,onConfirm}) =>
    <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
    >
        <DialogTitle id="alert-dialog-title">{strings.confirmDeleteTitle}</DialogTitle>
        <DialogContent key={"content"}>
            {
                rows.map(item =>
                    <Typography variant="h6">
                        {JSON.stringify(item)}
                    </Typography>
                )
            }
        </DialogContent>,
        <DialogActions key={"actions"}>
            <Button onClick={onClose} color="primary">
                {strings.cancel}
            </Button>
            <Button onClick={onConfirm} color="primary">
                {strings.confirmDelete}
            </Button>
        </DialogActions>
    </Dialog>;

export default ConfirmDeleteDialog