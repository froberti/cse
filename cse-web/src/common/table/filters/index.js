import React from "react";
import {IconButton,Toolbar,Typography,Menu,MenuItem} from "@material-ui/core";
import MoreIcon from "@material-ui/icons/MoreVert";
import FilterModal from "./filterDialog/index";
import {strings} from "../../../localization/strings";
import DeleteIcon from "@material-ui/icons/Delete";
import ConfirmDeleteDialog from "./deleteDialog";

class CustomToolbar extends React.Component{
    state={modalIsOpen:false,menuAnchor:null,openDeleteConfirmDialog:false};
    render(){
        const {filters,tableTitle,asyncProps,selectedRows,deletable} = this.props;
        const {modalIsOpen,menuAnchor,openDeleteConfirmDialog} = this.state;
        return <div>
            {
                deletable && selectedRows && selectedRows.length>0 ?
                <Toolbar className={"filter-toolbar delete"}>
                    <Typography variant="h6" id={"table-title"}>
                        {selectedRows.length}
                    </Typography>
                    <div className={"spacer"} />
                    <div>
                        <IconButton onClick={this.openDeleteConfirmDialog}>
                            <DeleteIcon/>
                        </IconButton>
                    </div>
                </Toolbar>:
                <Toolbar className={"filter-toolbar"}>
                    <Typography variant="h6" id={"table-title"}>
                        {tableTitle}
                    </Typography>
                    <div className={"spacer"} />
                    <div>
                        <IconButton onClick={this.handleOpenMenu}>
                            <MoreIcon/>
                        </IconButton>
                    </div>
                </Toolbar>
            }
            <Menu
                anchorEl={menuAnchor}
                open={Boolean(menuAnchor)}
                onClose={this.handleCloseMenu}
            >
                <MenuItem onClick={this.handleOpenModal}>{strings.filter}</MenuItem>
            </Menu>
            <FilterModal
                handleClose={this.handleCloseModal}
                open={modalIsOpen}
                filters={filters}
                asyncProps={asyncProps}
                handleFilterChange={this.handleFilterChange}
            />
            <ConfirmDeleteDialog
                open={openDeleteConfirmDialog}
                onClose={this.closeDeleteConfirmDialog}
                rows={selectedRows}
                onConfirm={this.handleDeleteRowsClick}
            />
        </div>
    }
    handleDeleteRowsClick = () =>{
        this.closeDeleteConfirmDialog();
        this.props.deleteRows();
    };
    handleFilterChange = (filters) => {this.setState({modalIsOpen:false},this.props.handleFilterChange(filters))};
    handleCloseMenu = () => this.setState({menuAnchor:null});
    handleOpenMenu = (anchor) => this.setState({menuAnchor:anchor.currentTarget});
    handleCloseModal = () => this.setState({modalIsOpen:false});
    handleOpenModal = () => {this.setState({modalIsOpen:true});this.handleCloseMenu()};
    openDeleteConfirmDialog = () => this.setState({openDeleteConfirmDialog: true});
    closeDeleteConfirmDialog = () => this.setState({openDeleteConfirmDialog: false});
}

export default CustomToolbar