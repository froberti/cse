import React from 'react';
import Table from '@material-ui/core/Table';
import Paper from '@material-ui/core/Paper';
import CustomToolbar from "./filters";
import CustomPagination from "./pagination";
import CustomTableHead from "./head";
import CustomTableBody from "./body";
import EditDialog from "./editDialog";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import {connect} from "react-redux";
import {openErrorToast} from "../../features/snackbar";

const ROWS_PER_PAGE_OPTIONS = [5,15,20];

let orderDirections = {
    asc: "asc",
    desc: "desc"
};

class CustomTable extends React.Component {
    state = {
        orderDirection: orderDirections.asc,
        orderBy: 1,
        selectedRows: [],
        rowsPerPage: 15,
        editRow: null,
        editDialogOpen: false,
        addDialogOpen: false,
    };

    render() {
        const {orderDirection, orderBy, selectedRows, rowsPerPage, editRow, editDialogOpen, addDialogOpen} = this.state;
        const {filters, tableTitle, editable, columns, rows, page, totalItems, asyncProps, editableFields, deletable, uniqueIdRowField} = this.props;
        return (
            <Paper className={"table-wrapper"}>
                <CustomToolbar
                    filters={filters}
                    asyncProps={asyncProps}
                    handleFilterChange={this.handleFilterChange}
                    tableTitle={tableTitle}
                    selectedRows={selectedRows}
                    deleteRows={this.onDeleteRows}
                    deletable={deletable}
                />
                <Table aria-labelledby="tableTitle">
                    <CustomTableHead
                        deletable={deletable}
                        toggleAllRows={this.toggleAllRows}
                        allSelected={selectedRows.length === rows.length}
                        order={orderDirection}
                        orderBy={orderBy}
                        rows={columns}
                        handleSort={this.onSort}
                    />
                    <CustomTableBody
                        uniqueIdRowField={uniqueIdRowField}
                        deletable={deletable}
                        editable={editable}
                        onEditClick={this.onEditClick}
                        rows={rows}
                        onSelectRow={this.onSelectRow}
                        cols={columns}
                        selectedRows={selectedRows}
                    />
                    <CustomPagination
                        page={page}
                        rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
                        count={totalItems}
                        rowsPerPage={rowsPerPage}
                        handleChangePage={this.handleChangePage}
                        handleChangeRowsPerPage={this.handleChangeRowsPerPage}
                    />
                </Table>

                <div className={"centered"}>
                    <IconButton onClick={this.openAddDialog}>
                        <AddIcon/>
                    </IconButton>
                </div>
                <EditDialog open={editDialogOpen}
                            onClose={this.closeEditDialog}
                            editableFields={editableFields}
                            onConfirm={this.handleConfirmEdit}
                            asyncProps={asyncProps}
                            row={editRow}/>
                <EditDialog open={addDialogOpen}
                            onClose={this.closeAddDialog}
                            editableFields={editableFields}
                            onConfirm={this.handleConfirmAddRow}
                            asyncProps={asyncProps}
                            title={"Nuevo"}
                            row={{}}/>
            </Paper>
        );
    }

    componentDidMount() {
        this.loadRows(0)
    };
    loadRows = (page, filters) => {
        const {
            orderDirection,
            orderBy,
            rowsPerPage,
        } = this.state;
        this.props.getRows({orderDirection, orderBy, rowsPerPage, page, ...(filters || {})});
    };
    handleChangePage = (event, page) => this.loadRows(page);
    handleChangeRowsPerPage = event => this.setState({rowsPerPage: event.target.value});
    onSort = (property) => {
        let {orderDirection, orderBy} = this.state;
        let newOrderDirection = orderDirections.asc;

        if (orderBy === property)
            newOrderDirection = (orderDirection === orderDirections.asc) ? orderDirections.desc : orderDirections.asc;

        this.setState({orderDirection: newOrderDirection, orderBy: property}, () => this.loadRows(0));
    };
    handleConfirmEdit = (newRowInfo) => {
        this.props.editRow(newRowInfo);
        this.closeEditDialog();
    };
    handleFilterChange = (filters) => this.loadRows(0, filters);
    onEditClick = (row) => this.setState({editRow: row, editDialogOpen: true});
    closeEditDialog = () => this.setState({editDialogOpen: false});
    closeAddDialog = () => this.setState({addDialogOpen:false});
    openAddDialog = () => this.setState({addDialogOpen:true});
    handleConfirmAddRow = (newRow) => {

        let length = this.props.editableFields.length;
        for(let i=0; i<length; i++){
            if(this.props.editableFields[i].validator &&  ! this.props.editableFields[i].validator(newRow[this.props.editableFields[i].col])){
                console.log(this.props.editableFields[i]);
                return this.props.dispatch(openErrorToast({msg: "FUUU"}));
            }
        }

        this.props.addRow(newRow);
        this.closeAddDialog();
    };
    onSelectRow = (row) => {
        const filteredRows = this.state.selectedRows.filter(item => item[this.props.uniqueIdRowField] !== row[this.props.uniqueIdRowField]);
        if( filteredRows.length !== this.state.selectedRows.length )
            this.setState({selectedRows: filteredRows});
        else
            this.setState({selectedRows: [...this.state.selectedRows,row]})
    };
    toggleAllRows = () => this.setState({selectedRows: this.state.selectedRows.length===0? this.props.rows:[]});
    onDeleteRows = () => {
        this.props.deleteRows(this.state.selectedRows);
        this.setState({selectedRows:[]})
    }
}

export default connect()(CustomTable);