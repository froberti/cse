import TableCell from "@material-ui/core/TableCell/TableCell";
import React from "react";
import DownloadIcon from "@material-ui/icons/CloudDownload";
import IconButton from "@material-ui/core/IconButton";
import LinkIcon from "@material-ui/icons/Link";
import axios from "axios";

let cellTypes = {
    link: "link",
    download: "file",
    raw: "raw"
};

const CustomCell = ({type,value}) => {
    switch(type){
        case cellTypes.raw: return <TableCell component="th" scope="row">{value}</TableCell>;
        case cellTypes.download: return <TableCell component="th" scope="row"><IconButton><DownloadIcon/></IconButton></TableCell>;
        case cellTypes.link: return <TableCell component="th" scope="row"><a href={"http://"+value} target="_blank" rel="noopener noreferrer"><IconButton><LinkIcon/></IconButton></a></TableCell>;
        default: return <TableCell component="th" scope="row">{value}</TableCell>;
    }
};

const downloadFile = (url,fileName) =>{
    axios.get(url,{responseType: 'blob'})
        .then(response =>{
            const objectUrl = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = objectUrl;
            link.setAttribute('download', fileName );
            document.body.appendChild(link);
            link.click();
        });
};



export default CustomCell

