import React from "react"
import { Pagination,Navigation } from 'swiper/dist/js/swiper.esm'
import Swiper from "../swiper";

const params = {
    slidesPerView: 1,
    grabCursor: true,
    modules: [Pagination,Navigation ],
    pagination: {
        el: '.swiper-pagination.customized-swiper-pagination',
    },
    navigation: {
        nextEl: '.swiper-button-next.customized-swiper-button-next', // Add your class name for next button
        prevEl: '.swiper-button-prev.customized-swiper-button-prev' // Add your class name for prev button
    },
};

const ImagesSlider = ({images}) =>
    <div className="images-slider">
        <Swiper items={(images||[]).map(image => <img src={image} alt={"Item"} className={"item-image"}/>)} containerClass={"image"}/>
    </div>;

export default ImagesSlider