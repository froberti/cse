import React from "react";

const Divider = () => <div className={"divider-container"}>
    <div className="divider"/>
</div>;

export default Divider