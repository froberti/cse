import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import {routes} from "../../router/routes";

const formatTitle = (title) => title.replace("./","").replace(".jpg","").replace(".jpeg","").replace(".png","");

function importAll(r) {
    let images = {};
    r.keys().map((item, index) => {
        const title = formatTitle(item);
        images[title] = {icon: r(item),title};
        return item
    });
    return images;
}

const icons = importAll(require.context('../../assets/images/main-icons', false, /\.(png|jpe?g)$/));

const iconDescriptions = [
    {background: "#000642",...icons["KIT ROBOTICA PRIMARIA Y SEC"],isProduct:true},
    {background: "#009F8B",...icons["KIT ROBOTICA INICIAL"],isProduct:true},
    {background: "#004B7B",...icons["KIT ROBOTICA HUMANOIDE"],isProduct:true},
    {background: "rgb(137, 17, 92)",...icons["KIT DRONES DIY"],isProduct:true},
    {background: "#E7BE00",...icons["CASCO REALIDAD VIRTUAL"],isProduct:true},
    {background: "rgb(237, 135, 0)",...icons["ESCANER 3D"],isProduct:true},
    {background: "rgb(191, 48, 97)",...icons["IMPRESORA 3D"],isProduct:true},
    {background: "rgb(108, 177, 35)",...icons["LAPIZ 3D"],isProduct:true},
    {background: "rgb(2, 14, 67)",...icons["HOLOGRAMAS"],isProduct:true},
    {background: "rgb(237, 135, 0)",...icons["VISOR HDMI"],isProduct:true},
    {background: "#C03269",...icons["CARGADOR SOLAR"],isProduct:true},
    {background: "rgb(230, 190, 0)",...icons["MOCHILA SOLAR"],isProduct:true},
    {background: "#6CB123",...icons["CUADERNO INTELIGENTE"],isProduct:true},
    {background: "#009F8B",...icons["UNIFORME"],isProduct:true},
    {background: "#009F8B",...icons["APRENDIZAJE BASADO EN PROYECTOS"],link: "/",isCourse:true},
    {background: "rgb(198, 44, 91)",...icons["PRIMER USO DE PDI"],link: "/",isCourse:true},
    {background: "rgb(237, 135, 0)",...icons["PREPARACION DINAMIZADOR"],link: "/",isCourse:true},
    {background: "rgb(108, 177, 35)",...icons["PREPARACION ASESOR TECNICO"],link: "/",isCourse:true},
    {background: "rgb(176, 25, 83)",...icons["CURSOS Y TALLERES DE PROGRAMACION"],link: "/",isCourse:true},
    {background: "rgb(2, 14, 67)",...icons["CURSO ROBOTICA"],link: "/",isCourse:true},
    {background: "#00A2A2",...icons["INTELIGENCIA ARTIFICIAL"],link: "/",isCourse:true},
    {background: "#004B7B",...icons["CURSOS DRONES"],link: "/",isCourse:true},
    {background: "rgb(137, 17, 92)",...icons["CURSOS 3D"],link: "/",isCourse:true},
    {background: "#E6BE00",...icons["CURSO REALIDAD VIRTUAL"],link: "/",isCourse:true},
    {background: "rgb(89, 32, 131)",...icons["FOTOGRAFIA"],link: "/"},
    {background: "#ED8700",...icons["CONTROL ESTRICTO DE EQUIPAMIENTO"],link: "/"},
    {background: "#009F86",...icons["CALL CENTER SOPORTE"],link: "/"},
    {background: "rgb(230, 190, 0)",...icons["SOLUCION SOLAR Y LED"],link: "/sisyl"},
    {background: "#005DA1",...icons["CLEAN IT"],link: "/cleanit"},
    {background: "#009F86",...icons["SOPORTE TECNICO"],link: "/"},
    {background: "rgb(237, 135, 0)",...icons["INSTALACION EQUIPAMIENTO TECNOLOGICO"],link: "/"},
    {background: "rgb(220, 60, 98)",...icons["REVISION INSTALACION ELECTRICA"],link: "/"},
    {background: "#004B7B",...icons["MANTENIMIENTO"],link: "/"},
    {background: "rgb(208, 72, 122)",...icons["GARANTIA"],link: "/"},
    {background: "#89115C",...icons["CABLEADO ESTRUCTURAL"],link: "/"},
    {background: "#E6BE00",...icons["ASESORAMIENTO DE PLANIFICACION"],link: "/"},
    {background: "#6CB123", ...icons["ACOMPANAMIENTO DESARROLLO EDUCATIVO"],link: "/"},
    {background: "rgb(220, 60, 98)",...icons["SOFTWARE DE GESTION"],link: "/"},
    {background: "#009F8B", ...icons["ACTUALIZACIONES DE SOFTWARE COMPUTER CENTER"],link: "/"},
    {background: "#ED8700",...icons["ACTUALIZACIONES DE SOFTWARE TABLETS"],link: "/"},
    {background: "#004B7B",...icons["TRACKING"],link: "/"},
    {background: "#A1095D",...icons["SEGUROS MEDICOS"],link: "/"},
    {background: "#FF8800",...icons["SEGUROS CIVILES"],link: "/"}
];
//style={{backgroundColor: `rgb(${icon.rgb.r},${icon.rgb.g},${icon.rgb.b})`}}

const MainIcons = ({scrollToProducts,history}) => {
    function handleItemClick(item){
        if(item.isProduct)
            return scrollToProducts();

        if(item.isCourse) {
            window.scrollTo(0,0);
            return history.push(routes.themes.path)
        }

        if(item.link)
            return history.push(item.link)
    }

    return <div id={"main-icons-container"} className={"common-container"}>
        <Typography variant={"h3"} align={"center"} className={"title grey-colored"}>Cursos, productos y
            servicios</Typography>
        <div id={"main-icons"}>
            {
                iconDescriptions.map((icon, index) =>
                    <Paper key={index} style={{cursor: "pointer",backgroundColor: iconDescriptions [index].background}}
                           onClick={()=>handleItemClick(icon)}>
                        <Typography variant="subtitle1" align="center">
                            {icon.title}
                        </Typography>
                        <img src={icon.icon} alt={icon.title}/>
                    </Paper>
                )
            }
        </div>
    </div>;
};

export default MainIcons