import React from "react"
import Button from "@material-ui/core/Button"

const ProductButtons = ({onClick,products,selected}) =>
    <div className="product-buttons">
        <div className={"buttons"}>
            {products.map( (product,key) =>
                <div key={key} onClick={()=>onClick(key)} className={`button ${selected ? "selected":""}`}>
                    <Button color="primary">
                        {product.name}
                    </Button>
                </div>
            )}
        </div>
    </div>;

export default ProductButtons