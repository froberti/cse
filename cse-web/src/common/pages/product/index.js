import React,{useState} from "react";
import Typography from "@material-ui/core/Typography";
import Product from "./product";
import ProductButtons from "./buttons";
import {Waypoint} from "react-waypoint";

const products = [
    {
        name: "Mochila solar",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam arcu erat, ultricies non lorem a, sollicitudin sodales tortor. Etiam aliquet elit sit amet faucibus rhoncus. Sed ut tincidunt mauris. Mauris vehicula, lectus sit amet placerat rhoncus, urna nulla faucibus eros, ac maximus neque urna a risus. Ut consequat, augue at pharetra porta, ligula nulla condimentum ipsum, a tincidunt massa leo sodales sem. Nunc augue ligula, blandit efficitur vulputate ac, sollicitudin tempor nunc. Vestibulum nec magna in neque auctor commodo. Phasellus lectus dui, tempor eu urna ac, imperdiet ultricies est.",
        images: [
            "https://home.ripley.cl/store/Attachment/WOP/D340/2000361004233/2000361004233_2.jpg",
            "https://home.ripley.cl/store/Attachment/WOP/D340/2000361004233/2000361004233_2.jpg",
            "https://home.ripley.cl/store/Attachment/WOP/D340/2000361004233/2000361004233_2.jpg",
            "https://home.ripley.cl/store/Attachment/WOP/D340/2000361004233/2000361004233_2.jpg"
        ]
    },
    {
        name: "Uniformes",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam arcu erat, ultricies non lorem a, sollicitudin sodales tortor. Etiam aliquet elit sit amet faucibus rhoncus. Sed ut tincidunt mauris. Mauris vehicula, lectus sit amet placerat rhoncus, urna nulla faucibus eros, ac maximus neque urna a risus. Ut consequat, augue at pharetra porta, ligula nulla condimentum ipsum, a tincidunt massa leo sodales sem. Nunc augue ligula, blandit efficitur vulputate ac, sollicitudin tempor nunc. Vestibulum nec magna in neque auctor commodo. Phasellus lectus dui, tempor eu urna ac, imperdiet ultricies est.",
        images: [
            "https://static1.squarespace.com/static/5005b94fc4aa8b4d97618bfb/53d090b4e4b0625ce7aa0269/53d090c9e4b0625ce7aa0288/1406177504753/uniform+long+sleeve.png",
            "https://home.ripley.cl/store/Attachment/WOP/D340/2000361004233/2000361004233_2.jpg",
            "https://home.ripley.cl/store/Attachment/WOP/D340/2000361004233/2000361004233_2.jpg",
            "https://home.ripley.cl/store/Attachment/WOP/D340/2000361004233/2000361004233_2.jpg"
        ]
    }
];

const ProductPage = ({title,subtitle,
                     //products
}) => {
    const [productIndex, setIndex] = useState(0);
    const [active, setActive] = useState(false);

    return <section className={"page product"}>
        <Waypoint
            onEnter={() => setActive(true)}
            onLeave={() => setActive(false)}
        >
            <div className="text-container active center-all-content">
                <Typography variant={"h2"}>{title}</Typography>
                <Typography variant={"subtitle1"}>{subtitle}</Typography>
            </div>
        </Waypoint>
        <ProductButtons onClick={setIndex} products={products} selected={productIndex}/>
        <Product product={products[productIndex]} key={productIndex}/>
    </section>
};

export default ProductPage