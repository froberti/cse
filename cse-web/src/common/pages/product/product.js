import React from "react";
import Typography from "@material-ui/core/Typography";

const Product = ({product}) => {

    return <div className={"product-container " + product.name}>
            <div className={"image-container slide-in-right-left"}>
                <img src={product.images[0]} alt={product.name}/>
            </div>
            <div className={"slide-in-left-right"}>
                <Typography variant={"h5"}>{product.name}</Typography>
                <Typography variant={"h6"}>{product.description}</Typography>
            </div>
        </div>
};

export default Product