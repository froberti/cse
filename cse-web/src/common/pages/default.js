import React,{useState} from "react";
import { Waypoint } from 'react-waypoint';

const DefaultPage = ({children,className,id}) =>{
    const [active, setActive] = useState(false);

    return <Waypoint
        onEnter={()=>setActive(true)}
        onLeave={()=>setActive(false)}
    >
        <section id={id||""} className={`page ${active?"active":"inactive"} ${className || ""}`}>
            {active && children}
        </section>
    </Waypoint>
};

export default DefaultPage