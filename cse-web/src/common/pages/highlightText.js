import React,{useState} from "react";
import Typography from "@material-ui/core/Typography";
import {Waypoint} from "react-waypoint";
import BagImage from "../../assets/images/shopping-bag.svg";
import TimelineLite from "gsap/TimelineLite";

const HightlightText = ({text,subtitle,image,link}) => {
    const [active, setActive] = useState(false);

    function onEnter() {
        let tl = new TimelineLite();
        tl.to("."+text.replace(/\s/g,''), 1, {backgroundPosition: "-100% 0"});
    }
    function onLeave() {
        let tl = new TimelineLite();
        tl.to("."+text.replace(/\s/g,''), 0.1, {backgroundPosition: "0 0"});
    }
    return <div className={"highlight common-container"}>
            <div className={"texts-container center-all-content"}>
                <Waypoint
                    onEnter={onEnter}
                    onLeave={onLeave}
                >
                    <div>
                        <Typography variant={"h5"} className={"animated highlighter "+text.replace(/\s/g,'')}>
                            {text}
                        </Typography>
                    </div>
                </Waypoint>
                <Typography variant={"h6"}>
                    {subtitle}
                </Typography>
            </div>
            <div className={"center-all-content"}>
                <a href={link || "#"} target={"_blank"}>
                    <img src={image || BagImage} alt="Highlight"/>
                </a>
            </div>
        </div>;
};
export default HightlightText


