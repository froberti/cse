import React,{useState} from "react";
import Grid from "@material-ui/core/Grid";
import Slide from "./slidesContainer";
import {Waypoint} from "react-waypoint";
import ProductIcon from "@material-ui/icons/ShoppingCart";
import EditIcon from "@material-ui/icons/Edit";
import SupportIcon from "@material-ui/icons/Call";
//Mock
const slides =[
    {
        icon: <ProductIcon className={"slide-icon"}/>,
        background: "#F05926",
        title: "Productos",
        children: [
            {
                title: "Recuros Escolares 4ta Revolución Industrial",
                description: [
                    "Kit Robótica nivel Inicial",
                    "Kit Robótica Primaria y Secundaria",
                    "Kit Robótica Humanoide Secundaria",
                    "Kit Drones DIY",
                    "Casco Realidad Virtual",
                    "Escáner 3D",
                    "Impresora 3D",
                    "Lápiz 3D",
                ]
            },
            {
                title: "Recuros Escolares",
                description: [
                    "Hologramas",
                    "Visor celular HDMI",
                    "Cargador Solar",
                    "Mochila Solar",
                    "Cuadernos inteligentes",
                    "Uniformes",
                ]
            }
        ]
    },
    {
        icon: <EditIcon className={"slide-icon"}/>,
        background: "#FE05F0",
        title: "Capacitaciones",
        children: [
            {
                title: "Educación Digital",
                description: [
                    "Aprendizaje Basado en Proyectos",
                    "Primer uso de PDI, Directivo, Docente y Maestranza",
                    "Preparación Profesional del Dinamizador Pedagógico Digital",
                    "Preparación Profesional del Asesor Técnico",
                ]
            },
            {
                title: "Espacio Maker",
                description: [
                    "Cursos y Talleres de Programación",
                    "Curso Introducción a la programación",
                    "Curso Programación Intermedia",
                    "Curso Programación Avanzada",
                    "Curso El Pensamiento Computacional de cada día",
                    {subtitle:"Cursos y Talleres de Robótica"},
                    "Curso Mi primer robot (Básico)",
                    "Curso Mi primer robot (Intermedio)",
                    "Curso Mi primer robot (Avanzado)",
                    "Curso Introducción a la Robótica Educativa",
                    "Curso Robótica Educativa para Inicial",
                    "Curso Robótica Educativa para Primaria",
                    "Curso Robótica Educativa para Secundaria",
                    {subtitle:"Cursos y Talleres de Inteligencia Artificial"},
                    {subtitle:"Cursos y Talleres de Drones"},
                    "Curso Hacé tu primer Drone",
                    "Curso Usos del drone en el campo",
                    {subtitle:"Cursos y Talleres de Diseño e impresión 3D"},
                    "Curso Diseño 3D educativos",
                    "Conceptos básicos de la impresión 3D fuera y dentro del aula",
                    {subtitle:"Cursos y Talleres de Realidad Virtual,Realidad Aumentada y Hologramas"},
                    "Curso Gamificación con Realidad Aumentada en el cole",
                    "Curso Aprendiendo con Realidd Virtual",
                ]
            }
        ]
    },
    {
        icon: <SupportIcon className={"slide-icon"}/>,
        background: "#F05926",
        title: "Soporte",
        children: [
            {
                title: "Servicios más innovadores del mercado",
                description: [
                    "Control Estricto del Equipamiento (hacer dossier)",
                    "Call Center de Soporte (hacer dossier)",
                    "Sisyl Led y solar (dossier hecho)",
                    "CleanIT Limpieza Inteligente (hacer dossier)",
                ]
            },
            {
                title: "Servicio técnico de Hardware",
                description: [
                    "Soporte técnico en línea",
                    "Instalación de Equipamiento Tecnológico",
                    "Relevisión de Instalación Eléctrica para instalación de la tecnología",
                    "Mantenimiento Correctivo y preventivo",
                    "Grarantías de productos instalados",
                    "Cableado estructurado para redes",
                ]
            },
            {
                title: "Consultoría Técnica y Pedagógica",
                description: [
                    "Asesoramiento de Planificación para desarrollo en educación digital",
                    "Acompañamiento en el desarrollo educativo"
                ]
            },
            {
                title: "Actualizaciones - Servicio técnico de Software",
                description: [
                    "Software de Gestión",
                    "Actualización Software de Computer Center",
                    "Actualización Software de tablets",
                ]
            },
            {
                title: "Extras",
                description: [
                    "Tracking",
                    "Seguros Médicos",
                    "Seguros Civiles",
                    "Fotografías fin de curso"
                ]
            }
        ]
    }
];

const Items = () =>{
    return <Grid container className="fade-in-items-container page">
                {slides.map((slide,key)=>
                    <Slide slide={slide} key={key} index={key}/>
                )}
            </Grid>
};

export default Items