import React,{useState,useEffect} from "react";
import { TimelineLite, CSSPlugin, AttrPlugin }  from "gsap/all";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Waypoint} from "react-waypoint";

const plugins = [ CSSPlugin, AttrPlugin ];

const Slide_ = ({slide}) =>
    <React.Fragment>
        {slide.icon}
        <Typography variant={"h5"} className={"title text"}>
            {slide.title}
        </Typography>
        {
            slide.children.map( (child,index) =>
                <React.Fragment key={index}>
                    <Typography variant={"h6"} className={"text"}>
                        {child.title}
                    </Typography>
                    {
                        child.description.map( (description,key) =>
                            <Typography variant={"subtitle2"} key={key}
                                        className={"text " + ((description.subtitle && "bold") || "")}>
                                {description.subtitle || description}
                            </Typography>
                        )
                    }
                </React.Fragment>
            )
        }
    </React.Fragment>;

class Slide extends React.PureComponent{
    tween=null;
    render(){
        const {slide,index} = this.props;
        return <Grid item xs={12} sm={6} md={4} className={"slide-container slide-container-"+(index)}>
            <Waypoint
                onEnter={this.animateEntrance}
                onLeave={this.revertEntranceAnimation}
            >
                <div className={"slide-content with-shadow"} style={{background: slide.background}}>
                    <Slide_ slide={slide}/>
                </div>
            </Waypoint>
        </Grid>
    }
    animateEntrance = () =>{
        const {index} = this.props;
        let tl = new TimelineLite();
        this.tween = tl.from(".slide-container-"+index, 1+((index && index*1.2)||0), {opacity:0});
    };
    revertEntranceAnimation = () =>{
        const {index} = this.props;
        let tl = new TimelineLite();
        tl.to(".slide-container-"+index, 0.2, {opacity:1});
    };
    animateHover = () =>{
        const {index} = this.props;
        let tl = new TimelineLite();
        tl.to(".slide-container-"+index, 0.5, {transform:"rotateY(180deg)"});
        tl.to(".slide-container-"+index, 0.5, {transform:"rotateY(360deg)"});
    }
}

export default Slide