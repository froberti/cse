import React,{useState,useEffect} from "react";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
// import Grid from "@material-ui/core/Grid/Grid";
// import Paper from "@material-ui/core/Paper";
// import DefaultCourseImage from "../../assets/images/main-logo.png";
import {strings} from "../../localization/strings";
import {Waypoint} from "react-waypoint";
import { TimelineLite, CSSPlugin, AttrPlugin }  from "gsap/all";

const plugins = [ CSSPlugin, AttrPlugin ];
//Mock
//const themes = [{ name:"Programación" ,courses: [{name: "Curso 1", image: "https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"}] }];

const Course = ({title,subtitle,themes,handleSeeMoreClick,handleCourseClick }) =>{
    const [active, setActive] = useState(false);
    useEffect(() => {
        let tl = new TimelineLite();
        tl.to(".arrow", 3, {transform:"rotate(45deg)",opacity:1});
    });
    return <section className={"course-container page"}>
        <Waypoint
            onEnter={() => setActive(true)}
            onLeave={() => setActive(false)}
        >
            <div className={"text-container center-all-content"}>
                <Typography variant={"h2"} className={"grey-colored"}>{title}</Typography>
                <Typography variant={"subtitle1"}  className={"grey-colored"}>{subtitle}</Typography>
                {/*<div className="courses-buttons">*/}
                {/*{ themeValues && themeValues.map( (theme,index) =>*/}
                {/*<Button key={index} variant={"outlined"} color={"secondary"} onClick={()=>setIndex(index)}>{theme.name}</Button>)}*/}
                {/*</div>*/}
                {
                    active &&
                    <div className="arrow"/>
                }
                <div className="courses-buttons">
                    <Button variant={"outlined"}
                            className={"color-fade-background"}
                            onClick={handleSeeMoreClick}>
                        {strings.seeMore}
                    </Button>
                </div>
            </div>
        </Waypoint>
    </section>;
};

export default Course