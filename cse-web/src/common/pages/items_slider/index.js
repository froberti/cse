import React from "react"
import Typography from "@material-ui/core/es/Typography";
import Swiper from 'react-id-swiper';
import RenderOnScroll from "../../renderOnScroll/index";
//import { Pagination, Navigation } from 'swiper/dist/js/swiper.esm'
import {EffectCoverflow, Navigation, Pagination} from 'swiper/dist/js/swiper.esm'

// const items = [
//     {image: "https://home.ripley.cl/store/Attachment/WOP/D340/2000361004233/2000361004233_2.jpg",title: "Mochila solar"},
//     {image: "https://previews.123rf.com/images/yulia87/yulia871709/yulia87170900009/86383784-vector-conjunto-de-%C3%BAtiles-escolares-volver-al-fondo-de-la-escuela-con-papeler%C3%ADa.jpg",title: "Recursos Escolares"},
//     {image: "https://static1.squarespace.com/static/5005b94fc4aa8b4d97618bfb/53d090b4e4b0625ce7aa0269/53d090c9e4b0625ce7aa0288/1406177504753/uniform+long+sleeve.png",
//         title: "Uniformes"}];

const params =     (window.innerWidth <= 700) ?
    {
        modules: [EffectCoverflow],
        effect: 'coverflow',
        centeredSlides: true,
        slidesPerView: 2,
        loop:true
    }:{
        modules: [EffectCoverflow,Pagination,Navigation ],
        effect: 'coverflow',
        centeredSlides: true,
        slidesPerView: 2,
        pagination: {
            el: '.swiper-pagination.customized-swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next.customized-swiper-button-next', // Add your class name for next button
            prevEl: '.swiper-button-prev.customized-swiper-button-prev' // Add your class name for prev button
        },
        loop:true
    };
    // {
    //     spaceBetween: 30,
    //     slidesPerView: 5,
    //     grabCursor: true,
    //     centeredSlides: true,
    //};

const ItemsSlider = ({items,title,subtitle,handleItemClick}) =>
    <div className="items-slider">
        <div className="text-container center-all-content slide-in-right-left">
            <Typography variant={"h2"}>{title}</Typography>
            <Typography variant={"subtitle1"}>{subtitle}</Typography>
        </div>

        <Swiper {...params}>
            {
                (items||[]).map( (item,key) =>
                    <div className="item shadow-on-hover" key={key}>
                        <div className={"item-image-containter"} onClick={()=>handleItemClick(key)}>
                            <img src={item.image || (item.images && item.images[0])} alt={item.title} className={"item-image"}/>
                        </div>
                        <Typography variant={"h6"} className={"item-title"}>{item.title}</Typography>
                    </div>
                )
            }
        </Swiper>
    </div>;

export default ItemsSlider