import React from "react"
import Product from "./product/index";
import Service from "./service";
import Course from "./course";
import {sectionTypes} from "./types";

const Section = (props) => {
    switch(props.type){
        case sectionTypes.course: return <Course {...props}/>;
        case sectionTypes.product: return <Product {...props}/>;
        case sectionTypes.service: return <Service {...props}/>;
        default: return null;
    }
};

export default Section