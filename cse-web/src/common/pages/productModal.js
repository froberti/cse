import React from "react"
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import CustomSwiper from "../swiper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const formatDescription = (descriptionArray) => {
    const length = descriptionArray.length;
    //const maxItemsPerColumn = Math.ceil(length/2);
    const maxItemsPerColumn = 6;
    let columns = 1;
    if(length > maxItemsPerColumn)
        columns = 2;
    let description = [];
    for(let i = 0; i<columns; i++)
        description[i] =
            <Grid key={i} item xs={12} md={12/columns} className={"product-description-col"}>
                {descriptionArray.slice(i*maxItemsPerColumn,(i+1)*maxItemsPerColumn).map( (item,key) =>
                    <Typography variant={"subtitle2"} key={key}>{item}</Typography>)
                }
            </Grid>;
    return description;
};

const ProductModal = ({open,handleClose,product}) =>
    <Dialog
        open={open}
        onClose={handleClose}
        maxWidth={"lg"}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
    >
        {
            product &&
            <React.Fragment>
                <DialogTitle className={"product-dialog-title"} disableTypography onClose={handleClose}>
                    <div className={"spacer"}/>
                    <IconButton aria-label="Close" onClick={handleClose}>
                        <CloseIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent>
                    <Grid container className="product-modal-container">
                        <Grid item xs={12} md={6}>
                            <CustomSwiper
                                items={product.images.map((item,key)=> <img src={item} key={key} alt={product.title}/>)}
                                containerClass={"product-images"}
                            />
                        </Grid>
                        <Grid item xs={12} md={6} className={"product-description"}>
                            <Typography variant={"h5"}>{product.title}</Typography>
                            <Typography variant={"h6"}>{product.subtitle}</Typography>
                            <Grid container>
                                {
                                    product.description && formatDescription(product.description)
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                </DialogContent>
            </React.Fragment>
        }
    </Dialog>;

export default ProductModal