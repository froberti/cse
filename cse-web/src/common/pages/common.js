import React from "react"
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {strings} from "../../localization/strings";

const title = "Soporte";
const subtitle = "Tenemos un equipo dedicado 24hs a responder tus dudas y brindar soporte";
const image= "https://images.unsplash.com/photo-1529348915581-73628f0cf212?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80";

const CommonPage = ({}) =>
    <section className="page common-page first center-all-content" style={{background: `url(${image})`}}>
        <div className="centered-box center-all-content">
            <div className="titles">
                <Typography variant={"h1"}>{title}</Typography>
                <Typography variant={"h6"}>{subtitle}</Typography>
                <div className="divider"/>
            </div>
            <div className="button-container">
                <Button variant={"contained"} className={"color-fade-background"} size="large"  >
                    {strings.seeMore}
                </Button>
            </div>
        </div>

        <div className="overlay"/>
    </section>;

export default CommonPage