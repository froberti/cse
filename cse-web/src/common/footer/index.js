import React from "react"
import Typography from "@material-ui/core/Typography";
import {strings} from "../../localization/strings";

const Footer = ( ) =>
    <footer>
        <Typography variant={"overline"} className={"footer-text"}>{strings.footer}</Typography>
    </footer>;

export default Footer