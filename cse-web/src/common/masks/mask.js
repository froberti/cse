import React from "react";

const Mask = (props) =>  <svg  {...props}
                         width="100%"
                         height="100%"
                         viewBox="0 0 100 100"
                         className={"rounded-mask "+ (props.className||"")}
                         preserveAspectRatio="none">
                        <path d="M0 100 L 0 0 C 25 100 75 100 100 0 L 100 100" id="bottom-curve" fill="white"/>
                    </svg>;

export default Mask