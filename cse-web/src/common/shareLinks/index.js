import React from "react"
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import {getShareIcon} from "../../utils/share";

const ShareLinks = ({links}) =>
    <section className={"share-links"}>
        <Paper className={"share-links-container"}>
            {
                links.map( (link,index) =>
                    <a href={link.link} target={"_blank"} key={link.title}>
                        <Tooltip title={link.title} placement={index%2===0?"top":"bottom"} >
                            <IconButton className={"share-link-button"}>
                                {getShareIcon(link.icon)}
                            </IconButton>
                        </Tooltip>
                    </a>
                )
            }
        </Paper>
    </section>;

export default ShareLinks