import React from "react"
import Swiper from 'react-id-swiper';
import { Pagination,Navigation } from 'swiper/dist/js/swiper.esm'

const params = {
    slidesPerView: 1,
    grabCursor: true,
    modules: [Pagination,Navigation ],
    pagination: {
        el: '.swiper-pagination.customized-swiper-pagination',
    },
    navigation: {
        nextEl: '.swiper-button-next.customized-swiper-button-next', // Add your class name for next button
        prevEl: '.swiper-button-prev.customized-swiper-button-prev' // Add your class name for prev button
    },
};

const CustomSwiper = ({items,containerClass}) =>
    <div className="images-slider">
        <Swiper {...params}>
            {
                (items||[]).map( (item,key) =>
                    <div className={containerClass} key={key}>
                        {item}
                    </div>
                )
            }
        </Swiper>
    </div>;

export default CustomSwiper