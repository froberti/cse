import React from "react";
import Facebook from "../common/shareLinks/icons/facebook";
import Instagram from "../common/shareLinks/icons/instagram";
import Linkedin from "../common/shareLinks/icons/linkedin";
import Twitter from "../common/shareLinks/icons/twitter";
import Whatsapp from "../common/shareLinks/icons/whatsapp";
import Youtube from "../common/shareLinks/icons/youtube";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";

export const getShareIcon = (key) =>{
    switch(key){
        case "facebook": return Facebook;
        case "instagram": return Instagram;
        case "linkedin": return Linkedin;
        case "twitter": return Twitter;
        case "whatsapp": return Whatsapp;
        case "youtube": return Youtube;
        default: return <MoreHorizIcon/>
    }
};