import moment from "moment"

export const formatDate = (date,withTime) => moment(date).format("DD/MM/YYYY" + (withTime?" hh:mm":""));