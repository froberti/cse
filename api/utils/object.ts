import {camelToSnake, snakeToCamel} from "./string";

export function swapKeyValue(json: { [x: string]: string | number; }){
    let ret = {};
    for(let key in json){
        // @ts-ignore
        ret[json[key]] = key;
    }
    return ret;
}

export function objectToCamel(obj){
    return objectKeyHoc(obj,snakeToCamel)
}

export function objectToSnake(obj){
    return objectKeyHoc(obj,camelToSnake)
}

function objectKeyHoc(obj,fn){
    let objectKeys = Object.keys(obj);
    let newObject:any = {};
    for(let i=0;i<objectKeys.length;i++){
        newObject[fn(objectKeys[i])] = obj[objectKeys[i]]
    }
    return newObject
}