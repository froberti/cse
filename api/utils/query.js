const parsePaginationQuery = (query) =>{
    if(!query) return {};

    // {orderBy, limit, offset, orderDirection}
    let paginationKeys = ["orderBy", "limit", "offset", "orderDirection"];
    let queryKeys = Object.keys(query);
    let pagination = {};
    let filters = {};
    for(let i=0;i<queryKeys.length;i++){
        let queryKey = queryKeys[i];

        if(paginationKeys.includes(queryKey))
            pagination[queryKey] = query[queryKey];
        else if(queryKey !== "rowsPerPage" && queryKey !== "page")
            filters[queryKey] = query[queryKey];
    }

    if(query.rowsPerPage)
        pagination.limit = query.rowsPerPage;
    if(query.rowsPerPage && query.page)
        pagination.offset = (query.page * query.rowsPerPage);

    return {pagination,filters};
};

module.exports = {parsePaginationQuery};