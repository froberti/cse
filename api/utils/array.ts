const groupBy = (array,key) =>
    array.reduce((objectsByKeyValue, obj) => {
        const value = obj[key];
        objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
        return objectsByKeyValue;
    }, {});

const toChunks = (array:any[],size:number) => {
    const chunkedArr:any[] = [];
    let index:number = 0;
    while (index < array.length) {
        chunkedArr.push(<never>array.slice(index, size + index));
        index += size;
    }
    return chunkedArr;
};

const toObjectByKey = (array,key) => {
    let newObject = {};

    for(let i=0;i<array.length;i++){
        newObject[array[i][key]] = array[i]
    }

    return newObject
};

export const arrayHelper = {
    toObjectByKey,
    groupBy,
    toChunks
};