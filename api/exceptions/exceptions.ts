export class CustomError{
    constructor(type: string){
        return {type}
    }
}

export class UserAlreadyExistsException extends CustomError{
    constructor(){
        super("USER_ALREADY_EXISTS");
    }
}
export class DniAlreadyExistsException extends CustomError{
    constructor(){
        super("DNI_ALREADY_EXISTS");
    }
}
export class InvalidTokenProvidedException extends CustomError{
    constructor(){
        super("InvalidTokenProvidedException");
    }
}
export class NoTokenProvidedException extends CustomError{
    constructor(){
        super("NoTokenProvidedException");
    }
}
export class UserNotFoundException extends CustomError{
    constructor(){
        super("UserNotFoundException");
    }
}
export class InvalidPasswordException extends CustomError{
    constructor(){
        super("InvalidPasswordException");
    }
}
export class GoogleDataErrorException extends CustomError{
    constructor(){
        super("GoogleDataErrorException");
    }
}
export class DefaultException extends CustomError{
    constructor(){
        super("DefaultException");
    }
}
export class ConsultingRoomNotAvailableForDate extends CustomError{
    constructor(){
        super("ConsultingRoomNotAvailableForDate")
    }
}

export class ProfessionalNotAvailableForDate extends CustomError{
    constructor(){
        super("ProfessionalNotAvailableForDate")
    }
}

export class ScheduleRangeAlreadyTaken extends CustomError{
    constructor(){
        super("ScheduleRangeAlreadyTaken")
    }
}
export class DateToIsGreaterThanDateFrom extends CustomError{
    constructor(){
        super("DateToIsGreaterThanDateFrom")
    }
}
export class FacebookTokenErrorException extends CustomError{
    constructor(){
        super("FacebookTokenErrorException")
    }
}
export class IvalidPermissions extends CustomError{
    constructor(){
        super("IvalidPermissions")
    }
}
export class MercadoLibreAuthException extends CustomError{
    constructor(){
        super("MercadoLibreAuthException")
    }
}
export class InvalidUserType extends CustomError{
    constructor(){
        super("InvalidUserType")
    }
}
export class InvalidParamsException extends CustomError{
    constructor(){
        super("InvalidParamsException");
    }
}