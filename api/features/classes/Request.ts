export interface ILoginRequest {
    email: string;
    password: string;
    token: string;
    providerId: number;
}

export interface IUpdateUserRequest {
    id: number;
    password: string;
    dni: number;
    phone: number;
}

export interface ICreateCourseRequest {
    name:string;
    description:string;
    price:number;
    courseStatusId:number;
    courseThemeId:number;
    showPrice:boolean;
    duration:string;
    target:string;
    order:number;
}

export interface IUpdateCourseRequest {
    courseId:number;
    name:string;
    description:string;
    price:number;
    courseStatusId:number;
    courseThemeId:number;
    showPrice:boolean;
    duration:string;
    target:string;
    order:number;
}

export interface ICreateExerciseRequest {
    title:string;
    introduction:string;
    videoUrl:string;
    functionToEvaluateName:string;
    courseId:number;
    exerciseLanguageId:number;
    assertions:any;
    createdByUserId:number;
}

export interface IUpdateExerciseRequest {
    exerciseId: number;
    title:string;
    introduction:string;
    videoUrl:string;
    functionToEvaluateName:string;
    courseId:number;
    exerciseLanguageId:number;
    assertions:any;
}

export interface IUpdateProfileRequest{
    description: string;
    profileId: string;
}

export interface IAddCollaboratorToCourseRequest{
    courseId: number;
    userId: number;
    collaboratorTypeId: number;
}

export interface IAddUsersToCourseRequest{
    userEmails: string[];
    courseId: number;
}

export interface IRemoveUserFromCourseRequest{
    email: string;
    courseId: string;
}