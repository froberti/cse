export interface ILoginResponse {
    id:number;
    email: string;
    name: string;
    surname:string;
    phone?:string;
    dni?:number;
    roleId: number;
    userStatusId: number;
    functionalities: number[];
}

export interface ICreateCourseResponse{
    title:string;
    introduction:string;
    videoUrl:string;
    maxDuration:number;
    courseStatusId:number;
    isPublic:number;
    userId:number;
    goals: string[];
}


export interface IUpdateCourseResponse {
    courseId:number;
    name:string;
    description:string;
    price:number;
    courseStatusId:number;
    courseThemeId:number;
    showPrice:boolean;
    duration:string;
    target:string;
    order:number;
}


export interface ICreateExerciseResponse{
    title:string;
    introduction:string;
    videoUrl:string;
    functionToEvaluateName:string;
    courseId:number;
    exerciseLanguageId:number;
    assertions:any;
    createdByUserId:number;
}


export interface IUpdateExerciseResponse {
    exerciseId: number;
    title:string;
    introduction:string;
    videoUrl:string;
    functionToEvaluateName:string;
    courseId:number;
    exerciseLanguageId:number;
    assertions:any;
    createdByUserId:number;
}