import {configKeys} from "../../shared/constants";
import db from "../../db";
let NodeCache = require("node-cache");

const CACHE_DURATION = 60*60*1000; //una hora de cache
const cache = new NodeCache( { stdTTL: CACHE_DURATION, checkperiod: 600 } );

export class ConfigManager {
    static async getConfig(key: string):Promise<string>{
        if(cache.get(key)) return cache.get(key);
        let dbConfig = await db('config').select();

        //dbConfig.find(item => item. getDataValue().key === key);

        // @ts-ignore
        const configObjectKeys = Object.values(configKeys);
        for(let i=0;i<configObjectKeys.length;i++){
            let configValue = dbConfig.find(item => item.key === configObjectKeys[i]);
            cache.set(configObjectKeys[i],configValue && configValue.value,CACHE_DURATION);
        }
        return cache.get(key)
    }

    static clearCache(){
        return cache.flushAll()
    }
}
