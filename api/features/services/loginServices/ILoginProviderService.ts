import {ILoginResponse} from "../../classes/Response";
import {ILoginRequest} from "../../classes/Request";

interface ILoginProviderService{
    login(userInfo: ILoginRequest): Promise<ILoginResponse>;
}

export default ILoginProviderService