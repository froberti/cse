import {configKeys, logInProviders} from "../../../shared/constants";
import {OAuth2Client} from "google-auth-library";
import {GoogleDataErrorException} from "../../../exceptions/exceptions";
import UserService from "../UserService";
import ILoginProviderService from "./ILoginProviderService";
import CacheAuth from "./CacheAuth";
import {ILoginResponse} from "../../classes/Response";
import {ILoginRequest} from "../../classes/Request";
import {UserDTO} from "../../dto/User";
import {ConfigManager} from "../ConfigManager";

class GoogleLoginService extends CacheAuth implements ILoginProviderService{
    login(userInfo: ILoginRequest):Promise<ILoginResponse>{
        return new Promise( async (resolve,reject)=>{
            try{
                let clientId = await ConfigManager.getConfig(configKeys.googleLoginClientId);
                const client = new OAuth2Client(clientId);

                const r = await client.verifyIdToken({
                    idToken: userInfo.token,
                    audience: clientId
                });

                const googleUserInfo = r.getAttributes().payload;

                if(!googleUserInfo || !googleUserInfo.email || !googleUserInfo.given_name)
                    return reject(new GoogleDataErrorException());

                const user:UserDTO = {
                    email: googleUserInfo.email,
                    name: googleUserInfo.given_name,
                    surname: <string>googleUserInfo.family_name
                };
                const newUser = await UserService.getOrCreateUser(user,logInProviders.google);
                //this.storeUserAuth(googleUserInfo,newUser.id);
                resolve(newUser);
            }catch(e){
                reject(new GoogleDataErrorException());
            }
        })
    }
}

const googleLoginService = new GoogleLoginService();
export default googleLoginService