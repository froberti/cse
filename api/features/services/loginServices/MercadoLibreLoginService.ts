// import ILoginProviderService from "./ILoginProviderService";
// import axios from "axios";
// import {configKeys, logInProviders} from "../../../shared/constants";
// import UserService from "../UserService";
// import {MercadoLibreAuthException} from "../../../exceptions/exceptions";
// import {ConfigManager} from "../ConfigManager";
// import CacheAuth from "./CacheAuth";
// import {ILoginResponse} from "../../classes/Response";
// import {ILoginRequest} from "../../classes/Request";
// import {UserDTO} from "../../dto/User";
//
// class MercadoLibreLoginService extends CacheAuth implements ILoginProviderService{
//     login(userInfo: ILoginRequest):Promise<ILoginResponse>{
//         return new Promise( async(resolve,reject)=>{
//             try{
//                 const mlAppId = await ConfigManager.getConfig(configKeys.mercadoLibreAppId);
//                 const mlAppSecretKey = await ConfigManager.getConfig(configKeys.mercadoLibreAppSecretKey);
//                 const mlRedirectUri = await ConfigManager.getConfig(configKeys.mercadoLibreAppRedirectUri);
//
//                 const mlTokenResponse = await axios.post(`https://api.mercadolibre.com/oauth/token?grant_type=authorization_code&client_id=${mlAppId}&client_secret=${mlAppSecretKey}&code=${userInfo.token}&redirect_uri=${mlRedirectUri}`);
//                 const {access_token,user_id,expires_in} = mlTokenResponse.data;
//                 const mlAccessTokenResponse = await axios.get(`https://api.mercadolibre.com/users/${user_id}?access_token=${access_token}`);
//                 const {email,first_name,last_name} = mlAccessTokenResponse.data;
//
//                 //TODO VER QUE DATOS GUARDAR EN LA BASE
//                 /*
//                 const mock = {
//                     "id": 211681434,
//                     "nickname": "ROBERTIFRANCO",
//                     "registration_date": "2016-04-18T23:52:45.000-04:00",
//                     "first_name": "Franco",
//                     "last_name": "Roberti",
//                     "gender": "",
//                     "country_id": "AR",
//                     "email": "franroberti95@hotmail.com",
//                     "identification": {
//                         "number": "38590169",
//                         "type": "DNI"
//                     },
//                     "address": {
//                         "address": "Italia 842",
//                         "city": "San Antonio de Padua - Merlo",
//                         "state": "AR-B",
//                         "zip_code": "1718"
//                     },
//                     "phone": {
//                         "area_code": " ",
//                         "extension": "",
//                         "number": "15-6199-6820",
//                         "verified": false
//                     },
//                     "alternative_phone": {
//                         "area_code": "",
//                         "extension": "",
//                         "number": ""
//                     },
//                     "user_type": "normal",
//                     "tags": [
//                         "normal",
//                         "user_info_verified",
//                         "mshops",
//                         "messages_as_seller",
//                         "messages_as_buyer"
//                     ],
//                     "logo": null,
//                     "points": 10,
//                     "site_id": "MLA",
//                     "permalink": "http://perfil.mercadolibre.com.ar/ROBERTIFRANCO",
//                     "shipping_modes": [
//                         "custom",
//                         "not_specified",
//                         "me2"
//                     ],
//                     "seller_experience": "NEWBIE",
//                     "bill_data": {
//                         "accept_credit_note": null
//                     },
//                     "seller_reputation": {
//                         "level_id": null,
//                         "power_seller_status": null,
//                         "transactions": {
//                             "canceled": 0,
//                             "completed": 0,
//                             "period": "historic",
//                             "ratings": {
//                                 "negative": 0,
//                                 "neutral": 0,
//                                 "positive": 0
//                             },
//                             "total": 0
//                         },
//                         "metrics": {
//                             "sales": {
//                                 "period": "60 months",
//                                 "completed": 0
//                             },
//                             "claims": {
//                                 "period": "60 months",
//                                 "rate": 0
//                             },
//                             "delayed_handling_time": {
//                                 "period": "60 months",
//                                 "rate": 0
//                             }
//                         }
//                     },
//                     "buyer_reputation": {
//                         "canceled_transactions": 0,
//                         "tags": [
//                         ],
//                         "transactions": {
//                             "canceled": {
//                                 "paid": null,
//                                 "total": null
//                             },
//                             "completed": null,
//                             "not_yet_rated": {
//                                 "paid": null,
//                                 "total": null,
//                                 "units": null
//                             },
//                             "period": "historic",
//                             "total": null,
//                             "unrated": {
//                                 "paid": null,
//                                 "total": null
//                             }
//                         }
//                     },
//                     "status": {
//                         "billing": {
//                             "allow": true,
//                             "codes": [
//                             ]
//                         },
//                         "buy": {
//                             "allow": true,
//                             "codes": [
//                             ],
//                             "immediate_payment": {
//                                 "reasons": [
//                                 ],
//                                 "required": false
//                             }
//                         },
//                         "confirmed_email": true,
//                         "shopping_cart": {
//                             "buy": "allowed",
//                             "sell": "allowed"
//                         },
//                         "immediate_payment": false,
//                         "list": {
//                             "allow": true,
//                             "codes": [
//                             ],
//                             "immediate_payment": {
//                                 "reasons": [
//                                 ],
//                                 "required": false
//                             }
//                         },
//                         "mercadoenvios": "not_accepted",
//                         "mercadopago_account_type": "personal",
//                         "mercadopago_tc_accepted": true,
//                         "required_action": "",
//                         "sell": {
//                             "allow": true,
//                             "codes": [
//                             ],
//                             "immediate_payment": {
//                                 "reasons": [
//                                 ],
//                                 "required": false
//                             }
//                         },
//                         "site_status": "active",
//                         "user_type": "simple_registration"
//                     },
//                     "secure_email": "frobert.g0jb5w@mail.mercadolibre.com",
//                     "credit": {
//                         "consumed": 0,
//                         "credit_level_id": "MLA5",
//                         "rank": "newbie"
//                     },
//                     "context": {
//                         "device": null,
//                         "flow": null,
//                         "source": "buflo"
//                     }
//                 }
//                 */
//
//                 const user:UserDTO = {
//                     email: email,
//                     name: first_name,
//                     surname: last_name
//                 };
//                 const newUser = await UserService.getOrCreateUser(user,logInProviders.mercadoLibre);
//                 this.storeUserAuth(mlTokenResponse.data,newUser.id,expires_in);
//                 resolve(newUser);
//             }catch(e){
//                 reject(new MercadoLibreAuthException())
//             }
//         })
//     }
// }
//
// const mercadoLibreLoginService = new MercadoLibreLoginService();
// export default mercadoLibreLoginService