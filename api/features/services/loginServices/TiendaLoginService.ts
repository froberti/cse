import ILoginProviderService from "./ILoginProviderService";
import {DefaultException, InvalidPasswordException, UserNotFoundException} from "../../../exceptions/exceptions";
import db from "../../../db";
import * as bcrypt from 'bcrypt';
import {ILoginResponse} from "../../classes/Response";
import {ILoginRequest} from "../../classes/Request";
import UserService from "../UserService";

class TiendaLoginService implements ILoginProviderService{
    login(userInfo: ILoginRequest):Promise<ILoginResponse>{
        return new Promise( async(resolve,reject)=>{
            try{
                const user = await db('user').where({email:userInfo.email}).first();
                if(!user)
                    return reject(new UserNotFoundException());

                let passwordsMatch = await bcrypt.compare(userInfo.password,user.password);
                if(!passwordsMatch)
                    return reject(new InvalidPasswordException());

                // @ts-ignore
                resolve(await UserService.getUser(userInfo.email));
            }catch(e){
                reject(new DefaultException())
            }
        })
    }
}

const tiendaLoginService = new TiendaLoginService();
export default tiendaLoginService