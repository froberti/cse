let NodeCache = require("node-cache");

const CACHE_DURATION = 60*60*1000; //una hora de cache default

abstract class CacheAuth {
    cache = new NodeCache({ stdTTL: CACHE_DURATION, checkperiod: 600 });

    storeUserAuth(auth:any, userId: number, duration:number = CACHE_DURATION):void{ this.cache.set(userId,auth,duration); }
    getUserAuth(userId:number):any{
        return this.cache.get(userId)
    };
    deleteUserAuth(userId:number):any{return this.cache.del(userId)};
}

export default CacheAuth