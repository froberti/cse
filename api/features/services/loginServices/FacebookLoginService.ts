import axios from "axios";
import {FacebookTokenErrorException} from "../../../exceptions/exceptions";
import {ILoginRequest} from "../../classes/Request";
import UserService from "../UserService";
import {logInProviders, userStatus} from "../../../shared/constants";
import ILoginProviderService from "./ILoginProviderService";
import CacheAuth from "./CacheAuth";
import {ILoginResponse} from "../../classes/Response";
import {UserDTO} from "../../dto/User";

class FacebookLoginService extends CacheAuth implements ILoginProviderService{
    login(userInfo: ILoginRequest):Promise<ILoginResponse>{
        return new Promise( async(resolve,reject)=>{
            try{
                const response = await axios.get(`https://graph.facebook.com/v3.3/me?access_token=${userInfo.token}&fields=email,name`);
                const user:UserDTO = {
                    email: response.data.email,
                    name: response.data.name,
                    surname: response.data.surname
                };
                const newUser = await UserService.getOrCreateUser(user,logInProviders.facebook);
                //this.storeUserAuth(response,newUser.id);
                resolve(newUser);
            }catch(e){
                reject(new FacebookTokenErrorException())
            }
        })
    }
}

const facebookLoginService = new FacebookLoginService();
export default facebookLoginService