import {UserDTO} from "../dto/User";
import {ILoginResponse} from "../classes/Response";
import {DefaultException, UserNotFoundException} from "../../exceptions/exceptions";
import db from "../../db";
import {configKeys, logInProviders, userRoles, userStatus} from "../../shared/constants";
import {IUpdateProfileRequest, IUpdateUserRequest} from "../classes/Request";
import moment = require("moment");
import * as bcrypt from "bcrypt";
import {jwtSignKey, jwtTokenDuration, jwtSaltRounds} from "../../constants/config";
import {ConfigManager} from "./ConfigManager";
let jwt = require("jsonwebtoken");
let nodemailer = require('nodemailer');

class UserService{
    getOrCreateUser(user: UserDTO,loginProviderId):Promise<ILoginResponse>{
        return new Promise( async(resolve,reject) => {
            try{
                const userFromDb = await this.getUser(user.email);

                if(userFromDb)
                    return resolve(userFromDb);

                const createdUser = await this.createProviderUser(user,loginProviderId);
                resolve( createdUser )
            }catch(e){
                reject(new DefaultException())
            }
        })
    }
    async getUser(email: string):Promise<ILoginResponse|null>{
        const user = await db('user')
            .where({email}).first();
        if(!user) return null;
        const functionalities = (await db('role_has_functionality').where({role_id: user.role_id})).map(r => r.functionality_id);
        return <ILoginResponse>{
            id: user.id,
            dni: user.dni,
            phone: user.phone,
            name: user.name,
            surname: user.surname,
            email: user.email,
            userStatusId: user.user_status_id,
            roleId: user.role_id,
            functionalities,
            createDatetime: user.createDatetime
        }
    }

    createProviderUser(user: UserDTO,loginProviderId):Promise<ILoginResponse>{
        return new Promise( async (resolve,reject) =>{
            try{

                const insertedUserId =
                    await db('user')
                        .insert({
                            name: user.name,
                            surname: user.surname,
                            email: user.email,
                            user_status_id: userStatus.pendingInfo,
                            role_id: userRoles.default,
                            login_provider_id: loginProviderId
                        });

                const functionalities = await db('role_has_functionality').where({role_id: userRoles.default});

                resolve(<ILoginResponse>{
                    id: insertedUserId,
                    name: user.name,
                    surname: user.surname,
                    email: user.email,
                    userStatusId: userStatus.pendingInfo,
                    roleId: userRoles.default,
                    functionalities
                })
            }catch(e){
                reject(new DefaultException())
            }
        })
    }

    createLocalUser(user: UserDTO):Promise<ILoginResponse>{
        return new Promise( async (resolve,reject) =>{
            try{

                let password = await bcrypt.hash(user.password, jwtSaltRounds);
                const dbUser = await db('user').where({email:user.email}).first();

                let insertedUserId;

                if(dbUser)
                    insertedUserId = dbUser.id;
                else
                    insertedUserId =
                        (await db('user')
                            .insert({
                                name: user.name,
                                surname: user.surname,
                                email: user.email,
                                user_status_id: userStatus.pendingEmail,
                                password,
                                dni: user.dni,
                                phone: user.phone,
                                role_id: userRoles.default,
                                login_provider_id: logInProviders.tienda
                            }))[0];

                const functionalities = await db('role_has_functionality').where({role_id: userRoles.default});

                const backUrl = (await ConfigManager.getConfig(configKeys.apiUrl)) + "/api/user/verifyEmail/";
                const token = this.sendVerificationEmail({id:insertedUserId,email: user.email,name: user.name,surname: user.surname},backUrl);

                await db('user_email_verification_token').insert({
                    token,
                    user_id: insertedUserId
                });

                resolve(<ILoginResponse>{
                    id: insertedUserId,
                    name: user.name,
                    surname: user.surname,
                    email: user.email,
                    userStatusId: userStatus.pendingInfo,
                    roleId: userRoles.default,
                    functionalities
                })
            }catch(e){
                reject(new DefaultException())
            }
        })
    }

    verifyEmailToken(token:string){
        return new Promise( async (resolve,reject) => {
            try{
                const decodedToken = jwt.verify(token, jwtSignKey);
                const {userId} = decodedToken;

                let rows = await db('user_email_verification_token').where({token,user_id:userId}).first();

                if(!rows)
                    return reject(new UserNotFoundException());

                const user = await db('user').where({id:userId}).first();

                if(user && user.user_status_id === userStatus.pendingEmail)
                    await db('user').where({id:userId}).update({user_status_id: userStatus.valid});

                const redirectUri = (await ConfigManager.getConfig(configKeys.frontUrl)) + "/verification";
                resolve(redirectUri)
            }catch(e){
                const redirectUri = (await ConfigManager.getConfig(configKeys.frontUrl)) + "/verificationFailed";
                reject(redirectUri);
            }
        });
    }

    sendVerificationEmail(user:{id:number,email:string,name:string,surname:string},backUrl: string):string{
        const token = jwt.sign(
            {userId:user.id,email: user.email,name: user.name,surname: user.surname},
            jwtSignKey,
            { expiresIn: jwtTokenDuration}
        );

        const mailOptions = {
            from : "<noreply@cse.com>",
            to : user.email,
            subject : "CSE - Verificación de email",
            text : backUrl+token,
            html : `<h2>CSE - Verfiicación de email</h2>
                <p>Para verificar tu cuenta haga click <a href="${backUrl+token}">aquí</a></p>`};

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'acliment@cseducacion.com.ar',
                pass: 'hOLA123$'
            }
        });
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });

        return token;
    }

    updatePendingUserInfo(user: IUpdateUserRequest):Promise<any>{
        return new Promise( async (resolve,reject) =>{
            try{
                const updatedUser =
                    await db('user')
                        .update({
                            phone: user.phone,
                            dni: user.dni,
                            password: user.password,
                            user_status_id: userStatus.valid,
                            //update_datetime: moment().format("YYYY-MM-DD HH:mm:ss")
                        })
                        .where({id: user.id});

                resolve(updatedUser);
            }catch(e){
                reject(new DefaultException())
            }
        })
    }
}

const userService = new UserService();
export default userService