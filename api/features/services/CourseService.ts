import {ICreateCourseResponse, IUpdateCourseResponse} from "../classes/Response";
import {DefaultException} from "../../exceptions/exceptions";
import db from "../../db";
import {ICreateCourseRequest, IUpdateCourseRequest} from "../classes/Request";
import {courseStatus, paymentStatus} from "../../shared/constants";
import {objectToCamel} from "../../utils/object";

class CourseService{

    async deleteTheme(themeId){
        try{
            return await db('course_theme').where({id:themeId}).del();
        }catch(e){
            throw new DefaultException()
        }
    }
    async deleteCourse(courseId){
        try{
            return await db('course').where({id:courseId}).del();
        }catch(e){
            throw new DefaultException()
        }
    }

    async getThemes(userId) {
        try{
            const themes = (await db('course_theme').select()).map( t => objectToCamel(t));
            const courses = (await db('course')
                    .where({
                        //TODO: ver esto
                        course_status_id: courseStatus.active
                    })).map( c => objectToCamel(c));

            const payments =
                (await db('payment')
                    .where('user_id',userId)
                    .whereNotNull('mp_payment_id')
                    .whereIn('payment_status_id', [paymentStatus.done,paymentStatus.pending]))
                    .map(p => objectToCamel(p));

            const coursesWithPayments = courses.map(c => ({...c,payments: payments.filter(p => p.itemId === c.itemId)}));
            return themes.map(t => ({...t, courses: coursesWithPayments.filter(c => c.courseThemeId === t.id)}));
        }catch(e){
            throw new DefaultException();
        }
    }


    async updateCourseTheme(theme){
        try{
            await db('course_theme')
                .where({
                    id:theme.id
                })
                .update({
                name:theme.name,
                description: theme.description || null,
                order: theme.order || null,
                image_url: theme.imageUrl||null
            })
        }catch(e){
            throw new DefaultException()
        }
    }

    async createCourseTheme(theme,userId){
        try{
            const [courseThemeId] = await db('course_theme').insert({
                name:theme.name,
                description: theme.description || null,
                created_by_user_id:userId,
                order: theme.order,
                image_url: theme.imageUrl||null
            });

            return courseThemeId
        }catch(e){
            throw new DefaultException()
        }
    }

    createCourse(course: ICreateCourseRequest,userId: number):Promise<ICreateCourseResponse>{
        return new Promise( async(resolve,reject) => {
            try{
                const courseId = await db.transaction(async t =>{
                        const [itemId] = await db('item')
                            .transacting(t)
                            .insert({
                                price: course.price,
                                description: course.description,
                                name: course.name
                            });

                        const [courseId] = await db('course')
                            .transacting(t)
                            .insert({
                                name: course.name,
                                description: course.description || null,
                                price: course.price,
                                created_by_user_id: userId,
                                item_id: itemId,
                                course_status_id: course.courseStatusId,
                                course_theme_id: course.courseThemeId,
                                show_price: course.showPrice,
                                duration: course.duration || null,
                                target: course.target || null,
                                order: course.order
                            });

                        return courseId;
                    }
                );

                resolve( courseId )
            }catch(e){
                reject(new DefaultException())
            }
        })
    }
    updateCourse(course: IUpdateCourseRequest):Promise<IUpdateCourseResponse>{
        return new Promise( async(resolve,reject) => {
            try{
                await db.transaction( async t => {
                    let dbCourse = await db('course').where({id: course.courseId}).first();

                    if(!dbCourse)
                        throw new DefaultException();

                    await db('item')
                        .transacting(t)
                        .update({name:course.name,description:course.description||null,price:course.price})
                        .where({id:dbCourse.item_id});

                    await db('course')
                        .transacting(t)
                        .update({
                            name: course.name,
                            description: course.description || null,
                            price: course.price,
                            course_status_id: course.courseStatusId,
                            course_theme_id: course.courseThemeId,
                            show_price: course.showPrice,
                            duration: course.duration || null,
                            target: course.target || null,
                            order: course.order
                        })
                        .where({
                            id: course.courseId
                        });
                });

                resolve( course )
            }catch(e){
                reject(new DefaultException())
            }
        })
    }

}

const courseService = new CourseService();
export default courseService