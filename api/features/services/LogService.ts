import db from "../../db";

class LogService {
    async log(name:string,log: string){
        try{
            await db('log').insert({
                name,
                log
            })
        }catch(e){
            console.warn("Failed to log: ",name);
            console.warn(log)
        }
    }
}

const logService = new LogService();
export default logService