import {DefaultException} from "../../exceptions/exceptions";
import db from "../../db";
import MercadoPagoService from "./MercadoPagoService";
import {paymentStatus} from "../../shared/constants";
import moment = require("moment");

const DEFAULT_QUANTITY = 15;

class PaymentService{

    async getPayments(query:{filter:string,page:string,quantity:string|undefined}){
        try{
            let rowsQuantity = (query.quantity && parseInt(query.quantity))|| DEFAULT_QUANTITY;
            let rawWhere = "";
            if(query.filter)
                rawWhere = `email like '${query.filter}%' or dni like '${query.filter}%' or name like '%${query.filter}%' or surname like '%${query.filter}%'`;

             const payments = await db('payment')
                    .select('payment.*','user.name','user.surname','user.email','user.id as user_id','item.name as itemName')
                    .innerJoin('user','user.id','payment.user_id')
                    .innerJoin('item','item.id','payment.item_id')
                    .whereRaw(rawWhere)
                    .orderBy('payment.create_datetime')
                    .limit(rowsQuantity)
                    .offset(rowsQuantity*parseInt(query.page));

            return payments.map( p => ({
                id: p.id,
                createDatetime: p.create_datetime,
                paymentStatusId: p.payment_status_id,
                confirmDatetime: p.confirm_datetime,
                mpPrefId: p.mp_pref_id,
                amount: p.amount,
                user:{
                    id: p.user_id,
                    name: p.name,
                    surname: p.surname,
                    email: p.email
                }
            }))
        }catch(e){
            throw new DefaultException()
        }
    }

    async getPaymentLink(itemId,userId){
        try{
            const item = await db('item').where({id: itemId}).first();
            const user = await db('user').where({id: userId}).first();

            const [paymentId] = await db('payment')
                                .insert({
                                    user_id:userId,
                                    item_id:itemId,
                                    amount: item.price,
                                    payment_status_id: paymentStatus.initialized
                                });

            const mpResponse:any = await MercadoPagoService.getMpItemLink(item,user,paymentId);

            return mpResponse
        }catch(e){
            throw new DefaultException()
        }
    }

    async updatePayment(mlPaymentId,dbPaymentId){
        //https://api.mercadopago.com//v1/payments/21953163?access_token=TEST-862750584686981-092516-5e60a9a45a27427ae6d35654939c3075-469464778
        try{
            const newStatus = await MercadoPagoService.getMpPaymentStatus(mlPaymentId);
            await db('payment')
                .update({
                    payment_status_id: newStatus,
                    mp_payment_id: mlPaymentId,
                    update_datetime: moment().format("YYYY-MM-DD HH:mm:ss"),
                    confirm_datetime: newStatus === paymentStatus.done? moment().format("YYYY-MM-DD HH:mm:ss"):null
                })
                .where({
                    id: dbPaymentId
                });

            return [mlPaymentId,dbPaymentId,newStatus];
        }catch(e){
            throw new DefaultException()
        }
    }

    // async createPayment(itemId,userId){
    //     try{
    //         await db('course_theme')
    //             .where({
    //                 id:theme.id
    //             })
    //             .update({
    //             name:theme.name,
    //             description: theme.description || null,
    //             order: theme.order,
    //             image_url: theme.imageUrl||null
    //         })
    //     }catch(e){
    //         throw new DefaultException()
    //     }
    //}
}

const paymentService = new PaymentService();
export default paymentService