import {ConfigManager} from "./ConfigManager";
import {configKeys, paymentStatus} from "../../shared/constants";
import axios from "axios";
import {DefaultException} from "../../exceptions/exceptions";

const mercadopago = require ('mercadopago');


class MercadoPagoService{
    getMpItemLink(item,user,paymentId){
        return new Promise( async (resolve,reject) => {
            try{
                const {name,surname,email,phone,dni} = user;
                const payer = {
                    name,
                    surname,
                    email,
                    date_created: "2015-06-02T12:58:41.425-04:00",
                    phone: {
                        area_code: "54",
                        number: parseInt(phone)
                    },
                    identification: {
                        type: "DNI",
                        number: dni
                    }
                };
                const items = [
                    {
                        id: item.id,
                        title: item.name,
                        quantity: 1,
                        currency_id: item.currencyId,
                        unit_price: item.price
                    }
                ];

                const frontUrl = await ConfigManager.getConfig(configKeys.frontUrl);
                const backUrl = await ConfigManager.getConfig(configKeys.apiUrl);

                const preference = {
                    payer,
                    items,
                    "back_urls": {
                        "success": `${frontUrl}/payment/success`,
                        "failure": `${frontUrl}/payment/failure`,
                        "pending": `${frontUrl}/payment/pending`
                    },
                    "auto_return": "approved",
                    "notification_url": `${backUrl}/api/payment/notifications/${paymentId}`
                };

                mercadopago.configure({
                    access_token: await ConfigManager.getConfig(configKeys.mpSandboxAccessToken)
                });

                mercadopago.preferences.create(preference)
                    .then( r => resolve(r))
                    .catch( e => reject(e))
            }catch(e){
                reject(e)
            }
        });
    }

    async getMpPaymentStatus(mpPaymentId): Promise<number>{
        try{
            const accessToken = await ConfigManager.getConfig(configKeys.mpSandboxAccessToken);

            let mlResponse = await axios.get(`https://api.mercadopago.com/v1/payments/${mpPaymentId}?access_token=${accessToken}`);

            switch(mlResponse.data.status){
                case "approved": return paymentStatus.done;
                case "in_process": return paymentStatus.pending;
                case "rejected": return paymentStatus.error;
                default:
                    throw new DefaultException()
            }

        }catch(e){
            throw new DefaultException()
        }
    }
}

const mercadoPagoService = new MercadoPagoService();
export default mercadoPagoService