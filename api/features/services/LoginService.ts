import {ILoginResponse} from "../classes/Response";
import {ILoginRequest} from "../classes/Request";
import {logInProviders} from "../../shared/constants";
import FacebookLoginService from "./loginServices/FacebookLoginService";
import GoogleLoginService from "./loginServices/GgoogleLoginService";
import TiendaLoginService from "./loginServices/TiendaLoginService";
//import MercadoLibreLoginService from "./loginServices/MercadoLibreLoginService";

const loginServices = {
    [logInProviders.facebook]: FacebookLoginService,
    [logInProviders.google]: GoogleLoginService,
    [logInProviders.tienda]: TiendaLoginService,
    //[logInProviders.mercadoLibre]: MercadoLibreLoginService
};

class LoginService{
    getUser(userInfo: ILoginRequest):Promise<ILoginResponse>{
        return loginServices[userInfo.providerId].login(userInfo);
    }
}

const loginService = new LoginService();
export default loginService