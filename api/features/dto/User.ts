export interface UserDTO {
    id?:number;
    email: string;
    name: string;
    surname:string;
    roleId?: number;
    password?:string;
    dni?:number;
    phone?: number;
    userStatusId?: number;
    profileId?: number;
}