import * as express from "express";
import {bodyValidationMiddleware, bodyValidations} from "../../middlewares/validation";
import CourseService from "../services/CourseService";
import roleValidation from "../../middlewares/roles";
import {functionality} from "../../shared/constants";

let router = express.Router();

router.delete('/theme/:id',(req,res)=>{
    const {id} = req.params;
    CourseService.deleteTheme(id)
        .then(r =>{
            res.json(r)
        })
        .catch(err =>{
            res.status(500).send(err)
        })
});

router.get('/theme',(req,res)=>{
    const {id} = req.decodedToken;

    CourseService.getThemes(id)
        .then(r =>{
            res.json(r)
        })
        .catch(err =>{
                res.status(500).send(err)
        })
});

router.post('/theme',
    roleValidation(functionality.manageCourses),
    bodyValidationMiddleware({
        name: [bodyValidations.string,bodyValidations.required],
        description: [bodyValidations.string],
        order: [bodyValidations.numeric],
        imageUrl: [bodyValidations.string]
    }), function (req, res) {
        let {name,description,order,imageUrl} = req.body;
        const {id} = req.decodedToken;

        CourseService.createCourseTheme({name,description,order,imageUrl},id)
            .then(r =>{
                res.json(r)
            })
            .catch(err => {
                res.status(500).send(err)
            });
    });

router.put('/theme',
    roleValidation(functionality.manageCourses),
    bodyValidationMiddleware({
        themeId: [bodyValidations.numeric,bodyValidations.required],
        name: [bodyValidations.string,bodyValidations.required],
        description: [bodyValidations.string],
        order: [bodyValidations.numeric],
        imageUrl: [bodyValidations.string]
    }), function (req, res) {
        let {name,description,order,imageUrl,themeId} = req.body;

        CourseService.updateCourseTheme({id:themeId,name,description,order,imageUrl})
            .then(r =>{
                res.json(r)
            })
            .catch(err => {
                res.status(500).send(err)
            });
    });
router.post('/',
    roleValidation(functionality.manageCourses),
    bodyValidationMiddleware({
        name: [bodyValidations.string,bodyValidations.required],
        description: [bodyValidations.string],
        price: [bodyValidations.string,bodyValidations.required],
        courseStatusId: [bodyValidations.numeric,bodyValidations.required],
        courseThemeId: [bodyValidations.numeric,bodyValidations.required],
        showPrice: [bodyValidations.boolean,bodyValidations.required],
        duration: [bodyValidations.string],
        target: [bodyValidations.string],
        order: [bodyValidations.numeric]
    }), function (req, res) {
        let {name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order} = req.body;
        const user = req.decodedToken;

        CourseService.createCourse({name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order},user.id)
            .then(r =>{
                res.json(r)
            })
            .catch(err => {
                res.status(500).send(err)
            });
    });

router.put('/',
    roleValidation(functionality.manageCourses),
    bodyValidationMiddleware({
        courseId: [bodyValidations.numeric,bodyValidations.required],
        name: [bodyValidations.string,bodyValidations.required],
        description: [bodyValidations.string],
        price: [bodyValidations.string,bodyValidations.required],
        courseStatusId: [bodyValidations.numeric,bodyValidations.required],
        courseThemeId: [bodyValidations.numeric,bodyValidations.required],
        showPrice: [bodyValidations.boolean,bodyValidations.required],
        duration: [bodyValidations.string],
        target: [bodyValidations.string],
        order: [bodyValidations.numeric]
    }), function (req, res) {
        let {courseId,name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order} = req.body;
        CourseService.updateCourse({courseId,name,description,price,courseStatusId,courseThemeId,showPrice,duration,target,order})
            .then(r => {
                res.json(r)
            })
            .catch(err => {
                res.status(500).send(err)
            });
    });

router.delete('/:id',(req,res)=>{
    const {id} = req.params;
    CourseService.deleteCourse(id)
        .then(r =>{
            res.json(r)
        })
        .catch(err =>{
            res.status(500).send(err)
        })
});
module.exports = router;