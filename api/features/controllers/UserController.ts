import * as express from "express";
import {bodyValidationMiddleware, bodyValidations} from "../../middlewares/validation";
import UserService from "../services/UserService";
import {ConfigManager} from "../services/ConfigManager";
import {configKeys} from "../../shared/constants";

let router = express.Router();

router.get('/verifyEmail/:token',(req,res) =>{
    const {token} = req.params;
    UserService.verifyEmailToken(token)
        .then(r =>
            res.redirect(r)
        )
        .catch(err => {
            res.redirect(err)
        });
});

router.post('/register',
    bodyValidationMiddleware({
        email: [bodyValidations.email,bodyValidations.required],
        password: [bodyValidations.string],
        phone: [bodyValidations.numeric],
        dni: [bodyValidations.numeric],
        name: [bodyValidations.string],
        surname: [bodyValidations.string]
    }), function (req, res) {
        let {email,password,phone,dni,name,surname} = req.body;
        UserService.createLocalUser({email,password,phone,dni,name,surname})
            .then(r =>
                res.json(r)
            )
            .catch(err => {
                res.status(500).send(err)
            });
    });

router.put('/',
    bodyValidationMiddleware({
        password: [bodyValidations.string],
        phone: [bodyValidations.numeric,bodyValidations.required],
        dni: [bodyValidations.numeric,bodyValidations.required]
    }), function (req, res) {
        let {password,phone,dni} = req.body;
        const user = req.decodedToken;
        UserService.updatePendingUserInfo({password,phone,dni,id: user.id})
            .then(r =>{
                res.json()
            })
            .catch(err => {
                res.status(500).send(err)
            });
    });


module.exports = router;