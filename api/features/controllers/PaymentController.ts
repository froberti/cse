import * as express from "express";
import {bodyValidationMiddleware, bodyValidations} from "../../middlewares/validation";
import PaymentService from "../services/PaymentService";

let router = express.Router();

router.post('/notifications/:paymentId',async (req,res)=>{
    const {paymentId} = req.params;

    PaymentService.updatePayment(req.body.data.id,paymentId)
        .then(r =>{
            res.json(r)
        })
        .catch( err => {
            res.status(500).send(err)
        });

    res.json();
});

router.post('/',
    bodyValidationMiddleware({
        itemId: [bodyValidations.numeric,bodyValidations.required]
    }), function (req, res) {
        let {itemId} = req.body;
        const {id} = req.decodedToken;

        PaymentService.getPaymentLink(itemId,id)
            .then( (r:any) => {
                res.json(r.body.init_point)
            })
            .catch(err => {
                res.status(500).send(err)
            });
    });

router.get('/', function (req, res) {
        let {filter,page,quantity} = req.query;

        PaymentService.getPayments({filter,page,quantity})
            .then( (r:any) => {
                res.json(r)
            })
            .catch(err => {
                res.status(500).send(err)
            });
    });

module.exports = router;