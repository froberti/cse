import * as express from "express";
import {bodyValidationMiddleware, bodyValidations} from "../../middlewares/validation";
import {jwtSignKey,jwtTokenDuration} from "../../constants/config";
import LoginService from "../services/LoginService";
import {ILoginResponse} from "../classes/Response";
let jwt = require("jsonwebtoken");

let router = express.Router();

router.post('/',
    bodyValidationMiddleware({
        email: [bodyValidations.email],
        password: [bodyValidations.string],
        token: [bodyValidations.string],
        providerId: [bodyValidations.numeric]
    }), function (req, res) {
        let {email,password,token,providerId} = req.body;
        LoginService.getUser({email,password,token,providerId})
            .then((user:ILoginResponse) =>{
                let token = jwt.sign(
                    user,
                    jwtSignKey,
                    { expiresIn: jwtTokenDuration}
                );

                res.json({token, user});
            })
            .catch(err => {
                res.status(500).send(err)
            });
    });

module.exports = router;