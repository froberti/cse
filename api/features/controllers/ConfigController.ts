import * as express from "express";
import {ConfigManager} from "../services/ConfigManager";

let router = express.Router();

router.get('/clear',(req,res)=>{
    try{
        ConfigManager.clearCache();
        res.json()
    }catch(e){
        res.status(500).send(e)
    }
});
module.exports = router;