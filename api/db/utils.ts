import mysqldump from 'mysqldump';
import * as dbJson from "./db.json";


// dump the result straight to a file

const dumpDb = path => mysqldump({
    connection: dbJson.dbConnection,
    dumpToFile: path,
});

export default dumpDb