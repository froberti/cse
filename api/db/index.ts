import * as dbJson from "./db.json";

const db = require('knex')({
    client: 'mysql',
    version: '5.7',
    connection: {
        ...dbJson.dbConnection
    }
});

db('user').first()
    .then(() => {
        console.log(`Connection has been established successfully. Host: ${dbJson.dbConnection.host} Db: ${dbJson.dbConnection.database}`);
    })
    .catch(err => {
        //TODO: reintentar ?
        console.error(`Unable to connect to the database. Host: ${dbJson.dbConnection.host} Db: ${dbJson.dbConnection.database}`);
        console.error(err);
        process.exit(0)
    });
db.on('query', console.log)
export default db;