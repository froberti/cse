import app from "./app";
const port =  process.env.PORT || 8080;

// Arranca app en el puerto 8080 si el puerto no esta especificado en el archivo .env.
app.listen(port, () => console.log(`Express server listening on port ${port}`));