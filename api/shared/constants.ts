export const logInProviders = {
    'tienda': 1,
    'google': 2,
    'facebook': 3,
    'mercadoLibre': 4
};

export const userStatus = {
    'valid': 1,
    'pendingEmail': 2,
    'pendingInfo': 3,
    'disabled': 4
};

export const paymentStatus = {
    done: 1,
    pending: 2,
    cancelled: 3,
    error: 4,
    initialized: 5
};

export const courseStatus = {
    active: 1,
    inactive: 2,
    deleted: 3
};

export const userRoles = {
    'default': 1,
    'manager': 2
};

export const functionality = {
    'payForCourses': 1,
    'addEnabledUserEmails': 2,
    'checkPaymentStatus': 3,
    'manageCourses': 4
};

export const configKeys = {
    mpSandboxAccessToken: 'MP_SANDBOX_ACCESS_TOKEN',
    mpSandboxPublicKey: 'MP_SANDBOX_PUBLIC_KEY',
    mpProdAccessToken: 'MP_PROD_ACCESS_TOKEN',
    mpProdPublicKey: 'MP_PROD_PUBLIC_KEY',
    mpClientId: 'MP_CLIENT_ID',
    mpClientSecret: 'MP_CLIENT_SECRET',
    googleLoginClientId: 'GoogleLoginClientId',
    googleCloudStorageBucketName: 'GoogleCloudStorageBucketName',
    googleCloudStorageProjectId: 'GoogleCloudStorageProjectId',
    maxImageUploadBits: 'MaxImageUploadBits',
    maxVideoUploadBits: 'MaxVideoUploadBits',
    defaultAssetImage: 'DefaultAssetImage',
    apiUrl: 'API_URL',
    frontUrl: 'FRONT_URL'
};
