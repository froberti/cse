import {DefaultException, IvalidPermissions, InvalidUserType,} from "../exceptions/exceptions";

const roleValidation = (functionalityId:number) => (req,res,next) => {
    try{
        if(functionalityId && !req.decodedToken.functionalities.some(i => i === functionalityId))
            return res.status(403).send(new IvalidPermissions());
        next();
    }catch(e){
        return res.status(403).send(new DefaultException());
    }
};

export default roleValidation;