let axios = require("axios");
const {configKeys} = require("../shared/constants");
const ConfigManager = require("../features/services/ConfigManager");
const {exceptions} = require("../exceptions/exceptions");
const jwt = require("jsonwebtoken");
const jwtSignKey = require("../constants/config").jwtSignKey;

const mercadoLibreAuthorizationMiddleware = async (req,res,next) => {

    let token = req.headers['mlauth'];

    //Si no hay token, respondo y termino la solicitud
    if (!token)
        return res.status(400).send(exceptions.noTokenProvided);

    //Chequeo que el token sea válido
    try{
        //Si el token es válido, le agrego al request datos que necesite en los controllers
        req.mercadoLibreAuth = jwt.verify(token, jwtSignKey);

        //Sigo a la próxima solicitud
        next();
    }catch(err){
        //Si no es válido, termino la solicitud con un error
        return res.status(403).send(exceptions.invalidTokenProvided);
    }
};

module.exports = mercadoLibreAuthorizationMiddleware;