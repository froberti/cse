//const {product_providers} = require("../shared/constants");
const mercadoLibreAuthorizationMiddleware = require("./mercadoLibre");
const tiendaAuthorizationMiddleware = require("./tienda");

//Devuelve todos los middleware necesarios para autenticar con los proveedores de productos externos

const providersAuthorizationMiddleware = () =>
    [mercadoLibreAuthorizationMiddleware,tiendaAuthorizationMiddleware];

module.exports = providersAuthorizationMiddleware;