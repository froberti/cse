let {InvalidParamsException} = require("../exceptions/exceptions");

// @ts-ignore
export const bodyValidationMiddleware = (validation: any) => (req: any, res: any,next: any)=> middleware(req,res,next,validation);

export const bodyValidations = {
    required: "required",
    email: "email",
    numeric: "numeric",
    string: "string",
    letters: "letters",
    alphanumeric: "Alphanumeric",
    boolean: "boolean",
};

function middleware(req: { body: any; }, res: { status: (arg0: number) => { send: (arg0: any) => void; }; }, next: () => void,validation: any ){
    const validationResult = validator(req.body,validation);
    if(validationResult !== true){
        res.status(500).send({...new InvalidParamsException(),validationResult});
        return;
    }
    next();
}

function validator(objectToValidate: { [x: string]: any; }, validation: { [x: string]: any; }){
    const keys = Object.keys(validation);
    for(let i=0;i<keys.length;i++){
        let keyToValidate = keys[i];
        let validationArray = validation[keyToValidate];
        for(let j=0;j<validationArray.length;j++){
            let valueToValidate = objectToValidate[keyToValidate];
            if(!validateFunction(validationArray[j],valueToValidate))
                return {key: keyToValidate,bodyValidation: validationArray[j]}
        }
    }
    return true
}

function validateFunction(functionKey: string, value: any){

    if(!value && functionKey !== bodyValidations.required)
        return true;

    switch(functionKey){
        case bodyValidations.required: return value !== null && value !== undefined;
        case bodyValidations.email: return !value || emailReg.test(value);
        case bodyValidations.numeric: return !value || numericReg.test(value);
        case bodyValidations.string: return !value || typeof value === 'string';
        case bodyValidations.letters: return !value || lettersReg.test(value);
        case bodyValidations.alphanumeric: return !value || alphanumericReg.test(value);
        case bodyValidations.boolean: return (value === false || value === true || value === 0 || value === 1 || value === '0' || value === '1' );
        default: return false
    }
}

const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const numericReg = /^\d+$/;
const lettersReg = /^[a-zA-Z]+$/;
const alphanumericReg = /^[a-zA-Z0-9_]*$/;

