import * as jwt from "jsonwebtoken";
import {jwtSignKey} from "../constants/config";
import {InvalidTokenProvidedException, NoTokenProvidedException} from "../exceptions/exceptions";
import {ILoginResponse} from "../features/classes/Response";

export interface TokenInfo extends ILoginResponse {}

function verifyToken(req: { authorization?: string, url: { includes: (arg0: string) => boolean; }; headers: { [x: string]: string; },decodedToken: TokenInfo }, res: { status: { (arg0: number): { send: (arg0: NoTokenProvidedException) => void; }; (arg0: number): { send: (arg0: InvalidTokenProvidedException) => void; }; }; }, next: { (): void; (): void; }) {

    if (req.url.includes("login") ||
        req.url.includes("register") ||
        req.url.includes("verifyEmail") ||
        req.url.includes("notifications")) {
        next();
        return;
    }

    //Verifico que el token este presente el header del request
    let token = req.headers.authorization && req.headers.authorization.substring(7);

    //Si no hay token, respondo y termino la solicitud
    if (!token)
        return res.status(400).send(new NoTokenProvidedException());

    //Chequeo que el token sea válido
    try{

        //Si el token es válido, le agrego al request datos que necesite en los controllers
        req.decodedToken = jwt.verify(token, jwtSignKey);

        //Sigo a la próxima solicitud
        next();
    }catch(err){
        //Si no es válido, termino la solicitud con un error
        return res.status(403).send(new InvalidTokenProvidedException());
    }
}

export default verifyToken;