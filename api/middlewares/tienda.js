let axios = require("axios");

const tiendaAuthorizationMiddleware = async (req,res,next) => {
    req.tiendaAuth = {};
    next();
};

module.exports = tiendaAuthorizationMiddleware;