export const jwtSignKey = require('crypto').randomBytes(256).toString('base64');
export const jwtTokenDuration = 60 * 60 * 24 * 31; //31 dias

export const jwtSaltRounds = 15;