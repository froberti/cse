import * as express from "express";
import * as cors from "cors";
import * as helmet from "helmet";
import * as bodyParser from "body-parser";
import verifyToken from "./middlewares/jwt";
import * as fs from "fs";
import * as path from "path";

let app = express();
// Habilito cors para todos los origenes.
// Cuando la request con "OPTIONS" sale bien, devuelve un 200 (por default es 204 y algunos exploradores viejos no lo captan bien)
app.use(cors({origin: '*',optionsSuccessStatus: 200}));

// Helmet, middleware de seguridad de requests
app.use(helmet());

// Parser del body de las requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//Agrego middleware de jwt
app.use(verifyToken);


let controllersPath = path.join(__dirname, "./features/controllers");
fs.readdirSync(controllersPath).forEach( async file => {
    if (file.includes("Controller") && !file.includes(".map") ) {
        console.log("file",file);
        let fileName = file.replace("Controller.ts","").replace("Controller.js","").toLowerCase();
        let controller = await import(`${controllersPath}/${file}`);
        app.use(`/api/${fileName}`,controller);
    }
});

export default app;