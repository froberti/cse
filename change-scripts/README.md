#Change scripts

## Parametros
````
         [pathConfiguracion]  [dbEngine]
Default: /config              mysql
````

## Flags
````
    --delete-failed-scripts-log //Borra los scripts que fallaron en la anterior corrida
    --silent //No pregunta para continuar a correr los scripts ni para finalizar
````