-- -----------------------------------------------------
-- Populate Table `cse`.`logInProvider`
-- -----------------------------------------------------
INSERT INTO
`cse`.`login_provider` (id,name)
Values 
	(1,'Tienda'),
	(2,'Google'),
	(3,'Facebook'),
	(4,'Mercado Libre');

-- -----------------------------------------------------
-- Populate Table `cse`.`user_status`
-- -----------------------------------------------------
INSERT INTO 
`cse`.`user_status` (id,name)
Values 
	(1,'Valid'),
	(2,'Pending email'),
	(3,'Pending info'),
	(4,'Disabled');

-- -----------------------------------------------------
-- Populate Table `cse`.`user_status`
-- -----------------------------------------------------
INSERT INTO 
`cse`.`payment_status` (id,name)
Values 
	(1,'Done'),
	(2,'Pending'),
	(3,'Cancelled'),
	(4,'Error'),
	(5,'Initialized');

-- -----------------------------------------------------
-- Populate Table `cse`.`role`
-- -----------------------------------------------------
INSERT INTO
`cse`.`role` (id,name)
Values 
	(1,'Default'),
	(2,'Manager');

-- -----------------------------------------------------
-- Populate Table `cse`.`functionality`
-- -----------------------------------------------------
INSERT INTO 
`cse`.`functionality` (id,name)
Values 
	(1,'Pay for courses'),
	(2,'Add enabled user emails'),
	(3,'Check payment status'),
	(4,'Manage Courses');

-- -----------------------------------------------------
-- Populate Table `cse`.`role_has_functionality`
-- -----------------------------------------------------
INSERT INTO 
`cse`.`role_has_functionality` (role_id,functionality_id)
Values 
	(1,1),
	(2,1),
	(2,2),
	(2,3),
	(2,4);

-- -----------------------------------------------------
-- Populate Table `cse`.`course_status`
-- -----------------------------------------------------
INSERT INTO 
`cse`.`course_status` (id,name)
Values 
	(1,'Active'),
	(2,'Inactive'),
	(3,'Deleted');

-- -----------------------------------------------------
-- Populate Table `cse`.`currency`
-- -----------------------------------------------------
INSERT INTO 
`cse`.`currency` (id)
Values 
	('ARS');

-- -----------------------------------------------------
-- Populate Table `cse`.`config`
-- -----------------------------------------------------
INSERT INTO 
`cse`.`config` (`key`,`value`)
Values 
	('MP_SANDBOX_ACCESS_TOKEN','TEST-862750584686981-092516-5e60a9a45a27427ae6d35654939c3075-469464778'),
	('MP_SANDBOX_PUBLIC_KEY','TEST-5410c786-1765-456a-912f-829dfff0c026'),
	('MP_PROD_ACCESS_TOKEN','APP_USR-59ab0771-62db-4a7b-a02e-a30b4cdae89c'),
	('MP_PROD_PUBLIC_KEY','APP_USR-862750584686981-092516-16460831b378c94a34a2fb0fda5a65aa-469464778'),
	('MP_CLIENT_ID','862750584686981'),
	('MP_CLIENT_SECRET','oNvFWgbCdqqQTUu3OarOJQ1nluBXG44s'),
	('API_URL','https://cse-web-253604.appspot.com'),
	('FRONT_URL','https://cseducacion.com.ar'),
	('GoogleLoginClientId','840476848398-j2mk936v6e37s74um6j64kd119l6jsq6.apps.googleusercontent.com');