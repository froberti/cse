-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema cse
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema cse
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cse` DEFAULT CHARACTER SET latin1 ;
USE `cse` ;

-- -----------------------------------------------------
-- Table `cse`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`role` (
  `id` INT(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`user_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`user_status` (
  `id` INT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`login_provider`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`login_provider` (
  `id` BIGINT NOT NULL,
  `name` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`user` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NULL,
  `name` VARCHAR(100) NULL,
  `surname` VARCHAR(100) NULL,
  `dni` VARCHAR(10) NULL,
  `phone` VARCHAR(100) NULL,
  `create_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` INT(2) UNSIGNED NOT NULL,
  `user_status_id` INT(3) UNSIGNED NOT NULL,
  `login_provider_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`, `role_id`, `user_status_id`, `login_provider_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_user_role1_idx` (`role_id` ASC) ,
  INDEX `fk_user_user_status1_idx` (`user_status_id` ASC) ,
  INDEX `fk_user_login_provider1_idx` (`login_provider_id` ASC) ,
  CONSTRAINT `fk_user_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `cse`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_user_status1`
    FOREIGN KEY (`user_status_id`)
    REFERENCES `cse`.`user_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `unique_email` UNIQUE (`email`),
  CONSTRAINT `fk_user_login_provider1`
    FOREIGN KEY (`login_provider_id`)
    REFERENCES `cse`.`login_provider` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`functionality`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`functionality` (
  `id` INT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`role_has_functionality`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`role_has_functionality` (
  `role_id` INT(2) UNSIGNED NOT NULL,
  `functionality_id` INT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `functionality_id`),
  INDEX `fk_role_has_functionality_functionality1_idx` (`functionality_id` ASC) ,
  INDEX `fk_role_has_functionality_role_idx` (`role_id` ASC) ,
  CONSTRAINT `fk_role_has_functionality_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `cse`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_has_functionality_functionality1`
    FOREIGN KEY (`functionality_id`)
    REFERENCES `cse`.`functionality` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`currency`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`currency` (
  `id` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`item` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `price` INT(10) NOT NULL,
  `description` VARCHAR(100) NULL,
  `create_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` VARCHAR(100) NOT NULL,
  `currency_id` VARCHAR(5) NOT NULL DEFAULT 'ARS',
  PRIMARY KEY (`id`, `currency_id`),
  INDEX `fk_item_currency1_idx` (`currency_id` ASC) ,
  CONSTRAINT `fk_item_currency1`
    FOREIGN KEY (`currency_id`)
    REFERENCES `cse`.`currency` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`course_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`course_status` (
  `id` INT(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`course_theme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`course_theme` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(500) NULL,
  `created_by_user_id` BIGINT UNSIGNED NOT NULL,
  `order` INT(3) UNSIGNED NOT NULL DEFAULT 0,
  `image_url` VARCHAR(200) NULL,
  PRIMARY KEY (`id`, `created_by_user_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_course_theme_user1_idx` (`created_by_user_id` ASC) ,
  CONSTRAINT `fk_course_theme_user1`
    FOREIGN KEY (`created_by_user_id`)
    REFERENCES `cse`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`course` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(300) NULL,
  `price` INT(10) NOT NULL,
  `create_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mercado_pago_link` VARCHAR(200) NULL,
  `item_id` BIGINT UNSIGNED NOT NULL,
  `created_by_user_id` BIGINT UNSIGNED NOT NULL,
  `course_status_id` INT(2) UNSIGNED NOT NULL,
  `course_theme_id` BIGINT UNSIGNED NOT NULL,
  `show_price` TINYINT NOT NULL DEFAULT 1,
  `duration` VARCHAR(50) NULL,
  `target` VARCHAR(100) NULL,
  `order` INT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`, `item_id`, `created_by_user_id`, `course_status_id`, `course_theme_id`),
  INDEX `fk_course_item1_idx` (`item_id` ASC) ,
  INDEX `fk_course_user1_idx` (`created_by_user_id` ASC) ,
  INDEX `fk_course_course_status1_idx` (`course_status_id` ASC) ,
  INDEX `fk_course_course_theme1_idx` (`course_theme_id` ASC) ,
  CONSTRAINT `fk_course_item1`
    FOREIGN KEY (`item_id`)
    REFERENCES `cse`.`item` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_user1`
    FOREIGN KEY (`created_by_user_id`)
    REFERENCES `cse`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_course_status1`
    FOREIGN KEY (`course_status_id`)
    REFERENCES `cse`.`course_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_course_theme1`
    FOREIGN KEY (`course_theme_id`)
    REFERENCES `cse`.`course_theme` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`payment_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`payment_status` (
  `id` INT(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`payment` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `item_id` BIGINT UNSIGNED NOT NULL,
  `create_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_status_id` INT(2) UNSIGNED NOT NULL,
  `confirm_datetime` DATETIME NULL,
  `update_datetime` DATETIME NULL,
  `amount` BIGINT,
  `mp_pref_id` VARCHAR(100) NULL,
  `mp_payment_id` VARCHAR(100) NULL,
  PRIMARY KEY (`id`, `user_id`, `item_id`, `payment_status_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_payment_user1_idx` (`user_id` ASC) ,
  INDEX `fk_payment_item1_idx` (`item_id` ASC) ,
  INDEX `fk_payment_payment_status1_idx` (`payment_status_id` ASC) ,
  CONSTRAINT `fk_payment_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `cse`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_item1`
    FOREIGN KEY (`item_id`)
    REFERENCES `cse`.`item` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_payment_status1`
    FOREIGN KEY (`payment_status_id`)
    REFERENCES `cse`.`payment_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`user_email_verification_token`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`user_email_verification_token` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `create_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` VARCHAR(300) NOT NULL,
  `user_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `user_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_verification_token_user1_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_verification_token_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `cse`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`config` (
  `key` VARCHAR(200) NOT NULL,
  `value` VARCHAR(500) NOT NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`course_faq`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`course_faq` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` VARCHAR(100) NOT NULL,
  `answer` VARCHAR(500) NOT NULL,
  `course_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `course_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_course_faq_course1_idx` (`course_id` ASC) ,
  CONSTRAINT `fk_course_faq_course1`
    FOREIGN KEY (`course_id`)
    REFERENCES `cse`.`course` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`course_image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`course_image` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(150) NOT NULL,
  `course_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `course_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_course_image_course1_idx` (`course_id` ASC) ,
  CONSTRAINT `fk_course_image_course1`
    FOREIGN KEY (`course_id`)
    REFERENCES `cse`.`course` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cse`.`log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cse`.`log` (
  `name` VARCHAR(100) NULL,
  `log` VARCHAR(500) NULL,
  `create_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
